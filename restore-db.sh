#!/bin/sh

PROD_DB="donut_prod"
LOCAL_DB="donut_dev"
REMOTE_NAME="donut"
DATE=$(date +"%Y%m%d")

set -e

if [ -f ../db/$DATE.dump ]; then
  echo "Today's dump found, using existing file..."
else
  echo "Dumping DB on the remote server..."
  ssh $REMOTE_NAME "pg_dump $PROD_DB -Fc -f ~/$DATE.dump"
  echo "Fetching the DB dump..."
  mkdir -p ../db
  scp $REMOTE_NAME:~/$DATE.dump ../db/
fi
echo "Dropping database..."
psql -c "drop database $LOCAL_DB"
psql -c "create database $LOCAL_DB"
echo "Restoring dump..."
pg_restore -O -d $LOCAL_DB ../db/$DATE.dump
echo "OK"
exit 0

