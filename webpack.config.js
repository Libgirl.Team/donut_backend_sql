process.env["NODE_CONFIG_DIR"] = __dirname + "/assets/config";
const webpack = require("webpack");
const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ConfigWebpackPlugin = require("config-webpack");

const appDirectory = path.resolve(__dirname, "assets");
const resolveAppPath = (relativePath) =>
  path.resolve(appDirectory, relativePath);

module.exports = {
  mode: "development",
  devtool: "eval-source-map",
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx"],
    alias: {
      shared: resolveAppPath("../src/shared"),
      icons: resolveAppPath("js/icons"),
      components: resolveAppPath("js/components"),
      views: resolveAppPath("js/views"),
      helpers: resolveAppPath("js/helpers")
    }
  },
  entry: {
    app: path.resolve(__dirname, "assets/js/index.tsx")
  },
  devServer: {
    contentBase: path.join(__dirname, "assets/public"),
    compress: true,
    port: 5000,
    hot: true,
    publicPath: "/",
    historyApiFallback: {
      index: "/"
    }
  },
  output: {
    path: path.resolve(__dirname, "dist/assets"),
    filename: "js/[name].js",
    publicPath: "/"
  },
  module: {
    rules: [
      {
        test: /.jsx?$/i,
        exclude: /node_modules/,
        use: ["babel-loader"]
      },
      {
        test: /\.tsx?$/i,
        exclude: /node_modules/,
        loader: "ts-loader"
      },
      {
        test: /\.(css|scss|sass)$/,
        use: [
          "style-loader",
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader"
        ]
      },
      {
        test: /\.pug$/,
        loader: "pug-plain-loader"
      },
      {
        test: /\.svg$/,
        exclude: /node_modules/,
        use: ["@svgr/webpack"]
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url-loader?limit=10000&mimetype=application/font-woff"
      },
      {
        test: /\.(ttf|eot|svg|png)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        include: /node_modules/,
        loader: "file-loader"
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({ filename: "css/[name].css" }),
    new ConfigWebpackPlugin(),
    new CopyWebpackPlugin([
      {
        from: "assets/public"
      }
    ]),
    new HtmlWebpackPlugin({
      inject: true,
      template: path.resolve(__dirname, "assets/public/index.html")
    })
  ]
};

if (process.env.NODE_ENV === "production") {
  module.exports.mode = "production";
  module.exports.devtool = false;
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    }),
    new CompressionPlugin()
  ]);
}
