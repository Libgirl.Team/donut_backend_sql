module.exports = {
  oauth: {
    client_id: "MY_CLIENT_ID",
    project_id: "MY_PROJECT_ID",
    client_secret: "MY_CLIENT_SECRET",
    redirect_uri: "https://example.com/api/oauth"
  },
  dbConfig: {
    database: "donut_prod",
    host: "/var/run/postgresql",
    user: "app"
  },
  session: {
    jwtSigner: "MY_JWT_SECRET"
  }
};
