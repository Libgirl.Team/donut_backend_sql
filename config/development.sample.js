module.exports = {
  // Credentials for OAuth
  oauth: {
    client_id: "MY_CLIENT_ID",
    project_id: "MY_PROJECT_ID",
    client_secret: "MY_CLIENT_SECRET"
  },
  // Credentials for other GCP services, used in deployment
  gcp: {
    region: "asia-east1",
    projectId: "wizard-sleeve",
    keyFilename: "/home/user/credentials.json",
    serviceRoleKeyFilename: "/home/user/service-credentials.json",
    serviceRoleName: "deploy@my-service-account.iam.gserviceaccount.com"
  }
};
