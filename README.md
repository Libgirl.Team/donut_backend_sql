# Donut

This is a repo that combines the backend and frontend modules of the Donut machine learning
model management system.

## Up and running. Step 1: System dependencies

After cloning, please set up GCP OAuth secrets in `config/development.js`.
You can use `config/development.sample.js` as a reference.

### Installing PostgreSQL, Node and additional software on macOS

Installing dependencies is a piece of cake if you are working on macOS.

```bash
brew install postgresql node@12 yarn tmux
```

### Installing PostgreSQL, Node and additional software on Ubuntu GNU/Linux 18.04

On Ubuntu GNU/Linux, this step is a bit more complicated.

#### PostgreSQL 12 installation

You can skip this step if you already have PostgreSQL 12 installed.
Otherwise, you can just paste the snippet below inside a terminal window.

Based on https://computingforgeeks.com/install-postgresql-12-on-ubuntu/ and tested
on Ubuntu 18.04.

```bash
# Import GPG key for PostgreSQL apt repositiories
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

# Add PostgreSQL repository to apt sources
echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" | sudo tee  /etc/apt/sources.list.d/pgdg.list

# Update package cache
sudo apt update

# Install PostgreSQL 12
sudo apt -y install postgresql-12 postgresql-client-12

# Set a password for database admin user
sudo su postgres -c "psql -c \"alter user postgres with password 'postgres';\""

# Create a DB superuser account and default database for your system account
sudo su postgres -c "createuser -s $USER && createdb $USER"
```

#### Node installation

You can skip this step if you already have Node 12+ installed.
Otherwise, you can just paste the snippet below inside a terminal window.

Based on https://computingforgeeks.com/how-to-install-nodejs-on-ubuntu-debian-linux-mint/
and tested on Ubuntu 18.04.

```bash
# Install software dependencies for Node
sudo apt -y install curl dirmngr apt-transport-https lsb-release ca-certificates build-essential

# Add apt repositories for Node
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -

# Update package cache
sudo apt update

# Install Node 12, npm and yarn
sudo apt -y install nodejs

# Install yarn
sudo npm i -g yarn
```

#### Install tmux

```bash
sudo apt -y install tmux
```

## Up and running. Step 2: Application setup

```bash
# Install node dependencies inside the project directory
yarn install

# Create and seed the database
# Make sure that the database configuration for development
# in `config/development.js` and `src/repo/config.js` are correct.
yarn db:setup

# Run the project in split window
tmux # This command will start a new terminal session inside tmux, terminal multiplexer
yarn start

# Alternatively, you can use separate commands,
yarn server:watch # run the backend development server
yarn assets:watch # run webpack development server
```

By default, the development server binds to port 3000, and the Wepack development server for assets
binds to port 5000.

The application has been developed on PostgreSQL 12.2 and Node 12.16.1, on a macOS 10.15.4 machine.
