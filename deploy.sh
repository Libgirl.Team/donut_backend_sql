#!/bin/sh

set -e

export NODE_ENV=production

cd /usr/local/lib/donut
git pull
npm install
npm run build
sequelize db:migrate
sudo systemctl restart backend
