import typeDefs from "./schema";
import resolvers from "./resolvers";
import typeResolvers from "./typeResolvers";
import { ApolloServer } from "apollo-server-express";
import snakeCase from "lodash/snakeCase";
import { createContext } from "dataloader-sequelize";
import DataLoader from "dataloader";
import ModelType from "../repo/models/ModelType";
import { repo } from "../repo";
import EfficiencyDataLoader from "../repo/loaders/EfficiencyDataLoader";
import EvaluationDataLoader from "../repo/loaders/EvaluationDataLoader";
import { GraphQLResolveInfo } from "graphql";

const server = new ApolloServer({
  typeDefs,
  fieldResolver: (
    source: any,
    args: any,
    context: any,
    info: GraphQLResolveInfo
  ) => {
    return source[info.fieldName] || source[snakeCase(info.fieldName)];
  },
  context: async ({ req, res }) => {
    return {
      ip: req.headers["x-forwarded-for"] || req.connection.remoteAddress,
      user: res.locals.user,
      loader: createContext(repo()),
      efficiencyLoader: EfficiencyDataLoader(),
      evaluationLoader: EvaluationDataLoader(),
      modelVersionLoader: new DataLoader((keys: any) =>
        ModelType.getVersionMap(keys)
      )
    };
  },
  resolvers: { ...resolvers, ...typeResolvers },
  playground: {
    settings:
      {
        "editor.theme": "dark",
        "editor.fontSize": 16,
        "request.credentials": "same-origin",
        "schema.polling.enable": false // it is there in the docs! (KM 2020-04-07)
      } as any
  }
});

export default server;
