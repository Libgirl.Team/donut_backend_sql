import { IResolverObject } from "graphql-tools";
import User from "../repo/models/User";
import Evaluation from "../repo/models/Evaluation";
import Efficiency from "../repo/models/Efficiency";
import EvaluationType from "../repo/models/EvaluationType";
import EfficiencyType from "../repo/models/EfficiencyType";
import DataType from "../repo/models/DataType";
import Usage from "../repo/models/Usage";
import Query from "./resolvers/Query";
import Mutation from "./resolvers/Mutation";
import Model from "./resolvers/Model";
import ModelSchema from "../repo/models/Model";
import resolveWithDataLoader, {
  resolvePluralWithDataLoader
} from "./resolveWithDataLoader";
import Algorithm from "../repo/models/Algorithm";
import ModelType from "../repo/models/ModelType";
import Dataset from "../repo/models/Dataset";

const owner = resolveWithDataLoader("owner", User);

const resolvers: IResolverObject = {
  Algorithm: {
    owner
  },
  Dataset: {
    owner,
    models: resolvePluralWithDataLoader("datasetId", ModelSchema)
  },
  DataType: {
    owner
  },
  ModelType: {
    owner,
    models: resolvePluralWithDataLoader("modelTypeId", ModelSchema),
    usage: resolveWithDataLoader("usage", Usage),
    algorithm: resolveWithDataLoader("algorithm", Algorithm),
    versionMap: (parent: ModelType, _args, { modelVersionLoader }) =>
      modelVersionLoader.load(parent.id),
    versions: (parent: ModelType) => ModelType.getExistingVersions(parent.id)
  },
  EfficiencyEvaluation: {
    __resolveType: (parent: Evaluation | Efficiency) => {
      if (parent instanceof Evaluation) return "Evaluation";
      return "Efficiency";
    }
  },
  EfficiencyEvaluationType: {
    __resolveType: (parent: EfficiencyType | EvaluationType) =>
      parent.__resolveType()
  },
  EvaluationType: {
    dataset: resolveWithDataLoader("dataset", Dataset)
  },
  Format: {
    owner,
    models: resolvePluralWithDataLoader("formatId", ModelSchema)
  },
  Evaluation: {
    type: resolveWithDataLoader("type", EvaluationType)
  },
  Efficiency: {
    type: resolveWithDataLoader("type", EfficiencyType)
  },
  Usage: {
    owner,
    inputType: resolveWithDataLoader("inputType", DataType),
    outputType: resolveWithDataLoader("outputType", DataType)
  },
  Model,
  Query,
  Mutation
};

export default resolvers;
