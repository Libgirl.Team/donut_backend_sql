import { IResolverObject } from "graphql-tools";
import User from "../../repo/models/User";
import resolveWithPermission from "../resolveWithPermission";

interface IContext {
  resource: User;
}

const UserMutations: IResolverObject = {
  deactivateUser: resolveWithPermission(
    async (_parent, _args, { resource }: IContext) => {
      const updated = await resource.deactivate();
      return { success: true, resource: updated || resource };
    },
    User
  ),
  activateUser: resolveWithPermission(
    async (_parent, _args, { resource }: IContext) => {
      const updated = await resource.activate();
      return { success: true, resource: updated || resource };
    },
    User
  )
};

export default UserMutations;
