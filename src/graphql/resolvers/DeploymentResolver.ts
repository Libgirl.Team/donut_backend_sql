import config from "config";
import { IResolverObject } from "graphql-tools";
import CloudRunClient from "../../gcp/CloudRunClient";
import resolveWithPermission from "../resolveWithPermission";
import Model from "../../repo/models/Model";

const { projectId } = config.get("gcp");

const DeploymentResolver: IResolverObject = {
  projectData: () => ({
    id: projectId
  }),
  services: CloudRunClient.listServices,
  deployModel: resolveWithPermission(
    async (_obj, { serviceName }, { resource }) => {
      console.log(serviceName, resource);
      return {
        success: true
      };
    },
    Model
  )
};

export default DeploymentResolver;
