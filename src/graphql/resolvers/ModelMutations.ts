import { IResolverObject } from "graphql-tools";
import Model from "../../repo/models/Model";
import resolveWithPermission from "../resolveWithPermission";

const ModelMutations: IResolverObject = {
  setModelEfficiencies: resolveWithPermission(
    async (_, { efficiencies }, { resource }) => {
      const updated = await resource.updateEfficiencies(efficiencies);
      return { success: true, resource: updated };
    },
    Model
  ),
  setModelEvaluations: resolveWithPermission(
    async (_, { evaluations }, { resource }) => {
      const updated = await resource.updateEvaluations(evaluations);
      return { success: true, resource: updated };
    },
    Model
  ),
  deactivateModel: resolveWithPermission(
    async (_parent, _args, { resource }) => {
      const updated = await resource.deactivate();
      return { success: true, resource: updated };
    },
    Model
  ),
  deleteModel: resolveWithPermission(async (_parent, _args, { resource }) => {
    await resource.virtuallyDelete();
    return { success: true };
  }, Model),
  activateModel: resolveWithPermission(async (_parent, _args, { resource }) => {
    const updated = await resource.activate();
    return { success: true, resource: updated };
  }, Model)
};

export default ModelMutations;
