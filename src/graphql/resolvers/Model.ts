import { IResolverObject } from "graphql-tools";
import Model from "../../repo/models/Model";
import Evaluation from "../../repo/models/Evaluation";
import Efficiency from "../../repo/models/Efficiency";
import ModelType from "../../repo/models/ModelType";
import Dataset from "../../repo/models/Dataset";
import User from "../../repo/models/User";
import Format from "../../repo/models/Format";
import resolveWithDataLoader, {
  resolvePluralWithDataLoader
} from "../resolveWithDataLoader";

const ModelResolver: IResolverObject = {
  displayName: ({ id, minor, major }: Model) => `${major}.${minor} (#${id})`,
  version: ({ minor, major }: Model) => `${major}.${minor}`,
  efficiencies: resolvePluralWithDataLoader("modelId", Efficiency),
  evaluations: resolvePluralWithDataLoader("modelId", Evaluation),
  modelURI: (model: any) => {
    return model.modelURI || model.model_uri;
  },
  evaluationMap: (model: Model, _args, { evaluationLoader }) =>
    evaluationLoader.load(model.id),
  efficiencyMap: (model: Model, _args, { efficiencyLoader }) =>
    efficiencyLoader.load(model.id),
  author: resolveWithDataLoader("author", User),
  owner: resolveWithDataLoader("owner", User),
  dataset: resolveWithDataLoader("dataset", Dataset),
  modelType: resolveWithDataLoader("modelType", ModelType),
  sourceModel: resolveWithDataLoader("sourceModel", Model),
  format: resolveWithDataLoader("format", Format)
};

export default ModelResolver;
