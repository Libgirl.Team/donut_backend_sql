import { IResolverObject } from "graphql-tools";
import Model from "../../repo/models/Model";
import Algorithm from "../../repo/models/Algorithm";
import Dataset from "../../repo/models/Dataset";
import DataType from "../../repo/models/DataType";
import User from "../../repo/models/User";
import ModelType from "../../repo/models/ModelType";
import EvaluationType from "../../repo/models/EvaluationType";
import EfficiencyType from "../../repo/models/EfficiencyType";
import Format from "../../repo/models/Format";
import Usage from "../../repo/models/Usage";
import Permissions from "../../shared/permissions";
import paginateResource from "../paginateResource";
import resolveWithPermission from "../resolveWithPermission";
import DonutModel from "../types/DonutModel";
import camelCase from "lodash/camelCase";
import { knex } from "../../repo/knex";
import DeploymentResolver from "./DeploymentResolver";

const singleResource = <T>(model: DonutModel<T>, preload?: any[]) =>
  resolveWithPermission(
    async (_: any, { id }: { id: any }) =>
      model.findByPk(id, preload && { include: preload }),
    model,
    `view${model.name}`
  );

const singleResourceKnex = <T>(model: DonutModel<T>) => {
  return resolveWithPermission(
    async (_: any, { id }: { id: any }) => {
      return knex().table(model.tableName).where({ id }).first();
    },
    model,
    `view${model.name}`
  );
};

const all = <T>(model: DonutModel<T>, preload?: any[]) => (
  _: any,
  { ids }: { ids?: any },
  { user }: any,
  info: any
) => {
  const resourceName = Permissions.resolveActionName(info);
  if (user && Permissions.can(user, camelCase(`list_${resourceName}`)))
    return model.findAll(ids && { where: { id: ids }, include: preload });
  else return null;
};

const QueryResolver: IResolverObject = {
  algorithm: singleResource(Algorithm, ["modelTypes"]),
  algorithms: all(Algorithm),
  dataset: singleResourceKnex(Dataset),
  datasets: all(Dataset),
  dataType: singleResource(DataType, ["usages"]),
  dataTypes: all(DataType),
  efficiencyType: singleResource(EfficiencyType),
  efficiencyTypes: all(EfficiencyType),
  format: singleResource(Format),
  formats: all(Format),
  evaluationType: singleResource(EvaluationType),
  evaluationTypes: all(EvaluationType),
  hello: () => "Hello world!",
  model: (_, { id }, { user }) => Model.fetchOneWithAssocs(id, user),
  modelType: singleResource(ModelType),
  modelTypes: all(ModelType),
  paginateAlgorithms: paginateResource(Algorithm),
  paginateDataTypes: paginateResource(DataType),
  paginateDatasets: paginateResource(Dataset),
  paginateEfficiencyTypes: paginateResource(EfficiencyType),
  paginateEvaluationTypes: paginateResource(EvaluationType),
  paginateFormats: paginateResource(Format),
  paginateModels: paginateResource(Model),
  paginateModelTypes: paginateResource(ModelType),
  paginateUsages: paginateResource(Usage),
  paginateUsers: paginateResource(User),
  usage: singleResource(Usage, ["modelTypes"]),
  usages: all(Usage),
  user: (_, { id }, { user }) => {
    if (id && Permissions.can(user, "viewUser", { id })) {
      return User.findByPk(id);
    } else if (id) {
      return null;
    }
    return user;
  },
  users: all(User),
  whoIsMyAdmin: () => User.getAdmin(),
  summaryMap: (_, { id, typeId, type }) => {
    return ModelType.summaryMap(id, type, typeId);
  },
  projectData: DeploymentResolver.projectData,
  services: DeploymentResolver.services
};

export default QueryResolver;
