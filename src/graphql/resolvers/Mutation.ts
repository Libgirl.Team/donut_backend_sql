import { IResolverObject } from "graphql-tools";
import Algorithm from "../../repo/models/Algorithm";
import Dataset from "../../repo/models/Dataset";
import Model from "../../repo/models/Model";
import ModelType from "../../repo/models/ModelType";
import Usage from "../../repo/models/Usage";
import User from "../../repo/models/User";
import DataType from "../../repo/models/DataType";
import Format from "../../repo/models/Format";
import EfficiencyType from "../../repo/models/EfficiencyType";
import EvaluationType from "../../repo/models/EvaluationType";
import resolveWithPermission from "../resolveWithPermission";
import resolveWithTransformer from "../resolveWithTransformer";
import ModelMutations from "./ModelMutations";
import UserMutations from "./UserMutations";
import JSONImport from "../../repo/helpers/jsonImport";
import DonutModel from "../types/DonutModel";
import DeploymentResolver from "./DeploymentResolver";

const createResource = <T>(model: DonutModel<T>) => {
  return resolveWithPermission(
    resolveWithTransformer(async (_, { params }, { user }) => {
      const resource = await model?.createWithUser?.(user, params);
      return { success: true, resource };
    }),
    model
  );
};

const deleteResource = <T>(model: DonutModel<T>) => {
  return resolveWithPermission(
    resolveWithTransformer(async (_, _args, { resource }) => {
      await resource.destroy({ cascade: true });
      return { success: true };
    }),
    model
  );
};

const importResource = (model: any) => {
  return resolveWithPermission(async (_, { file }, { user }) => {
    return JSONImport.import(user, model, file);
  }, model);
};

const updateResource = <T>(model: DonutModel<T>) => {
  return resolveWithPermission(
    resolveWithTransformer(async (_, { params }, { resource }) => {
      const updated = await resource.update(params);
      return { success: true, resource: updated };
    }),
    model
  );
};

const MutationResolver: IResolverObject = {
  updatePreferences: async (_, { preferences }, { user }) => {
    try {
      await user.updatePreferences(preferences);
      return { success: true };
    } catch (e) {
      return { success: false };
    }
  },
  createAlgorithm: createResource(Algorithm),
  createDataset: createResource(Dataset),
  createDataType: createResource(DataType),
  createEfficiencyType: createResource(EfficiencyType),
  createFormat: createResource(Format),
  createEvaluationType: createResource(EvaluationType),
  createModelType: createResource(ModelType),
  createModel: resolveWithPermission(createResource(Model), Model),
  createUsage: createResource(Usage),
  deleteAlgorithm: deleteResource(Algorithm),
  deleteDataset: deleteResource(Dataset),
  deleteDataType: deleteResource(DataType),
  deleteEvaluationType: deleteResource(EvaluationType),
  deleteEfficiencyType: deleteResource(EfficiencyType),
  deleteFormat: deleteResource(Format),
  deleteModelType: deleteResource(ModelType),
  deleteUsage: deleteResource(Usage),
  deleteUser: deleteResource(User),
  deployModel: DeploymentResolver.deployModel,
  importAlgorithms: importResource(Algorithm),
  importDatasets: importResource(Dataset),
  importDataTypes: importResource(DataType),
  importModels: importResource(Model),
  importModelTypes: importResource(ModelType),
  importEvaluationTypes: importResource(EvaluationType),
  importEfficiencyTypes: importResource(EfficiencyType),
  importUsages: importResource(Usage),
  importFormats: importResource(Format),
  updateAlgorithm: updateResource(Algorithm),
  updateDataset: updateResource(Dataset),
  updateDataType: updateResource(DataType),
  updateEfficiencyType: updateResource(EfficiencyType),
  updateFormat: updateResource(Format),
  updateEvaluationType: updateResource(EvaluationType),
  updateModel: updateResource(Model),
  updateModelType: updateResource(ModelType),
  updateUsage: updateResource(Usage),
  updateUser: updateResource(User),
  ...ModelMutations,
  ...UserMutations
};

export default MutationResolver;
