import { gql } from "apollo-server";

export default gql`
  extend type Mutation {
    createEvaluationType(
      params: EvaluationTypeParams!
    ): EvaluationTypeMutationResponse!
    updateEvaluationType(
      id: ID!
      params: EvaluationTypeParams!
    ): EvaluationTypeMutationResponse!
    createEfficiencyType(
      params: EfficiencyTypeParams!
    ): EfficiencyTypeMutationResponse!
    updateEfficiencyType(
      id: ID!
      params: EfficiencyTypeParams!
    ): EfficiencyTypeMutationResponse!
    deleteEfficiencyType(id: ID!): EfficiencyTypeMutationResponse!
    deleteEvaluationType(id: ID!): EvaluationTypeMutationResponse!
    importEfficiencyTypes(file: Upload!): ImportResponse!
    importEvaluationTypes(file: Upload!): ImportResponse!
  }

  extend type Query {
    evaluationType(id: ID!): EvaluationType
    efficiencyType(id: ID!): EfficiencyType
    efficiencyTypes(ids: [ID!]): [EfficiencyType!]!
    evaluationTypes(ids: [ID!]): [EvaluationType!]!
    paginateEvaluationTypes(
      page: Int = 1
      sortOrder: SortOrder
      sortColumn: String
    ): EvaluationTypePage!
    paginateEfficiencyTypes(
      page: Int = 1
      sortOrder: SortOrder
      sortColumn: String
    ): EfficiencyTypePage!
  }

  type EvaluationTypePage {
    cursor: Cursor!
    data: [EvaluationType!]
  }

  type EfficiencyTypePage {
    cursor: Cursor!
    data: [EfficiencyType!]
  }

  input EvaluationTypeParams {
    name: String
    description: String
    unit: String
    datasetId: ID
  }

  input EfficiencyTypeParams {
    name: String
    description: String
    unit: String
  }

  type EvaluationTypeMutationResponse {
    success: Boolean!
    errors: JSON
    resource: EvaluationType
  }

  type EfficiencyTypeMutationResponse {
    success: Boolean!
    errors: JSON
    resource: EfficiencyType
  }

  interface EfficiencyEvaluationType {
    id: ID!
    name: String!
    unit: String
    description: String
    insertedAt: DateTime!
    updatedAt: DateTime!
  }

  interface EfficiencyEvaluation {
    id: ID!
    typeId: ID!
    type: EfficiencyEvaluationType!
    value: Float!
    insertedAt: DateTime!
    updatedAt: DateTime!
  }

  type EvaluationType implements EfficiencyEvaluationType {
    id: ID!
    name: String!
    unit: String
    description: String
    insertedAt: DateTime!
    updatedAt: DateTime!
    datasetId: ID!
    dataset: Dataset!
  }

  type EfficiencyType implements EfficiencyEvaluationType {
    id: ID!
    name: String!
    unit: String
    description: String
    insertedAt: DateTime!
    updatedAt: DateTime!
  }

  type Evaluation implements EfficiencyEvaluation {
    id: ID!
    typeId: ID!
    type: EvaluationType!
    value: Float!
    insertedAt: DateTime!
    updatedAt: DateTime!
  }

  type Efficiency implements EfficiencyEvaluation {
    id: ID!
    typeId: ID!
    type: EfficiencyType!
    value: Float!
    insertedAt: DateTime!
    updatedAt: DateTime!
  }

  input EfficiencyEvaluationParam {
    typeId: ID!
    value: Float!
  }
`;
