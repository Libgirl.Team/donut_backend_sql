import { gql } from "apollo-server";

export default gql`
  extend type Mutation {
    createUsage(params: UsageParams!): UsageMutationResponse!
    updateUsage(id: ID!, params: UsageParams!): UsageMutationResponse!
    deleteUsage(id: ID!): UsageMutationResponse
    importUsages(file: Upload!): ImportResponse!
  }

  extend type Query {
    usage(id: ID!): Usage
    usages: [Usage!]!
    paginateUsages(
      page: Int = 1
      sortOrder: SortOrder
      sortColumn: String
    ): UsagePage!
  }

  type UsagePage {
    cursor: Cursor!
    data: [Usage!]
  }

  type Usage {
    id: ID!
    description: String!
    name: String!
    inputTypeId: ID!
    outputTypeId: ID!
    inputType: DataType!
    outputType: DataType!
    ownerId: ID!
    owner: User!
    insertedAt: DateTime!
    updatedAt: DateTime!
    modelTypes: [ModelType!]!
  }

  input UsageParams {
    description: String
    name: String
    inputTypeId: ID
    outputTypeId: ID
  }

  type UsageMutationResponse {
    success: Boolean!
    resource: Usage
    errors: JSON
  }
`;
