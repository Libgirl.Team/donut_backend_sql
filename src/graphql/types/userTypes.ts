import { gql } from "apollo-server";

export default gql`
  extend type Mutation {
    updatePreferences(preferences: PreferencesInput): GenericMutationResponse!
    updateUser(params: UserParams!, id: ID!): UserMutationResponse!
    deleteUser(id: ID!): UserMutationResponse!
    deactivateUser(id: ID!): UserMutationResponse!
    activateUser(id: ID!): UserMutationResponse!
  }

  extend type Query {
    user(id: ID): User
    users: [User!]!
    paginateUsers(
      term: String
      page: Int = 1
      sortColumn: String
      sortOrder: SortOrder
    ): UserPage!
    whoIsMyAdmin: User
  }

  type UserPage {
    cursor: Cursor!
    data: [User!]!
  }

  type UserMutationResponse {
    success: Boolean!
    resource: User
    errors: JSON
  }

  input ModelPreferences {
    columns: [String!]!
    efficiencyTypeIDs: [ID!]!
    evaluationTypeIDs: [ID!]!
    sortColumn: String!
    sortOrder: String!
  }

  input PreferencesInput {
    models: ModelPreferences!
  }

  input UserParams {
    tmLevel: Int!
    dmLevel: Int!
    mmLevel: Int!
  }

  type User {
    id: ID!
    email: String!
    name: String!
    tmLevel: Int!
    dmLevel: Int!
    mmLevel: Int!
    picture: String
    preferences: JSON
    active: Boolean!
    deactivatedAt: DateTime
    insertedAt: DateTime!
    updatedAt: DateTime!
  }
`;
