import { gql } from "apollo-server";

export default gql`
  extend type Mutation {
    createDataset(params: DatasetParams!): DatasetMutationResponse!
    updateDataset(id: ID!, params: DatasetParams!): DatasetMutationResponse!
    deleteDataset(id: ID!): DatasetMutationResponse!
    importDatasets(file: Upload!): ImportResponse!
  }

  extend type Query {
    dataset(id: ID!): Dataset
    datasets: [Dataset!]!
    paginateDatasets(
      page: Int = 1
      sortOrder: SortOrder
      sortColumn: String
    ): DatasetPage!
  }

  type DatasetPage {
    cursor: Cursor!
    data: [Dataset!]
  }

  type Dataset {
    id: ID!
    datasetURI: String!
    description: String!
    name: String!
    ownerId: ID!
    owner: User!
    insertedAt: DateTime!
    updatedAt: DateTime!
    models: [Model!]!
  }

  input DatasetParams {
    datasetURI: String
    description: String
    name: String
  }

  type DatasetMutationResponse {
    success: Boolean!
    resource: Dataset
    errors: JSON
  }
`;
