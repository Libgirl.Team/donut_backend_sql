import { gql } from "apollo-server";

export default gql`
  extend type Mutation {
    createAlgorithm(params: AlgorithmParams!): AlgorithmMutationResponse!
    updateAlgorithm(
      id: ID!
      params: AlgorithmParams!
    ): AlgorithmMutationResponse!
    deleteAlgorithm(id: ID!): AlgorithmMutationResponse
    importAlgorithms(file: Upload!): ImportResponse!
  }

  type AlgorithmPage {
    cursor: Cursor!
    data: [Algorithm!]
  }

  extend type Query {
    algorithm(id: ID!): Algorithm
    algorithms: [Algorithm!]!
    paginateAlgorithms(
      page: Int = 1
      sortColumn: String
      sortOrder: SortOrder
    ): AlgorithmPage!
  }

  type Algorithm {
    id: ID!
    ownerId: ID!
    owner: User!
    name: String!
    description: String!
    insertedAt: DateTime!
    updatedAt: DateTime!
    modelTypes: [ModelType!]!
  }

  input AlgorithmParams {
    name: String
    description: String
  }

  type AlgorithmMutationResponse {
    success: Boolean!
    resource: Algorithm
    errors: JSON
  }
`;
