import { gql } from "apollo-server";

export default gql`
  extend type Query {
    projectData: ProjectData
    services: [Service!]
  }

  type Service {
    id: ID!
    name: String!
    url: String!
    insertedAt: DateTime!
  }

  type ProjectData {
    id: String!
  }
`;
