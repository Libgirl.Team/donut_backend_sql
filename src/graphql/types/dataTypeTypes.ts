import { gql } from "apollo-server";

export default gql`
  extend type Mutation {
    createDataType(params: DataTypeParams!): DataTypeMutationResponse!
    updateDataType(id: ID!, params: DataTypeParams!): DataTypeMutationResponse!
    deleteDataType(id: ID!): DataTypeMutationResponse
    importDataTypes(file: Upload!): ImportResponse!
  }

  extend type Query {
    dataType(id: ID!): DataType
    dataTypes: [DataType!]!
    paginateDataTypes(
      page: Int = 1
      sortOrder: SortOrder
      sortColumn: String
    ): DataTypePage!
  }

  type DataTypePage {
    cursor: Cursor!
    data: [DataType!]
  }

  input DataTypeParams {
    name: String
    description: String
  }

  type DataType {
    id: ID!
    description: String!
    name: String!
    ownerId: ID!
    owner: User!
    insertedAt: DateTime!
    updatedAt: DateTime!
    usages: [Usage!]!
  }

  type DataTypeMutationResponse {
    success: Boolean!
    resource: DataType
    errors: JSON
  }
`;
