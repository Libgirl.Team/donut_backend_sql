import { gql } from "apollo-server";

export default gql`
  extend type Mutation {
    createModel(params: ModelParams!): ModelMutationResponse!
    createModelType(params: ModelTypeParams!): ModelTypeMutationResponse!
    updateModelType(
      id: ID!
      params: ModelTypeParams!
    ): ModelTypeMutationResponse!
    updateModel(id: ID!, params: ModelParams!): ModelMutationResponse!
    setModelEfficiencies(
      id: ID!
      efficiencies: [EfficiencyEvaluationParam!]!
    ): ModelMutationResponse!
    setModelEvaluations(
      id: ID!
      evaluations: [EfficiencyEvaluationParam!]!
    ): ModelMutationResponse!
    deleteModelType(id: ID!): ModelTypeMutationResponse
    deleteModel(id: ID!): ModelMutationResponse
    deactivateModel(id: ID!): ModelMutationResponse
    activateModel(id: ID!): ModelMutationResponse
    virtuallyDelete(id: ID!): ModelMutationResponse
    importModelTypes(file: Upload!): ImportResponse!
    importModels(file: Upload!): ImportResponse!

    deployModel(id: ID!, serviceName: String!): ModelDeploymentResponse!
  }

  type ModelPage {
    cursor: Cursor!
    data: [Model!]
  }

  type ModelTypePage {
    cursor: Cursor!
    data: [ModelType!]
  }

  enum SummaryType {
    EVALUATION
    EFFICIENCY
  }

  enum SortOrder {
    ASC
    DESC
  }

  extend type Query {
    model(id: ID!): Model
    paginateModels(
      page: Int = 1
      term: String
      sortColumn: String
      sortOrder: SortOrder
    ): ModelPage!
    paginateModelTypes(
      page: Int = 1
      sortColumn: String
      sortOrder: SortOrder
    ): ModelTypePage!
    modelType(id: ID!): ModelType
    modelTypes(ids: [ID!]): [ModelType!]!
    summaryMap(id: ID!, typeId: ID!, type: SummaryType): JSON
  }

  type Model {
    id: ID!
    modelURI: String
    description: String
    displayName: String
    major: Int!
    minor: Int!
    version: String!
    ownerId: ID!
    owner: User!
    authorId: ID
    author: User
    parameters: String
    datasetId: ID!
    algorithm: Algorithm
    dataset: Dataset!
    evaluations: [Evaluation!]!
    evaluationMap: JSON!
    efficiencies: [Efficiency!]!
    efficiencyMap: JSON!
    modelTypeId: ID!
    modelType: ModelType!
    sourceModelId: ID
    sourceModel: Model
    sourceModelNotes: String
    formatId: ID!
    format: Format!
    deactivatedAt: DateTime
    insertedAt: DateTime!
    updatedAt: DateTime!
  }

  input ModelParams {
    modelTypeId: ID
    description: String
    authorId: ID
    datasetId: ID
    major: ID
    minor: ID
    modelURI: String
    sourceModelId: ID
    formatId: ID
    sourceModelNotes: String
  }

  input ModelTypeParams {
    name: String
    description: String
    algorithmId: ID
    usageId: ID
  }

  type ModelDeploymentResponse {
    success: Boolean!
  }

  type ModelTypeMutationResponse {
    success: Boolean!
    resource: ModelType
    errors: JSON
  }

  type ModelMutationResponse {
    success: Boolean!
    resource: Model
    errors: JSON
  }

  type ModelType {
    id: ID!
    name: String!
    description: String
    ownerId: ID!
    owner: User!
    algorithmId: ID!
    algorithm: Algorithm
    usageId: ID!
    usage: Usage!
    insertedAt: DateTime!
    updatedAt: DateTime!
    models: [Model!]!
    versionMap: JSON
    versions: [String!]!
  }
`;
