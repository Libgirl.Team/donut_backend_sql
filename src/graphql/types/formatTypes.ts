import { gql } from "apollo-server";

export default gql`
  type Format {
    id: ID!
    ownerId: ID!
    owner: User!
    name: String!
    description: String
    deployable: Boolean!
    insertedAt: DateTime!
    updatedAt: DateTime!
    models: [Model!]!
  }

  input FormatParams {
    name: String
    description: String
  }

  type FormatPage {
    cursor: Cursor!
    data: [Format!]
  }

  type FormatMutationResponse {
    success: Boolean!
    resource: Format
    errors: JSON
  }

  extend type Query {
    format(id: ID!): Format
    formats: [Format!]!
    paginateFormats(
      page: Int = 1
      sortColumn: String
      sortOrder: SortOrder
    ): FormatPage!
  }

  extend type Mutation {
    createFormat(params: FormatParams!): FormatMutationResponse!
    updateFormat(id: ID!, params: FormatParams!): FormatMutationResponse!
    deleteFormat(id: ID!): FormatMutationResponse
    importFormats(file: Upload!): ImportResponse!
  }
`;
