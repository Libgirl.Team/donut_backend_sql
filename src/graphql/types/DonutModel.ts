import { ModelCtor, Model, FindOptions } from "sequelize/types";
import User from "../../repo/models/User";
import { FileUpload } from "graphql-upload";

export interface PaginationParams {
  term?: string;
  page: number;
  sortOrder?: any;
  sortColumn?: string;
}

export interface Cursor {
  itemsPerPage: number;
  page: number;
  totalPages: number;
}

export interface PaginationPage<T> {
  cursor: Cursor;
  data: T[];
}

export default interface DonutModel<T> extends ModelCtor<Model<any, any>> {
  createWithUser?(user: User, params: any): Promise<T>;
  paginate?(user: User, params: PaginationParams): Promise<any>;
  importFromJSON?(user: User, file: Promise<FileUpload>): Promise<any>;
  exportAll?(opts?: FindOptions): Promise<any[]>;
}
