import DonutModel, { PaginationParams } from "./types/DonutModel";
import Permissions from "../shared/permissions";
import { ACCESS_DENIED_RESPONSE } from "./resolveWithPermission";
import { knex } from "../repo/knex";
import snakeCase from "lodash/snakeCase";

function normalizeOrder(order: any) {
  if (!order || order === "desc" || order === "DESC") return "DESC NULLS LAST";
  return "ASC";
}

export async function paginate<T>(
  model: DonutModel<T>,
  { page, sortOrder, sortColumn }: PaginationParams
) {
  let baseQuery = knex().table(model.tableName);
  const [{ count }] = await baseQuery.clone().count();
  const orderClause = `${
    snakeCase(sortColumn) || "inserted_at"
  } ${normalizeOrder(sortOrder)}`;
  let data = await baseQuery
    .orderByRaw(orderClause)
    .limit(DEFAULT_PER_PAGE)
    .offset((page - 1) * DEFAULT_PER_PAGE);
  return {
    cursor: {
      itemsPerPage: DEFAULT_PER_PAGE,
      page: page,
      totalPages: Math.ceil(count / DEFAULT_PER_PAGE)
    },
    data
  };
}

export const DEFAULT_PER_PAGE = 25;

const paginateResource = <T>(model: DonutModel<T>) => async (
  _obj: any,
  args: any,
  context: any,
  info: any
) => {
  const user = context?.user || null;
  if (Permissions.can(user, info)) {
    return model.paginate?.(user, args) || paginate(model, args);
  } else {
    const action = Permissions.resolveActionName(info);
    return ACCESS_DENIED_RESPONSE(action);
  }
};

export default paginateResource;
