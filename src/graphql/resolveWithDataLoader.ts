import snakeCase from "lodash/snakeCase";
const { EXPECTED_OPTIONS_KEY } = require("dataloader-sequelize");

export const resolvePluralWithDataLoader = (key: any, model: any) => (
  parent: any,
  _args: any,
  { loader }: any
) => {
  const resource = parent[key];
  if (resource) return resource;
  return model.findAll({
    where: { [key]: parent.id },
    [EXPECTED_OPTIONS_KEY]: loader
  });
};

const resolveWithDataLoader = (key: any, model: any) => (
  parent: any,
  _args: any,
  { loader }: any
) => {
  const resource = parent[key];
  if (resource) return resource;
  const keyId = `${key}Id`;
  const resourceId = parent[keyId] || parent[snakeCase(keyId)];
  return model.findByPk(resourceId, { [EXPECTED_OPTIONS_KEY]: loader });
};
export default resolveWithDataLoader;
