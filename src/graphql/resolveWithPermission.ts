import { IFieldResolver } from "graphql-tools";
import Permissions from "../shared/permissions";
import DonutModel from "./types/DonutModel";

export const NOT_FOUND_RESPONSE = {
  success: false,
  errors: { msg: "Record not found." }
};

export const ACCESS_DENIED_RESPONSE = (action: string | null | undefined) => ({
  success: false,
  errors: {
    msg: "You are not allowed to perform this action.",
    action
  }
});

const resolveWithPermission = <T>(
  resolver: IFieldResolver<any, any, any>,
  model: DonutModel<T>,
  actionName?: string
) => async (obj: any, args: any, context: any, info: any) => {
  const user = context?.user || null;
  const action = actionName || Permissions.resolveActionName(info);
  if (action?.match(/^view/) && Permissions.can(user, action)) {
    return resolver(obj, args, context, info);
  }
  const resourceId = args?.id;
  const resource = resourceId && (await model.findByPk(resourceId));
  if (!resource && !action?.match(/^(create|import)/))
    return NOT_FOUND_RESPONSE;
  if (Permissions.can(user, info, resource)) {
    return resolver(obj, args, { ...context, resource }, info);
  } else {
    return ACCESS_DENIED_RESPONSE(action);
  }
};

export default resolveWithPermission;
