export default function transformErrors(err: any) {
  return (
    err.errors?.reduce((acc: object, val: any) => {
      const { path, message } = val;
      return {
        ...acc,
        [path]: message
      };
    }, {}) || {
      msg: err.message
    }
  );
}
