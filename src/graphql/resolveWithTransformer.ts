import { IFieldResolver } from "graphql-tools";
import transformErrors from "./transformErrors";

const resolveWithTransformer = (
  resolver: IFieldResolver<any, any, any>
) => async (obj: any, args: any, context: any, info: any) => {
  try {
    const res = await resolver(obj, args, context, info);
    return res;
  } catch (e) {
    return {
      success: false,
      errors: transformErrors(e)
    };
  }
};

export default resolveWithTransformer;
