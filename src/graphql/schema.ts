import { gql } from "apollo-server";
import modelTypes from "./types/modelTypes";
import efficiencyEvaluationTypes from "./types/efficencyEvaluationTypes";
import userTypes from "./types/userTypes";
import usageTypes from "./types/usageTypes";
import algorithmTypes from "./types/algorithmTypes";
import datasetTypes from "./types/datasetTypes";
import dataTypeTypes from "./types/dataTypeTypes";
import formatTypes from "./types/formatTypes";
import deploymentTypes from "./types/deploymentTypes";

export default gql`
  scalar Date
  scalar DateTime
  scalar JSON

  ${modelTypes}
  ${efficiencyEvaluationTypes}
  ${userTypes}
  ${algorithmTypes}
  ${usageTypes}
  ${datasetTypes}
  ${dataTypeTypes}
  ${formatTypes}
  ${deploymentTypes}

  type ImportResponse {
    success: Boolean!
    importCount: Int
    errors: JSON
  }

  type GenericMutationResponse {
    success: Boolean!
  }

  type Cursor {
    page: Int!
    itemsPerPage: Int!
    totalPages: Int!
  }

  type Mutation
  type Query {
    hello: String # no-op
  }
`;
