import config from "config";
import jwt from "jsonwebtoken";
import User from "../repo/models/User";

export interface ITokenPayload {
  sub: string | number;
}

export default class Token {
  static issue(user: User) {
    return jwt.sign({ sub: user.email }, config.get("session.jwtSigner"), {
      expiresIn: Number(config.get("session.maxAge")) / 1000
    });
  }

  static verify(token: string): Promise<null | ITokenPayload> {
    return new Promise(resolve => {
      jwt.verify(token, config.get("session.jwtSigner"), {}, (err, payload) => {
        if (err) resolve(null);
        resolve(payload as ITokenPayload);
      });
    });
  }
}
