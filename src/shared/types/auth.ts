// The management level as stored in the database
export enum ManagementLevel {
  Default = 0,
  User,
  Maintainer,
  Admin,
  Super
}

export interface User {
  id: number;
  mmLevel: ManagementLevel;
  dmLevel: ManagementLevel;
  tmLevel: ManagementLevel;
  mm_level: ManagementLevel;
  dm_level: ManagementLevel;
  tm_level: ManagementLevel;
  active: boolean;
  email: string;
  name: string;
  picture: string | null;
  insertedAt: string;
  updatedAt: string;
}
