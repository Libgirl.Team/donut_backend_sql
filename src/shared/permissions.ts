import { User, ManagementLevel } from "./types/auth";
import { GraphQLResolveInfo } from "graphql";

type ActionParam = GraphQLResolveInfo | string | null;

enum Perm {
  Allow = "allow",
  Deny = "deny",
  OnlyTMAdmin = "only_tm_admin",
  TMAdminAndCannotChangeSelf = "tm_admin_and_cannot_change_self",
  OnlyAdmin = "only_admin",
  Maintainer = "maintainer",
  OnlyOwnerOrAdmin = "only_owner",
  RegularUser = "regular_user",
  TMRegularUser = "tm_regular_user"
}
type PermissionResolutionFunction = (user: User, resource: any) => boolean;

const rules: { [k: string]: Perm | PermissionResolutionFunction } = {
  createService: Perm.Deny,
  createUser: Perm.Deny,
  exportService: Perm.Deny,
  "create*": Perm.RegularUser,
  updateUser: Perm.OnlyTMAdmin,
  "view*": Perm.RegularUser,
  "update*": Perm.OnlyOwnerOrAdmin,
  "import*": Perm.RegularUser,
  "export*": Perm.Maintainer,
  "setModel*": Perm.OnlyOwnerOrAdmin,
  listUsers: Perm.TMRegularUser,
  "list*": Perm.RegularUser,
  "paginate*": Perm.RegularUser,
  activateModel: Perm.OnlyOwnerOrAdmin,
  activateUser: Perm.TMAdminAndCannotChangeSelf,
  deactivateModel: Perm.OnlyOwnerOrAdmin,
  deactivateUser: Perm.TMAdminAndCannotChangeSelf,
  deleteUser: Perm.OnlyTMAdmin,
  deleteModel: Perm.OnlyAdmin,
  viewUser: (user: any, res) => {
    const level = user.tmLevel || user.tm_level;
    return (
      typeof user.id !== "undefined" &&
      (level >= ManagementLevel.Admin || user.id === res.id)
    );
  },
  "*": Perm.OnlyAdmin
};

const wildcardRules = Object.keys(rules).filter((r) => r.match(/\*/));

function testWildcard(str: string, rule: string) {
  const escapeRegex = (s: string) =>
    s.replace(/([.*+?^=!:${}()|[\]/\\])/g, "\\$1");
  return new RegExp(
    "^" + rule.split("*").map(escapeRegex).join(".*") + "$"
  ).test(str);
}

function matchRule(actionName: string) {
  if (rules[actionName]) return rules[actionName];
  const matchingWildcard = wildcardRules.find((rule) =>
    testWildcard(actionName, rule)
  );
  return rules[matchingWildcard || "*"];
}

function mmLevel(user: User) {
  return Number(user.mmLevel || user.mm_level);
}

function tmLevel(user: User) {
  return Number(user.tmLevel || user.tm_level);
}

export default class Permissions {
  static can(user: User | null, action: ActionParam, resource?: any) {
    const actionName = Permissions.resolveActionName(action);
    if (!actionName || !user) return false;
    const resolution = matchRule(actionName);
    switch (resolution) {
      case Perm.Allow:
        return true;
      case Perm.Deny:
        return false;
      case Perm.OnlyOwnerOrAdmin:
        const ownerId = resource?.ownerId || resource?.owner?.id;
        return (
          mmLevel(user) >= ManagementLevel.Admin ||
          String(user.id) === String(ownerId)
        );
      case Perm.OnlyAdmin:
        return mmLevel(user) >= ManagementLevel.Admin;
      case Perm.Maintainer:
        return mmLevel(user) >= ManagementLevel.Maintainer;
      case Perm.OnlyTMAdmin:
        return tmLevel(user) >= ManagementLevel.Admin;
      case Perm.TMAdminAndCannotChangeSelf:
        return (
          tmLevel(user) >= ManagementLevel.Admin &&
          String(user.id) !== String(resource.id)
        );
      case Perm.RegularUser:
        return mmLevel(user) > 0;
      case Perm.TMRegularUser:
        return tmLevel(user) > 0;
      default:
        return resolution(user, resource);
    }
  }

  static canCreate(user: User | null, typename: string) {
    return Permissions.can(user, `create${typename}`);
  }

  static canExport(user: User | null, typename: string) {
    return Permissions.can(user, `export${typename}`);
  }

  static canImport(user: User | null, typename: string) {
    return Permissions.can(user, `import${typename}`);
  }

  static canDeactivate(user: User | null, resource: any) {
    return (
      resource.__typename === "Model" &&
      Permissions.can(user, `deactivateModel`, resource)
    );
  }

  static canDelete(user: User | null, resource: any) {
    return Permissions.can(user, `delete${resource.__typename}`, resource);
  }

  static canUpdate(user: User | null, resource: any) {
    return Permissions.can(user, `update${resource.__typename}`, resource);
  }

  static resolveActionName(action: ActionParam) {
    if (!action) return null;
    if (typeof action === "string") return action;
    return action?.fieldName;
  }
}
