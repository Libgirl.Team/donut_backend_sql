import express from "express";
import cors from "cors";
import { knex } from "./repo/knex";
import logger from "morgan";
import helmet from "helmet";
import errorHandler from "errorhandler";
import gqlServer from "./graphql/server";
import fetchUser from "./middleware/fetchUser";
import cookieParser from "cookie-parser";
import OAuthController from "./controllers/OAuthController";
import { repo } from "./repo";
import Exporter from "./controllers/Exporter";

const app = express();

// Network settings
app.use(cors({ credentials: true, origin: "http://localhost:5000" }));
app.use(express.json());
app.use(express.static("public"));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(logger("dev"));
app.use(helmet());

app.set("port", process.env.PORT || 3000);

app.get("/api/oauth", OAuthController.callback);
app.use(fetchUser);
app.post("/api/logout", OAuthController.logout);

app.get("/api/export", Exporter);

gqlServer.applyMiddleware({
  app,
  path: "/api",
  cors: {
    origin: true,
    credentials: true
  }
});

// OAuth 2.0
app.use(errorHandler());

async function initialize() {
  await repo();
  await knex();
  app.listen(app.get("port"), () => {
    const port = app.get("port");
    const env = app.get("env");
    console.log(
      `The application is running at http://localhost:${port} in ${env} mode\n` +
        `Press CTRL-C to stop\n`
    );
  });
}

initialize();
