import { Request, Response, NextFunction } from "express";

export default function restrictAccess(
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (res.locals?.user) {
    next();
  } else {
    res.status(401).end();
  }
}
