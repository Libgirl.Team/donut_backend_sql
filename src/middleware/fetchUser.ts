import { Request, Response, NextFunction } from "express";
import User from "../repo/models/User";

export default async function fetchUser(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    res.locals.user = null;
    const token = req.cookies.access_token;
    const user = await User.findByJwt(token);
    if (user) {
      res.locals.user = user;
    }
  } catch (err) {
    console.error(err);
  }
  next();
}
