import axios from "axios";
import qs from "querystring";
import { Request, Response } from "express";
import config from "config";
import JWT from "../util/Token";
import User from "../repo/models/User";

var GCP_CLIENT_ID = config.get("oauth.client_id") as string;
var GCP_CLIENT_SECRET = config.get("oauth.client_secret") as string;

// google oauth2 api
const TOKEN_URI = "https://www.googleapis.com/oauth2/v4/token";
const USERINFO_URI = "https://www.googleapis.com/oauth2/v3/userinfo";
const REDIRECT_URI = config.get("oauth.redirect_uri") as string;

export default class OAuthController {
  static async getAccessToken(code: string) {
    const res = await axios(TOKEN_URI, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data: qs.stringify({
        client_id: GCP_CLIENT_ID,
        client_secret: GCP_CLIENT_SECRET,
        code,
        grant_type: "authorization_code",
        redirect_uri: REDIRECT_URI
      })
    });
    return res.data.access_token;
  }

  static async getUserInfo(token: string) {
    const res = await axios(USERINFO_URI, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
    return res.data;
  }

  static closeTab(res: Response) {
    res.set("Content-Type", "text/html");
    res
      .status(200)
      .send('<script>window.open("","_self").close();</script></html>');
  }

  static setAccessTokenCookie(user: User, res: Response) {
    res.cookie("access_token", JWT.issue(user), {
      maxAge: config.get("session.maxAge"),
      httpOnly: true,
      secure: config.get("session.secure")
    });
  }

  static async logout(_req: Request, res: Response) {
    res.clearCookie("access_token");
    return res.status(204).end();
  }

  static async callback(req: Request, res: Response) {
    const accessToken = await OAuthController.getAccessToken(
      req.query.code as string
    );
    const userInfo = await OAuthController.getUserInfo(accessToken);
    if (accessToken && userInfo.email && userInfo.name) {
      const user = await User.findOrCreateByOAuthData(userInfo);

      // null response means that there is no active user
      // or that a new user cannot be created
      if (!user) {
        res.clearCookie("access_token");
        return res.status(401).end();
      }
      OAuthController.setAccessTokenCookie(user, res);
      OAuthController.closeTab(res);
    } else {
      console.log("Failed to get user info from Google");
      res.status(500).send("Oauth Failed");
    }
  }
}
