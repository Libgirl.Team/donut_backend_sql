import { Request, Response } from "express";
import Permissions from "../shared/permissions";
import DonutModel from "../graphql/types/DonutModel";
import day from "dayjs";
import snakeCase from "lodash/snakeCase";
import { repo } from "../repo";

export default async function (req: Request, res: Response) {
  const user = res.locals.user;
  const typename = req.query.typename as string;
  if (!Permissions.can(user, `export${typename}`)) {
    return res.status(403).end();
  }
  const model = repo().models[typename] as DonutModel<any>;
  if (!model) {
    return res.status(422).end();
  }
  let data;
  if (typeof model.exportAll === "function") {
    data = await model.exportAll({ order: ["insertedAt"] });
  } else {
    data = await model.findAll({ order: ["insertedAt"] });
  }
  const dateString = day().format(`YYYYMMDD`);
  const filename = `${dateString}-${snakeCase(typename)}.json`;
  res.set("Content-Disposition", `attachment; filename=${filename}`);
  res.json({ typename: model.name, data });
}
