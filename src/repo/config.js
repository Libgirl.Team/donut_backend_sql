module.exports = {
  development: {
    username: "postgres",
    password: "postgres",
    database: "donut_dev",
    host: "127.0.0.1",
    dialect: "postgres"
  },
  test: {
    username: "postgres",
    password: "postgres",
    database: "donut_test",
    host: "127.0.0.1",
    dialect: "postgres"
  },
  production: {
    database: "donut_prod",
    host: "/var/run/postgresql", // Different defaults on macOS and Linux
    dialect: "postgres"
  }
};
