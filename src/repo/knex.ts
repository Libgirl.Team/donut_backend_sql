import Knex from "knex";
import config from "config";

// This file exists because I'm doing more complicated database
// queries using Knex rather than Sequelize. Sequelize seems to
// have been the wrong choice.

const dbConfig = config.get("dbConfig") as any;

const instance = Knex({
  client: "pg",
  connection: {
    host: dbConfig.host,
    user: dbConfig.user,
    database: dbConfig.database,
    password: dbConfig.password
  },
  debug: process.env.NODE_ENV !== "production"
});

console.log(`Knex will connect to database ${dbConfig.database}...`);
instance
  .raw("select 1")
  .then(() => {
    console.log("Knex connected");
  })
  .catch((err) => {
    console.error(`Knex failed to connect: ${err}`);
    process.exit(1);
  });

export const knex = () => instance;
