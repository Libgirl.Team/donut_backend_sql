import { Sequelize } from "sequelize-typescript";
import path from "path";
import config from "config";

interface DBConfig {
  user: string;
  password: string;
  host: string;
  database: string;
}

const conf = config.get("dbConfig") as DBConfig;

const instance = new Sequelize(conf.database, conf.user, conf.password, {
  host: conf.host,
  dialect: "postgres",
  models: [path.resolve(__dirname, "models")],
  logging: process.env.NODE_ENV === "test" ? false : console.log
});

instance.authenticate();

export const repo = () => instance;
