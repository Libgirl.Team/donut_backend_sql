"use strict";

module.exports = {
  up: async (queryInterface, Seq) => {
    await queryInterface.createTable("efficiency_types", {
      id: {
        type: Seq.UUID,
        primaryKey: true,
        defaultValue: Seq.fn("uuid_generate_v4")
      },
      name: {
        type: Seq.STRING,
        allowNull: false
      },
      description: {
        type: Seq.STRING
      },
      unit: {
        type: Seq.STRING
      },
      inserted_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      },
      updated_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      }
    });
    await queryInterface.addIndex("efficiency_types", ["name"], {
      unique: true
    });
  }
};
