const tables = [
  "usages",
  "datasets",
  "data_types",
  "evaluation_types",
  "formats",
  "efficiency_types",
  "model_types",
  "models",
  "algorithms"
];

module.exports = {
  up: async (queryInterface) => {
    tables.forEach(async (table) => {
      await queryInterface.sequelize.query(
        `alter table ${table} alter column description type text;`
      );
    });
  }
};
