module.exports = {
  up: async (queryInterface, Seq) => {
    await queryInterface.createTable("formats", {
      id: {
        type: Seq.UUID,
        primaryKey: true,
        defaultValue: Seq.fn("uuid_generate_v4")
      },
      name: {
        type: Seq.STRING,
        allowNull: false
      },
      description: {
        type: Seq.STRING
      },
      owner_id: {
        type: Seq.UUID,
        references: {
          model: "users",
          key: "id"
        },
        onUpdate: "cascade",
        onDelete: "cascade"
      },
      inserted_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      },
      updated_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      }
    });
    await queryInterface.addIndex("formats", ["owner_id"]);
    await queryInterface.addIndex("formats", ["name"], {
      unique: true
    });
    // I insert the query using raw SQL to bypass validation
    await queryInterface.sequelize.query(
      "insert into formats (id, name) values ('3f7a8451-a586-4cbb-b3a7-8845165376f5', 'Open Neural Network Exchange (*.onnx)')"
    );
    await queryInterface.addColumn("models", "format_id", {
      type: Seq.UUID,
      references: {
        model: "formats",
        key: "id"
      },
      defaultValue: "3f7a8451-a586-4cbb-b3a7-8845165376f5"
    });
    await queryInterface.addIndex("models", ["format_id"]);
  },

  down: async (queryInterface, _) => {
    await queryInterface.removeIndex("models", ["format_id"]);
    await queryInterface.removeColumn("models", "format_id");
    await queryInterface.dropTable("formats");
  }
};
