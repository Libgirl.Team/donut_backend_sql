"use strict";

module.exports = {
  up: async (queryInterface, Seq) => {
    await queryInterface.sequelize.query(
      `create extension if not exists "uuid-ossp" with schema public;`
    );
    await queryInterface.createTable("users", {
      id: {
        type: Seq.UUID,
        primaryKey: true,
        defaultValue: Seq.fn("uuid_generate_v4")
      },
      email: {
        type: Seq.STRING,
        allowNull: false
      },
      picture: Seq.STRING,
      name: {
        type: Seq.STRING,
        allowNull: false
      },
      deactivated_at: {
        type: Seq.DATE
      },
      tm_level: {
        type: Seq.SMALLINT,
        allowNull: false,
        defaultValue: 0
      },
      dm_level: {
        type: Seq.SMALLINT,
        allowNull: false,
        defaultValue: 0
      },
      mm_level: {
        type: Seq.SMALLINT,
        allowNull: false,
        defaultValue: 0
      },
      preferences: Seq.JSONB,
      inserted_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      },
      updated_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      }
    });
    return queryInterface.addIndex("users", ["email"], { unique: true });
  }
};
