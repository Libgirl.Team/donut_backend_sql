module.exports = {
  up: async (queryInterface, Seq) => {
    await queryInterface.createTable(
      "models",
      {
        id: {
          type: Seq.UUID,
          primaryKey: true,
          defaultValue: Seq.fn("uuid_generate_v4")
        },
        major: {
          type: Seq.INTEGER,
          allowNull: false
        },
        minor: {
          type: Seq.INTEGER,
          allowNull: false
        },
        description: {
          type: Seq.TEXT,
          allowNull: false
        },
        model_uri: {
          type: Seq.STRING,
          allowNull: false
        },
        parameters: {
          type: Seq.STRING
        },
        model_type_id: {
          type: Seq.UUID,
          references: {
            model: "model_types",
            key: "id"
          },
          onUpdate: "cascade",
          onDelete: "cascade"
        },
        owner_id: {
          type: Seq.UUID,
          references: {
            model: "users",
            key: "id"
          },
          onUpdate: "cascade",
          onDelete: "cascade"
        },
        author_id: {
          type: Seq.UUID,
          references: {
            model: "users",
            key: "id"
          },
          onUpdate: "cascade",
          onDelete: "cascade"
        },
        source_model_id: {
          type: Seq.UUID,
          references: {
            model: "models",
            key: "id"
          }
        },
        source_model_notes: {
          type: Seq.TEXT
        },
        dataset_id: {
          type: Seq.UUID,
          references: {
            model: "datasets",
            key: "id"
          },
          onUpdate: "cascade",
          onDelete: "cascade"
        },
        deactivated_at: {
          type: Seq.DATE
        },
        deleted_at: {
          type: Seq.DATE
        },
        inserted_at: {
          type: Seq.DATE,
          allowNull: false,
          defaultValue: Seq.fn("now")
        },
        updated_at: {
          type: Seq.DATE,
          allowNull: false,
          defaultValue: Seq.fn("now")
        }
      },
      {
        uniqueKeys: {
          version_unique: {
            fields: ["model_type_id", "major", "minor"]
          }
        }
      }
    );
    await queryInterface.addIndex("models", ["owner_id"]);
    await queryInterface.addIndex("models", ["model_type_id"]);
  }
};
