module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(`
    drop view if exists model_search_cache;
    `);
    await queryInterface.sequelize.query(`
    create view model_search_cache
    as select m.id as model_id, mt.name || ' ' || m.major || '.' || m.minor as name_version from models m join model_types mt on m.model_type_id = mt.id
    `);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(
      `drop view if exists model_search_cache;`
    );
  }
};
