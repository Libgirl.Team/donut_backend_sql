"use strict";

module.exports = {
  up: async (queryInterface, Seq) => {
    await queryInterface.createTable("datasets", {
      id: {
        type: Seq.UUID,
        primaryKey: true,
        defaultValue: Seq.fn("uuid_generate_v4")
      },
      name: {
        type: Seq.STRING,
        allowNull: false
      },
      dataset_uri: {
        type: Seq.STRING,
        allowNull: false
      },
      description: {
        type: Seq.TEXT,
        allowNull: false
      },
      owner_id: {
        type: Seq.UUID,
        references: {
          model: "users",
          key: "id"
        }
      },
      inserted_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      },
      updated_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      }
    });
    await queryInterface.addIndex("datasets", ["owner_id"]);
    await queryInterface.addIndex("datasets", ["name"], {
      unique: true
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex("datasets", ["name"]);
    return queryInterface.dropTable("datasets");
  }
};
