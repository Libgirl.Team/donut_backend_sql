"use strict";

module.exports = {
  up: async (queryInterface, Seq) => {
    await queryInterface.createTable("model_types", {
      id: {
        type: Seq.UUID,
        primaryKey: true,
        defaultValue: Seq.fn("uuid_generate_v4")
      },
      name: {
        type: Seq.STRING,
        allowNull: false
      },
      description: {
        type: Seq.TEXT,
        allowNull: false
      },
      owner_id: {
        type: Seq.UUID,
        allowNull: false,
        references: {
          model: "users",
          key: "id",
          onDelete: "cascade"
        }
      },
      algorithm_id: {
        type: Seq.UUID,
        allowNull: false,
        references: {
          model: "algorithms",
          key: "id"
        },
        onDelete: "cascade"
      },
      usage_id: {
        type: Seq.UUID,
        allowNull: false,
        references: {
          model: "usages",
          key: "id"
        },
        onDelete: "cascade"
      },
      inserted_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      },
      updated_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      }
    });
    await queryInterface.addIndex("model_types", ["owner_id"]);
    await queryInterface.addIndex("model_types", ["name"], {
      unique: true
    });
  }
};
