"use strict";

module.exports = {
  up: async (queryInterface, Seq) => {
    await queryInterface.createTable("evaluation_types", {
      id: {
        type: Seq.UUID,
        primaryKey: true,
        defaultValue: Seq.fn("uuid_generate_v4")
      },
      dataset_id: {
        type: Seq.UUID,
        references: {
          model: "datasets",
          key: "id"
        }
      },
      description: {
        type: Seq.STRING
      },
      unit: {
        type: Seq.STRING
      },
      name: {
        type: Seq.STRING,
        allowNull: false
      },
      inserted_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      },
      updated_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      }
    });
    await queryInterface.addIndex("evaluation_types", ["name"], {
      unique: true
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex("evaluation_types", ["name"]);
    return queryInterface.dropTable("evaluation_types");
  }
};
