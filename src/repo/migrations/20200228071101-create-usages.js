"use strict";

module.exports = {
  up: async (queryInterface, Seq) => {
    await queryInterface.createTable("usages", {
      id: {
        type: Seq.UUID,
        primaryKey: true,
        defaultValue: Seq.fn("uuid_generate_v4")
      },
      name: {
        type: Seq.STRING,
        allowNull: false
      },
      description: {
        type: Seq.STRING,
        allowNull: false
      },
      owner_id: {
        type: Seq.UUID,
        allowNull: false,
        references: {
          model: "users",
          key: "id"
        }
      },
      input_type_id: {
        type: Seq.UUID,
        allowNull: false,
        references: {
          model: "data_types",
          key: "id"
        }
      },
      output_type_id: {
        type: Seq.UUID,
        allowNull: false,
        references: {
          model: "data_types",
          key: "id"
        }
      },
      inserted_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      },
      updated_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      }
    });
    await queryInterface.addIndex("usages", ["owner_id"]);
    await queryInterface.addIndex("usages", ["name"], { unique: true });
  }
};
