"use strict";

module.exports = {
  up: async (queryInterface, Seq) => {
    await queryInterface.createTable("data_types", {
      id: {
        type: Seq.UUID,
        primaryKey: true,
        defaultValue: Seq.fn("uuid_generate_v4")
      },
      name: {
        type: Seq.STRING,
        allowNull: false
      },
      description: {
        type: Seq.STRING,
        allowNull: false
      },
      owner_id: {
        type: Seq.UUID,
        references: {
          model: "users",
          key: "id"
        }
      },
      inserted_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      },
      updated_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      }
    });
    await queryInterface.addIndex("data_types", ["owner_id"]);
    await queryInterface.addIndex("data_types", ["name"], { unique: true });
  }
};
