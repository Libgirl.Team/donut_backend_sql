"use strict";

module.exports = {
  up: (queryInterface, Seq) => {
    return queryInterface.createTable("evaluations", {
      id: {
        type: Seq.UUID,
        primaryKey: true,
        defaultValue: Seq.fn("uuid_generate_v4")
      },
      value: {
        type: Seq.DECIMAL,
        allowNull: false
      },
      model_id: {
        type: Seq.UUID,
        references: {
          model: "models",
          key: "id"
        },
        onUpdate: "cascade",
        onDelete: "cascade"
      },
      type_id: {
        type: Seq.UUID,
        references: {
          model: "evaluation_types",
          key: "id"
        },
        onUpdate: "cascade",
        onDelete: "cascade"
      },
      inserted_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      },
      updated_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("evaluations");
  }
};
