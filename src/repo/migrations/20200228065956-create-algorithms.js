module.exports = {
  up: async (queryInterface, Seq) => {
    await queryInterface.createTable("algorithms", {
      id: {
        type: Seq.UUID,
        primaryKey: true,
        defaultValue: Seq.fn("uuid_generate_v4")
      },
      name: {
        type: Seq.STRING,
        allowNull: false
      },
      description: {
        type: Seq.STRING,
        allowNull: false
      },
      owner_id: {
        type: Seq.UUID,
        references: {
          model: "users",
          key: "id"
        },
        onUpdate: "cascade",
        onDelete: "cascade"
      },
      inserted_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      },
      updated_at: {
        type: Seq.DATE,
        allowNull: false,
        defaultValue: Seq.fn("now")
      }
    });
    await queryInterface.addIndex("algorithms", ["owner_id"]);
    await queryInterface.addIndex("algorithms", ["name"], {
      unique: true
    });
  }
};
