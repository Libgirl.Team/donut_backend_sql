import { Op } from "sequelize";

export default (columns: string[], term?: string) => {
  if (!term) return null;
  const ilikeTerm = term.trim().replace(/%/g, "\\%").replace(/\s+/g, "%");
  const ilikeWithPercents = `%${ilikeTerm}%`;
  return {
    [Op.or]: columns.map((column) => ({
      [column]: { [Op.iLike]: ilikeWithPercents }
    }))
  };
};
