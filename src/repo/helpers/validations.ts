import { Column } from "sequelize-typescript";
import { ModelAttributeColumnOptions } from "sequelize/types";

export function RequiredString(
  opts?: Partial<ModelAttributeColumnOptions>
): Function {
  return Column({
    validate: {
      notEmpty: { msg: "can't be blank" },
      notNull: { msg: "can't be blank" }
    },
    allowNull: false,
    ...(opts || {})
  });
}

export function UniqueString(
  opts?: Partial<ModelAttributeColumnOptions>
): Function {
  return Column({
    validate: {
      notEmpty: { msg: "can't be blank" },
      notNull: { msg: "can't be blank" }
    },
    unique: { name: "name_unique", msg: "This name is already taken" },
    allowNull: false,
    ...(opts || {})
  });
}

