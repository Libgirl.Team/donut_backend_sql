import { Model, Transaction } from "sequelize/types";
import { FileUpload } from "graphql-upload";
import JSONStream from "JSONStream";
import { ReadStream } from "fs";
import User from "../models/User";
import ModelSchema from "../models/Model";
import transformErrors from "../../graphql/transformErrors";

interface ImportResponse {
  success: boolean;
  importCount: number;
  errors?: any;
}

async function parseUpload(upload: Promise<FileUpload>): Promise<any> {
  const { createReadStream } = await upload;
  const stream = createReadStream();
  return new Promise((resolve, _reject) => {
    const result: any[] = [];
    stream
      .pipe(JSONStream.parse("data"))
      .on("data", (data: any) => {
        result.push(data);
      })
      .on("end", () => {
        resolve(result[0]);
      });
  });
}

export default class JSONImport {
  static async import(
    user: User,
    model: typeof Model,
    upload: Promise<FileUpload>
  ): Promise<ImportResponse> {
    let importCount = 0;
    const transaction = (await model.sequelize?.transaction()) as Transaction;
    const parsed = await parseUpload(upload);
    try {
      const isModelModel = model.tableName === "models";
      for (let i = 0; i < parsed.length; i++) {
        // separate logic for models
        if (isModelModel) {
          await ModelSchema.importOneFromJSON(user, parsed[i], transaction);
        } else {
          await JSONImport.importOne(model, user, parsed[i], transaction);
        }
        importCount++;
      }
      await transaction?.commit();

      return {
        success: true,
        importCount
      };
    } catch (error) {
      await transaction?.rollback();
      return {
        success: false,
        importCount: 0,
        errors: {
          index: importCount,
          errors: transformErrors(error)
        }
      };
    }
  }

  static importOne(
    model: any,
    user: User,
    params: any,
    transaction: Transaction
  ) {
    return model.create({ ...params, ownerId: user.id }, { transaction });
  }
}
