import DataLoader from "dataloader";
import { knex } from "../knex";

export default () => {
  return new DataLoader(async (keys: any) => {
    const evaluations = await knex()
      .table("evaluations")
      .whereIn("model_id", keys);
    const mapped = evaluations.reduce((acc, ev: any) => {
      if (!acc[ev.model_id]) acc[ev.model_id] = {};
      acc[ev.model_id][ev.type_id] = ev.value;
      return acc;
    }, {});
    return keys.map((key: string) => mapped[key] || {});
  });
};
