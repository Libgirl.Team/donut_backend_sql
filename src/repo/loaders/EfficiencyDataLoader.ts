import DataLoader from "dataloader";
import { knex } from "../knex";

export default () => {
  return new DataLoader(async (keys: any) => {
    const efficiencies = await knex()
      .table("efficiencies")
      .whereIn("model_id", keys);
    const mapped = efficiencies.reduce((acc, eff: any) => {
      if (!acc[eff.model_id]) acc[eff.model_id] = {};
      acc[eff.model_id][eff.type_id] = eff.value;
      return acc;
    }, {});
    return keys.map((key: string) => mapped[key] || {});
  });
};
