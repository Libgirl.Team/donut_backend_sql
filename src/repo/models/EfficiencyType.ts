import {
  Table,
  Column,
  Model,
  CreatedAt,
  UpdatedAt,
  HasMany,
  DataType
} from "sequelize-typescript";
import Efficiency from "./Efficiency";
import { UniqueString } from "../helpers/validations";

interface IEfficiencyTypeParams {
  name: string;
  description: string;
  unit: string;
}

@Table({ underscored: true })
export default class EfficiencyType extends Model<EfficiencyType> {
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    primaryKey: true
  })
  id!: string;

  @CreatedAt
  insertedAt!: Date;
  @UpdatedAt
  updatedAt!: Date;

  @UniqueString()
  name!: string;

  @Column
  description?: string;

  @Column
  unit?: string;

  @HasMany(() => Efficiency, "typeId")
  efficiencies?: Efficiency[];

  __resolveType = () => "EfficiencyType";

  static async createWithUser(_user: any, params: IEfficiencyTypeParams) {
    return EfficiencyType.create(params);
  }
}
