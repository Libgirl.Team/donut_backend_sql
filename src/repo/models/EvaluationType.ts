import {
  Table,
  Column,
  ForeignKey,
  BelongsTo,
  Model,
  CreatedAt,
  UpdatedAt,
  HasMany,
  DefaultScope,
  DataType
} from "sequelize-typescript";
import Evaluation from "./Evaluation";
import Dataset from "./Dataset";
import { UniqueString, RequiredString } from "../helpers/validations";

interface IEvaluationTypeParams {
  name: string;
  description: string;
  unit: string;
  datasetId: number | string;
}

@DefaultScope(() => ({ include: ["dataset"] }))
@Table({ underscored: true })
export default class EvaluationType extends Model<EvaluationType> {
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    primaryKey: true
  })
  id!: string;

  @CreatedAt
  insertedAt!: Date;
  @UpdatedAt
  updatedAt!: Date;

  @UniqueString()
  name!: string;

  @Column
  description?: string;

  @Column
  unit?: string;

  @HasMany(() => Evaluation, "typeId")
  evaluations?: Evaluation[];

  @ForeignKey(() => Dataset)
  @RequiredString({ type: DataType.UUID })
  datasetId!: number;
  @BelongsTo(() => Dataset, "datasetId")
  dataset?: Dataset;

  __resolveType = () => "EvaluationType";

  static async createWithUser(_user: any, params: IEvaluationTypeParams) {
    return EvaluationType.create(params);
  }
}
