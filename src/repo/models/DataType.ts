import {
  Table,
  Model,
  Column,
  CreatedAt,
  UpdatedAt,
  NotNull,
  ForeignKey,
  BelongsTo,
  DefaultScope,
  HasMany
} from "sequelize-typescript";
import User from "./User";
import Usage from "./Usage";
import { RequiredString, UniqueString } from "../helpers/validations";

interface IDataTypeParams {
  name: string;
  description: string;
}

@DefaultScope(() => ({ order: ["id"] }))
@Table({ underscored: true })
export default class DataType extends Model<DataType> {
  @CreatedAt
  insertedAt!: Date;
  @UpdatedAt
  updatedAt!: Date;

  @UniqueString()
  name!: string;

  @RequiredString()
  description?: string;

  @NotNull
  @ForeignKey(() => User)
  @Column({ allowNull: false })
  ownerId!: number;
  @BelongsTo(() => User, "ownerId")
  owner?: User;

  @HasMany(() => Usage)
  usages?: Usage[];

  static async createWithUser(user: User, params: IDataTypeParams) {
    return DataType.create({ ...params, ownerId: user.id });
  }
}
