import {
  Table,
  Column,
  Model,
  BelongsTo,
  ForeignKey,
  PrimaryKey
} from "sequelize-typescript";
import ModelSchema from "./Model";

// NOTE: this table is actually a view, i.e. it is calculated in the database
// at runtime. It is only used to query models.

@Table({
  underscored: true,
  tableName: "model_search_cache",
  timestamps: false
})
export default class ModelSearchCache extends Model<ModelSearchCache> {
  @ForeignKey(() => ModelSchema)
  @PrimaryKey
  @Column
  modelId!: number;
  @BelongsTo(() => ModelSchema, "modelId")
  model?: ModelSchema;

  @Column
  nameVersion!: string;
}
