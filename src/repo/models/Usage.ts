import {
  Table,
  Column,
  ForeignKey,
  Model,
  HasMany,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  DefaultScope
} from "sequelize-typescript";
import User from "./User";
import DataType from "./DataType";
import { RequiredString, UniqueString } from "../../repo/helpers/validations";
import ModelType from "./ModelType";

interface IUsageParams {
  description: string;
  name: string;
  inputTypeId: number | string;
  outputTypeId: number | string;
}

@DefaultScope(() => ({
  order: ["id"],
  include: ["inputType", "outputType"]
}))
@Table({ underscored: true })
export default class Usage extends Model<Usage> {
  @CreatedAt
  insertedAt!: Date;
  @UpdatedAt
  updatedAt!: Date;

  @UniqueString()
  name!: string;

  @RequiredString()
  description?: string;

  @ForeignKey(() => User)
  @RequiredString()
  ownerId!: number;
  @BelongsTo(() => User, "ownerId")
  owner?: User;

  @ForeignKey(() => DataType)
  @RequiredString()
  inputTypeId!: number;
  @BelongsTo(() => DataType, "inputTypeId")
  inputType?: DataType;

  @ForeignKey(() => DataType)
  @RequiredString()
  outputTypeId!: number;
  @BelongsTo(() => DataType, "outputTypeId")
  outputType?: DataType;

  @HasMany(() => ModelType, "usageId")
  modelTypes?: ModelType[];

  static async createWithUser(
    user: User,
    params: IUsageParams,
    options?: object
  ) {
    return Usage.create({ ...params, ownerId: user.id }, options);
  }
}
