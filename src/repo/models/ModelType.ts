import {
  Table,
  Column,
  ForeignKey,
  Model,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  HasMany,
  DefaultScope,
  Sequelize
} from "sequelize-typescript";
import { QueryTypes, Transaction } from "sequelize";

import User from "./User";
import ModelSchema from "./Model";
import Algorithm from "./Algorithm";
import Usage from "./Usage";
import { RequiredString, UniqueString } from "../helpers/validations";
import { literal } from "sequelize";

@DefaultScope(() => ({
  order: ["id"]
}))
@Table({ underscored: true })
export default class ModelType extends Model<ModelType> {
  @CreatedAt
  insertedAt!: Date;

  @UpdatedAt
  updatedAt!: Date;

  @UniqueString()
  name!: string;

  @RequiredString()
  description?: string;

  @ForeignKey(() => User)
  @Column({ allowNull: false })
  ownerId!: number;

  @BelongsTo(() => User, "ownerId")
  owner?: User;

  @ForeignKey(() => Algorithm)
  @Column({ allowNull: false })
  algorithmId!: number;

  @BelongsTo(() => Algorithm, "algorithmId")
  algorithm?: User;

  @ForeignKey(() => Usage)
  @Column({ allowNull: false })
  usageId!: number;

  @BelongsTo(() => Usage, "usageId")
  usage?: Usage;

  @HasMany(() => ModelSchema, "modelTypeId")
  models?: ModelSchema[];

  static async getExistingVersions(
    id: number | string,
    transaction?: Transaction
  ) {
    const versions = await ModelSchema.findAll({
      where: { modelTypeId: id },
      attributes: [[literal(`major || '.' || minor`), "version"]],
      transaction
    });
    return versions.map((m) => (m as any)?.dataValues?.version);
  }

  static async summaryMap(
    id: number,
    type: "EVALUATION" | "EFFICIENCY",
    typeId: number
  ) {
    const tableName = type === "EVALUATION" ? "evaluations" : "efficiencies";
    const res = await (ModelType.sequelize as Sequelize).query(
      `SELECT jsonb_build_object('id', m.major, 'data', jsonb_agg(jsonb_build_object('x', m.minor, 'y', e.value, 'label', m.major || '.' || m.minor) order by m.minor)) AS set
FROM models m JOIN model_types mt ON mt.id = m.model_type_id JOIN ${tableName} e ON e.model_id = m.id
WHERE mt.id = ? AND e.type_id = ? GROUP BY m.major`,
      { replacements: [id, typeId], type: QueryTypes.SELECT }
    );
    return res.map((r: any) => r.set);
  }

  // Batch loader for fetching maps of major-minor pairs
  // Used in resolver for ModelType.versionMap
  // It looks complicated but it could probably be much simpler in a language
  // with better query support, such as Elixir (KM 2020-03-27)
  static async getVersionMap(ids: any[]) {
    const [[{ res }]] =
      (await (ModelType.sequelize as Sequelize).query(
        `SELECT jsonb_object_agg(mtid, map) res FROM (SELECT model_type_id mtid, jsonb_object_agg(major, minor) map FROM
      (SELECT model_type_id, major, max(minor) AS minor FROM models GROUP BY model_type_id, major) s WHERE model_type_id in (?) GROUP BY 1) s2`,
        { replacements: [ids.filter(Boolean)] }
      )) as any;
    return ids.map((id) => res[id] || null);
  }

  static async createWithUser(
    user: User,
    params: typeof ModelType.rawAttributes
  ) {
    return ModelType.create({ ...params, ownerId: user.id });
  }
}
