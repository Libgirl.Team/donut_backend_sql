import {
  Table,
  Column,
  Model as DBModel,
  DefaultScope,
  NotNull,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
  BelongsTo,
  HasMany,
  HasOne,
  BeforeValidate,
  AfterValidate,
  DataType
} from "sequelize-typescript";
import User from "./User";
import Dataset from "./Dataset";
import Evaluation from "./Evaluation";
import Efficiency from "./Efficiency";
import ModelType from "./ModelType";
import { RequiredString } from "../helpers/validations";
import { Op, ModelValidateOptions, Transaction, FindOptions } from "sequelize";
import { DEFAULT_PER_PAGE } from "../../graphql/paginateResource";
import ModelSearchCache from "./ModelSearchCache";
import Format from "./Format";
import { knex } from "../knex";
import Knex from "knex";

interface EfficiencyEvaluationParam {
  value: number;
  typeId: number | string;
}

interface EfficiencyParam {
  value: number;
  typeId: number | string;
}

interface EvaluationParam {
  value: number;
  typeId: number | string;
}

export interface ModelParams {
  major: number;
  minor: number;
  modelTypeId: number | string;
  sourceModelId: number | string;
  authorId: number | string;
  description: string;
  modelURI: string;
  efficiencies: EfficiencyParam[];
  evaluations: EvaluationParam[];
}

async function majorMinorValidation(
  model: ModelValidateOptions,
  major: number | null
) {
  // Do not run this validator if major, minor, or modelTypeId is unset
  if (
    typeof major !== "number" ||
    typeof model.minor !== "number" ||
    !model.modelTypeId
  )
    return;
  const myVersion = `${major}.${model.minor}`;
  const transaction = model._validationTransaction as any;
  const existing = await ModelType.getExistingVersions(
    model.modelTypeId as number,
    transaction
  );
  if (!existing.length) {
    // When there are no existing versions, accept 1.0 and 0.0
    if (myVersion === "1.0" || myVersion === "0.0") return;
    throw new Error(
      "The first model for a new model type has to be numbered 0.0 or 1.0."
    );
  }
  if (myVersion === "0.0" && existing.indexOf("1.0") !== -1) {
    throw new Error("Version cannot be 0.0 if numbering started at 1.0.");
  }
  // from 1.0 upwards, major versions have to be consecutive
  if (major > 1 && existing.indexOf(`${major - 1}.0`) === -1) {
    throw new Error(
      `Major versions have to be consecutive. ${major - 1}.0 does not exist.`
    );
  }
  if (
    model.minor > 0 &&
    existing.indexOf(`${major}.${model.minor - 1}`) === -1
  ) {
    throw new Error(
      `Minor versions have to be consecutive. ${major}.${
        model.minor - 1
      } does not exist.`
    );
  }
}

async function validateSourceModel(
  model: ModelValidateOptions,
  sourceModelId: string | null
) {
  const modelTypeId = model.modelTypeId as string;
  if (!modelTypeId) return;
  const transaction = model._validationTransaction as any;
  const otherModelCount = await Model.count({
    where: { modelTypeId },
    transaction
  });
  if (!otherModelCount && !sourceModelId) return;
  if (!sourceModelId) {
    throw new Error(
      `You need to specify a source model, unless you're registering the first model for a model type.`
    );
  }
  const source = await Model.findOne({
    where: { modelTypeId, id: sourceModelId },
    transaction
  });
  if (!source) {
    throw new Error(`The source model does not belong to the same model type.`);
  }
  return;
}

@DefaultScope(() => ({ where: { deletedAt: null } }))
@Table({ underscored: true })
export default class Model extends DBModel<Model> {
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    primaryKey: true
  })
  id!: string;

  @CreatedAt
  insertedAt!: Date;
  @UpdatedAt
  updatedAt!: Date;

  @BeforeValidate
  static exposeTransaction(instance: any, options: any) {
    if (options.transaction) {
      instance._validationTransaction = options.transaction;
    }
  }

  @AfterValidate
  static deleteExposedTransaction(instance: any, _options: any) {
    delete instance._validationTransaction;
  }

  @Column
  deactivatedAt?: Date;

  @Column
  deletedAt?: Date;

  @RequiredString({ field: "model_uri" })
  modelURI!: string;

  @RequiredString()
  description!: string;

  @Column
  sourceModelNotes?: string;

  @ForeignKey(() => User)
  @RequiredString()
  ownerId!: number;
  @BelongsTo(() => User, "ownerId")
  owner?: User;

  @NotNull
  @RequiredString({
    allowNull: false,
    unique: "version_unique",
    validate: {
      majorMinorValidation(major: number | null) {
        return majorMinorValidation(this, major);
      }
    }
  })
  major!: number;

  @NotNull
  @Column({
    allowNull: false,
    unique: {
      name: "version_unique",
      msg: "This combination of major and minor version already exists."
    }
  })
  minor!: number;

  @ForeignKey(() => Model)
  @Column({
    type: DataType.UUID,
    validate: {
      sourceModelValidation(sourceModelId: string | null) {
        return validateSourceModel(this, sourceModelId);
      }
    }
  })
  sourceModelId!: string;
  @BelongsTo(() => Model, "sourceModelId")
  sourceModel?: Model;

  @ForeignKey(() => ModelType)
  @RequiredString({ unique: "version_unique" })
  modelTypeId!: number;
  @BelongsTo(() => ModelType, "modelTypeId")
  modelType?: ModelType;

  @ForeignKey(() => User)
  @Column
  authorId!: number;
  @BelongsTo(() => User, "authorId")
  author?: User;

  @ForeignKey(() => Format)
  @RequiredString({
    type: DataType.UUID,
    defaultValue: "3f7a8451-a586-4cbb-b3a7-8845165376f5"
  })
  formatId!: string;
  @BelongsTo(() => Format, "formatId")
  format?: Format;

  @Column
  parameters!: string;

  @ForeignKey(() => Dataset)
  @RequiredString({ type: DataType.UUID })
  datasetId!: number;
  @BelongsTo(() => Dataset, "datasetId")
  dataset?: Dataset;

  @HasMany(() => Evaluation, "modelId")
  evaluations?: Evaluation[];
  @HasMany(() => Efficiency, "modelId")
  efficiencies?: Efficiency[];

  @HasOne(() => ModelSearchCache, "modelId")
  modelSearchCache?: ModelSearchCache;

  // The documentation on updates with nested associations in Sequelize 5
  // was not clear enough, therefore I decided to reimplement analogous
  // functionality on my own. Sequelize is still a good ORM, compared to some
  // I have seen in my career. (KM 2020-03-18)
  async updateEfficiencies(params: EfficiencyEvaluationParam[]) {
    const efficiencies = params.map((e) => ({
      value: e.value,
      typeId: e.typeId,
      modelId: this.id
    }));
    await this.sequelize.transaction(async (transaction) => {
      await Efficiency.destroy({ where: { modelId: this.id }, transaction });
      await Efficiency.bulkCreate(efficiencies, {
        validate: true,
        transaction
      });
    });
    return Model.findByPk(this.id);
  }

  async updateEvaluations(params: EfficiencyEvaluationParam[]) {
    const evaluations = params.map((e) => ({
      value: e.value,
      typeId: e.typeId,
      modelId: this.id
    }));
    await this.sequelize.transaction(async (transaction) => {
      await Evaluation.destroy({ where: { modelId: this.id }, transaction });
      await Evaluation.bulkCreate(evaluations, {
        validate: true,
        transaction
      });
    });
    return Model.findByPk(this.id);
  }

  async deactivate() {
    if (!this.deactivatedAt) return this.update({ deactivatedAt: new Date() });
    return false;
  }

  // For reasons of data correctness, models cannot really be deleted, unless
  // the whole model type is deleted.
  // This function gets called when an administrator "Delete"s a Model in the
  // panel.
  async virtuallyDelete() {
    if (!this.deletedAt) {
      return this.update({ deletedAt: new Date() });
    }
  }

  async activate() {
    if (this.deactivatedAt) return this.update({ deactivatedAt: null });
  }

  async isDeployable() {
    const [{ deployable }] = await knex()
      .table("formats")
      .where({ id: this.formatId })
      .select("deployable");
    return deployable;
  }

  static async importOneFromJSON(
    user: User,
    params: ModelParams,
    transaction: Transaction
  ) {
    const model = await Model.createWithUser(user, params, transaction);
    if (params.efficiencies.length) {
      const efficiencies = params.efficiencies.map((e) => ({
        ...e,
        modelId: model.id
      }));
      await Efficiency.bulkCreate(efficiencies, {
        transaction,
        validate: true
      });
    }
    if (params.evaluations.length) {
      const evaluations = params.evaluations.map((e) => ({
        ...e,
        modelId: model.id
      }));
      await Evaluation.bulkCreate(evaluations, {
        transaction,
        validate: true
      });
    }
  }

  static async createWithUser(
    user: User,
    params: ModelParams,
    transaction?: Transaction
  ) {
    return Model.create(
      {
        ...params,
        ownerId: user.id
      },
      { transaction }
    );
  }

  static async exportAll(opts?: FindOptions) {
    const filter = { deactivatedAt: null, deletedAt: null };
    const res = await Model.findAll({
      ...opts,
      where: opts?.where ? { ...opts?.where, ...filter } : filter,
      include: ["efficiencies", "evaluations"]
    });
    const serializeEfficiencyOrEvaluation = (e: Efficiency | Evaluation) => {
      return {
        value: e.value,
        typeId: e.typeId
      };
    };
    return res.map((model: Model) => {
      return {
        id: model.id,
        modelTypeId: model.modelTypeId,
        modelURI: model.modelURI,
        description: model.description,
        major: model.major,
        minor: model.minor,
        ownerId: model.ownerId,
        authorId: model.authorId,
        datasetId: model.datasetId,
        sourceModelId: model.sourceModelId,
        formatId: model.formatId,
        insertedAt: model.insertedAt,
        updatedAt: model.updatedAt,
        efficiencies: model.efficiencies?.map(serializeEfficiencyOrEvaluation),
        evaluations: model.evaluations?.map(serializeEfficiencyOrEvaluation)
      };
    });
  }

  static orderByColumn(
    query: Knex.QueryBuilder<any, unknown[]>,
    sortColumn: string,
    sortOrder: "ASC" | "DESC" | null
  ) {
    let order = sortOrder || "desc";
    const match = sortColumn?.match(/^(efficiency|evaluation)\.([\w-]+)$/);
    if (match) {
      const type = match[1];
      const id = match[2];
      if (order === "desc" || order === "DESC") order = "desc nulls last";
      if (type === "evaluation") {
        return query
          .leftOuterJoin("evaluations", {
            "evaluations.model_id": "models.id",
            "evaluations.type_id": knex().raw("?", [id])
          })
          .orderByRaw(`evaluations.value ${order}`);
      }
      if (type === "efficiency") {
        return query
          .leftOuterJoin("efficiencies", {
            "efficiencies.model_id": "models.id",
            "efficiencies.type_id": knex().raw("?", [id])
          })
          .orderByRaw(`efficiencies.value ${order}`);
      }
    }

    switch (sortColumn) {
      case "modelType.name":
        return query
          .join("model_types", "models.model_type_id", "model_types.id")
          .orderBy("model_types.name", order);

      case "version":
        return query.orderBy([
          "model_type_id",
          { column: "models.major", order },
          { column: "models.minor", order }
        ]);

      case "owner.name":
        return query
          .join("users", "models.owner_id", "users.id")
          .orderBy("users.name", order);

      case "modelType.algorithm.name":
        return query
          .join("model_types", "models.model_type_id", "model_types.id")
          .join("algorithms", "model_types.algorithm_id", "algorithms.id")
          .orderBy("algorithms.name", order);

      default:
        return query.orderBy([{ column: sortColumn, order }]);
    }
  }

  static async paginate(
    user: User,
    { page, term, sortOrder, sortColumn }: any
  ) {
    let baseQuery = knex()
      .from("models")
      .join("model_search_cache", "model_search_cache.model_id", "models.id")
      .where({ "models.deleted_at": null });

    if (user.mmLevel < 3) {
      baseQuery = baseQuery.where((builder) => {
        builder
          .where({ "models.owner_id": user.id })
          .orWhere({ "models.deactivated_at": null });
      });
    }

    if (term) {
      baseQuery = baseQuery.andWhere((builder) => {
        const ilikeTerm = term.trim().replace(/%/g, "\\%").replace(/\s+/g, "%");
        const ilikeWithPercents = `%${ilikeTerm}%`;
        builder
          .where("model_search_cache.name_version", "ilike", ilikeWithPercents)
          .orWhere("models.description", "ilike", ilikeWithPercents);
      });
    }

    const [{ count }] = await baseQuery.clone().count("models.*");

    if (sortColumn) {
      baseQuery = this.orderByColumn(baseQuery, sortColumn, sortOrder);
    }

    const dataQuery = baseQuery
      .select("models.*")
      .limit(DEFAULT_PER_PAGE)
      .offset((page - 1) * DEFAULT_PER_PAGE);

    return {
      cursor: {
        itemsPerPage: DEFAULT_PER_PAGE,
        page: page,
        totalPages: Math.ceil(count / DEFAULT_PER_PAGE)
      },
      data: await dataQuery
    };
  }

  static async dataload(keys: string[]) {
    const models = await knex().table("models").where({ id: keys });
    return keys.map((key) => models.find((m) => m.id === key));
  }

  static async fetchOneWithAssocs(id: string, user?: User) {
    // Find one with the given id and either owned by the User
    // or not deactivated
    return Model.findOne({
      where: {
        [Op.and]: [
          { id },
          user && user?.mmLevel < 3
            ? {
                [Op.or]: [{ ownerId: user.id }, { deactivatedAt: null }]
              }
            : {}
        ]
      },
      include: ["evaluations", "efficiencies"]
    });
  }
}
