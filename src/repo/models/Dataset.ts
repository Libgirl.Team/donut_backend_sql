import {
  Table,
  Model,
  DefaultScope,
  Column,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
  BelongsTo,
  NotNull,
  HasMany,
  DataType
} from "sequelize-typescript";
import User from "./User";
import ModelSchema from "./Model";
import { UniqueString, RequiredString } from "../helpers/validations";

interface IDatasetParams {
  name: string;
  description: string;
  datasetURI: string;
}

@DefaultScope(() => ({ order: ["id"] }))
@Table({ underscored: true })
export default class Dataset extends Model<Dataset> {
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    primaryKey: true
  })
  id!: string;

  @CreatedAt
  insertedAt!: Date;
  @UpdatedAt
  updatedAt!: Date;

  @UniqueString()
  name!: string;

  @RequiredString({ field: "dataset_uri" })
  datasetURI!: string;

  @RequiredString()
  description!: string;

  @NotNull
  @ForeignKey(() => User)
  @Column({ allowNull: false })
  ownerId!: number;
  @BelongsTo(() => User, "ownerId")
  owner?: User;

  @HasMany(() => ModelSchema, "datasetId")
  models?: ModelSchema[];

  static async createWithUser(user: User, params: IDatasetParams) {
    return Dataset.create({ ...params, ownerId: user.id });
  }
}
