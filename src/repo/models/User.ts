import {
  Table,
  Column,
  Model,
  CreatedAt,
  UpdatedAt,
  Default,
  DataType,
  DefaultScope,
  BeforeUpdate
} from "sequelize-typescript";
import Token from "../../util/Token";
import { RequiredString } from "../helpers/validations";
import { DEFAULT_PER_PAGE } from "../../graphql/paginateResource";
import { knex } from "../knex";
import { PaginationParams } from "../../graphql/types/DonutModel";

export interface OAuthData {
  name: string;
  email: string;
  picture?: string;
}

@DefaultScope(() => ({ order: ["id"] }))
@Table({ underscored: true })
export default class User extends Model<User> {
  @RequiredString({
    validate: {
      isEmail: true
    },
    unique: true
  })
  email!: string;

  @RequiredString()
  name!: string;

  @Column
  picture?: string;

  @Column
  deactivatedAt?: Date;

  @Default(0)
  @Column({
    allowNull: false
  })
  tmLevel!: number;

  @Default(0)
  @Column({
    allowNull: false
  })
  mmLevel!: number;

  @Default(0)
  @Column({
    allowNull: false
  })
  dmLevel!: number;

  @Column({ type: DataType.JSONB })
  preferences?: any;

  @CreatedAt
  insertedAt!: Date;

  @UpdatedAt
  updatedAt!: Date;

  updatePreferences(preferences: any) {
    return this.update({ preferences });
  }

  static async getAdmin() {
    const [result] = await knex()
      .table("users")
      .whereRaw("deactivated_at is null and tm_level >= 3")
      .limit(1)
      .orderBy("id");
    return result;
  }

  static async paginate(
    user: User,
    { page, term, sortColumn, sortOrder }: PaginationParams
  ) {
    let baseQuery = knex().table("users");

    const tmLevel = Number((user as any).tm_level || user.tmLevel);

    if (tmLevel < 4) {
      baseQuery = baseQuery.whereNot({ tm_level: 4 });
    }
    if (tmLevel < 3) {
      baseQuery = baseQuery.andWhere({ deactivated_at: null });
    }

    if (term) {
      baseQuery = baseQuery.andWhere((builder) => {
        const ilikeTerm = term.trim().replace(/%/g, "\\%").replace(/\s+/g, "%");
        const ilikeWithPercents = `%${ilikeTerm}%`;
        builder
          .where("name", "ilike", ilikeWithPercents)
          .orWhere("email", "ilike", ilikeWithPercents);
      });
    }
    const column = sortColumn || "name";
    const order = sortOrder || "ASC";

    const [{ count }] = await baseQuery.clone().count();
    const data = await baseQuery
      .orderByRaw(`${column} ${order}`)
      .limit(DEFAULT_PER_PAGE)
      .offset((page - 1) * DEFAULT_PER_PAGE);

    return {
      cursor: {
        itemsPerPage: DEFAULT_PER_PAGE,
        page: page,
        totalPages: Math.ceil(count / DEFAULT_PER_PAGE)
      },
      data
    };
  }

  get active() {
    return !this.deactivatedAt;
  }

  async activate() {
    if (this.active) return false;
    return this.update({ deactivatedAt: null });
  }

  async deactivate() {
    if (!this.active) return false;
    return this.update({ deactivatedAt: new Date() });
  }

  @BeforeUpdate
  static updatePermissionLevels(instance: User) {
    if (instance.tmLevel === 3 || instance.tmLevel === 0) {
      instance.mmLevel = instance.tmLevel;
      instance.dmLevel = instance.tmLevel;
    }
  }

  static async isThereAnyAdmin() {
    return !!(await User.count({ where: { tmLevel: 3 } }));
  }

  static async findOrCreateByOAuthData(data: OAuthData) {
    const user = await User.findOne({ where: { email: data.email } });
    if (user?.active) {
      return user.update({ picture: data.picture, name: data.name });
    } else if (user) {
      return null;
    }

    if (await User.isThereAnyAdmin()) {
      return User.create({ ...data, tmLevel: 0, mmLevel: 0, dmLevel: 0 });
    } else {
      return User.create({ ...data, mmLevel: 3, tmLevel: 3, dmLevel: 3 });
    }
  }

  static findByEmail(email: string) {
    return User.findOne({ where: { email } });
  }

  static async findByJwt(token: string) {
    const payload = await Token.verify(token);
    if (!payload) return null;
    return knex().table("users").where({ email: payload.sub }).first();
  }
}
