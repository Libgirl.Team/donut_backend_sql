import {
  Table,
  Model,
  Column,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
  BelongsTo,
  DataType
} from "sequelize-typescript";
import EfficiencyType from "./EfficiencyType";
import ModelSchema from "./Model";

@Table({ underscored: true })
export default class Efficiency extends Model<Efficiency> {
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    primaryKey: true
  })
  id!: string;

  @CreatedAt
  insertedAt!: Date;
  @UpdatedAt
  updatedAt!: Date;

  @Column({ type: DataType.DECIMAL })
  value!: number;

  @ForeignKey(() => EfficiencyType)
  @Column({ allowNull: false, type: DataType.UUID })
  typeId!: string;
  @BelongsTo(() => EfficiencyType, "typeId")
  type?: EfficiencyType;

  @ForeignKey(() => ModelSchema)
  @Column({ allowNull: false, type: DataType.UUID })
  modelId!: string;
  @BelongsTo(() => ModelSchema, "modelId")
  model?: ModelSchema;
}
