import {
  Table,
  Column,
  DefaultScope,
  ForeignKey,
  Model,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  NotNull,
  HasMany
} from "sequelize-typescript";
import User from "./User";
import ModelType from "./ModelType";
import { UniqueString, RequiredString } from "../helpers/validations";

interface IAlgorithmParams {
  name: string;
  description: string;
}

@DefaultScope(() => ({ order: ["id"] }))
@Table({ underscored: true })
export default class Algorithm extends Model<Algorithm> {
  @CreatedAt
  insertedAt!: Date;
  @UpdatedAt
  updatedAt!: Date;

  @UniqueString()
  name!: string;

  @RequiredString()
  description!: string;

  @HasMany(() => ModelType, "algorithmId")
  modelTypes?: ModelType[];

  @NotNull
  @ForeignKey(() => User)
  @Column({ allowNull: false })
  ownerId!: number;
  @BelongsTo(() => User, "ownerId")
  owner?: User;

  static async createWithUser(
    user: User,
    params: IAlgorithmParams,
    options?: object
  ) {
    return Algorithm.create({ ...params, ownerId: user.id, ...options });
  }
}
