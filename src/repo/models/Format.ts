import {
  Model,
  Table,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
  BelongsTo,
  HasMany,
  Column,
  DefaultScope,
  DataType
} from "sequelize-typescript";
import { UniqueString } from "../helpers/validations";
import User from "./User";
import ModelSchema from "./Model";

interface FormatParams {
  name: string;
  description: string;
}

@DefaultScope(() => ({ order: ["id"] }))
@Table({ underscored: true })
export default class Format extends Model {
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    primaryKey: true
  })
  id!: string;

  @CreatedAt
  insertedAt!: Date;
  @UpdatedAt
  updatedAt!: Date;

  @UniqueString()
  name!: string;

  @Column
  description?: string;

  @Column
  deployable!: boolean;

  @ForeignKey(() => User)
  @Column
  ownerId?: number;
  @BelongsTo(() => User, "ownerId")
  owner?: User;

  @HasMany(() => ModelSchema, "formatId")
  models?: Model[];

  static async createWithUser(user: User, params: FormatParams) {
    return Format.create({ ...params, ownerId: user.id });
  }
}
