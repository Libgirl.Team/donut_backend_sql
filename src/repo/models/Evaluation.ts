import {
  Table,
  Model,
  Column,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
  DataType,
  BelongsTo
} from "sequelize-typescript";
import EvaluationType from "./EvaluationType";
import ModelSchema from "./Model";

@Table({ underscored: true })
export default class Evaluation extends Model<Evaluation> {
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    primaryKey: true
  })
  id!: string;

  @CreatedAt
  insertedAt!: Date;
  @UpdatedAt
  updatedAt!: Date;

  @Column({ type: DataType.DECIMAL })
  value!: number;

  @ForeignKey(() => EvaluationType)
  @Column({ allowNull: false, type: DataType.UUID })
  typeId!: number;
  @BelongsTo(() => EvaluationType, "typeId")
  type?: EvaluationType;

  @ForeignKey(() => ModelSchema)
  @Column({ allowNull: false, type: DataType.UUID })
  modelId!: number;
  @BelongsTo(() => ModelSchema, "modelId")
  model?: ModelSchema;
}
