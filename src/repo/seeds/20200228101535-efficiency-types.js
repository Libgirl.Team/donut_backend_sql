"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("efficiency_types", [
      {
        id: "6716984a-0864-4bf0-b872-6fd468c16b20",
        name: "Response time",
        description: "Response time by standard measurement in milliseconds.",
        unit: "ms"
      },
      {
        id: "02c17f9d-f535-421e-9ebf-7b2e68187ae7",
        name: "Memory usage",
        description: "Minimum memory required for the device to work smoothly.",
        unit: "MB"
      },
      {
        id: "3eabee02-9b26-4e1f-89e3-b9d5b55d9ff8",
        name: "GPU clock",
        description: "GPU clock!",
        unit: "GHz"
      },
      {
        id: "f143a46f-4ac8-4894-b6ac-4d52d1b94134",
        name: "Max detection num",
        description:
          "Maximum number of people that can be detected at the same time."
      },
      {
        id: "e1dc7756-c07d-446d-8f23-560afb8e1fa3",
        name: "Cost",
        unit: "USD/mo."
      }
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("efficiency_types", null, {});
  }
};
