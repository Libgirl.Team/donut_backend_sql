module.exports = {
  up: async (queryInterface, _) => {
    for (let m = 1; m < 3; m++)
      for (let i = 0; i < 5; i++) {
        await queryInterface.bulkInsert("models", [
          {
            owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e",
            description:
              "東京スカイツリー（とうきょうスカイツリー、英: TOKYO SKYTREE）は、東京都墨田区押上1-1-2にある電波塔（送信所）である。観光・商業施設やオフィスビルが併設されており、電波塔を含め周辺施設は「東京スカイツリータウン」と呼ばれている。2012年（平成24年）5月に電波塔・観光施設として開業した。",
            model_uri: "gs://donut-models/sample_model.zip",
            model_type_id: "2bdda142-621c-4071-8278-cec287870e9d",
            dataset_id: "ab487744-ec70-4c5d-9531-43712dc06d44",
            major: m,
            minor: i
          },
          {
            owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e",
            description:
              "通天閣（つうてんかく）は、大阪府大阪市浪速区の新世界中心部に建つ展望塔である。2007年5月15日に、国の登録有形文化財となった[1]。公式キャラクターは「ビリケン」。大阪の観光名所として知られる。",
            model_uri: "gs://donut-models/sample_model.zip",
            model_type_id: "8f5bb435-70a1-4a87-8b04-cf38a393c267",
            dataset_id: "8ac4c3b1-0dcb-4a31-a985-25cebfc1fabd",
            major: m,
            minor: i
          }
        ]);
      }
  },

  down: (queryInterface, _) => {
    return queryInterface.bulkDelete("models", null, {});
  }
};
