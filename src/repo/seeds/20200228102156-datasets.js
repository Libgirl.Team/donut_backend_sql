"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("datasets", [
      {
        id: "ab487744-ec70-4c5d-9531-43712dc06d44",
        description: `The CIFAR-10 dataset consists of 60000 32x32 colour images in 10 classes, with 6000 images per class. There are 50000 training images and 10000 test images.\n\nThe dataset is divided into five training batches and one test batch, each with 10000 images. The test batch contains exactly 1000 randomly-selected images from each class. The training batches contain the remaining images in random order, but some training batches may contain more images from one class than another. Between them, the training batches contain exactly 5000 images from each class.`,
        name: "CIFAR-10",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e",
        dataset_uri: "https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz"
      },
      {
        id: "8ac4c3b1-0dcb-4a31-a985-25cebfc1fabd",
        description: `This dataset is just like the CIFAR-10, except it has 100 classes containing 600 images each. There are 500 training images and 100 testing images per class. The 100 classes in the CIFAR-100 are grouped into 20 superclasses. Each image comes with a "fine" label (the class to which it belongs) and a "coarse" label (the superclass to which it belongs).`,
        name: "CIFAR-100",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e",
        dataset_uri: "https://www.cs.toronto.edu/~kriz/cifar-100-python.tar.gz"
      }
    ]);
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.bulkDelete("datasets", null, {});
  }
};
