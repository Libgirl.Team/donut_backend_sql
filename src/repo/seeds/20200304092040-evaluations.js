const { QueryTypes } = require("sequelize");

function randomWithBase(base) {
  const sign = Math.random() > 0.5 ? 1 : -1;
  const amount = ((Math.random() * 15 * base) / 100) * sign;
  const raw = base + amount;
  return Math.floor(raw * 1000) / 1000;
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const ids = await queryInterface.sequelize.query("select id from models;", {
      type: QueryTypes.SELECT
    });
    const types = await queryInterface.sequelize.query(
      "select id from evaluation_types;",
      {
        type: QueryTypes.SELECT
      }
    );
    ids.forEach(({ id: model_id }) => {
      types.forEach(async ({ id: type_id }) => {
        await queryInterface.bulkInsert("evaluations", [
          {
            model_id,
            type_id,
            value: randomWithBase(0.05)
          }
        ]);
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
