const { QueryTypes } = require("sequelize");

function randomWithBase(base) {
  const sign = Math.random() > 0.5 ? 1 : -1;
  const amount = ((Math.random() * 15 * base) / 100) * sign;
  const raw = base + amount;
  return Math.floor(raw * 1000) / 1000;
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const ids = await queryInterface.sequelize.query("select id from models;", {
      type: QueryTypes.SELECT
    });
    const types = await queryInterface.sequelize.query(
      "select id from efficiency_types;",
      {
        type: QueryTypes.SELECT
      }
    );

    ids.forEach(async ({ id }) => {
      await queryInterface.bulkInsert("efficiencies", [
        {
          model_id: id,
          type_id: types[0].id,
          value: randomWithBase(100)
        },
        {
          model_id: id,
          type_id: types[1].id,
          value: randomWithBase(1024)
        },
        { model_id: id, type_id: types[2].id, value: randomWithBase(3) },
        { model_id: id, type_id: types[3].id, value: randomWithBase(0.5) }
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
