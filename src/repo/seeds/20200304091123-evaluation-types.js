"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("evaluation_types", [
      {
        id: "fed24ecb-beef-462c-96a3-b91e1fa6858d",
        name: "f1@cifar10",
        description: "F1 score measurement for the CIFAR-10 dataset.",
        unit: "a.u.",
        dataset_id: "ab487744-ec70-4c5d-9531-43712dc06d44"
      },
      {
        id: "0fd11288-c1e1-496a-ac5d-29673098c623",
        name: "f1@cifar100",
        description: "F1 score measurement for the CIFAR-100 dataset.",
        unit: "a.u.",
        dataset_id: "8ac4c3b1-0dcb-4a31-a985-25cebfc1fabd"
      }
    ]);
  }
};
