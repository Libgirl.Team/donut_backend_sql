module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("model_types", [
      {
        id: "8f5bb435-70a1-4a87-8b04-cf38a393c267",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e",
        name: "Vatican",
        algorithm_id: "f2929499-1b5c-4c54-919e-70aeb448c3f8",
        description: "Predicting stonks",
        usage_id: "9917ced1-0105-4c0f-8b03-8a1c4bc4ee4b"
      },
      {
        id: "2bdda142-621c-4071-8278-cec287870e9d",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e",
        name: "SF Bay Area",
        algorithm_id: "1b8f5bbe-aab1-4ec2-96c0-6c1ee62542a1",
        description: "Yo momma so fat",
        usage_id: "de34d458-fb8a-4101-bf2c-72b2a400f568"
      }
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("model_types", null, {});
  }
};
