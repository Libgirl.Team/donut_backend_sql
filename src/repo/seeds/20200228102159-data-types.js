"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("data_types", [
      {
        id: "f68dc1e7-35f2-4952-8678-5549b1ed39d7",
        name: "Boolean",
        description: "Logical value with two states: true or false.",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e"
      },
      {
        id: "96bdf1a5-cf3c-4229-8937-faf7e7931ab2",
        name: "RGB Image",
        description: "Array of RGB tuples representing pixels of an image.",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e"
      },
      {
        id: "cfa37a71-0d90-4d3c-a1c8-42b9eeef09e1",
        name: "Grayscale Image",
        description: "Array of single-byte grayscale pixels.",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e"
      },
      {
        id: "611be30a-e6e3-4802-9232-a44cea39fc9c",
        name: "Text",
        description: "Ordered sequence of UTF-8 codepoints.",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e"
      },
      {
        id: "baa90d09-182f-4744-9de2-b99400c7501d",
        name: "Sentiment prediction",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e",
        description:
          "Floating-point number indicating sentiment of a body of text, in the range 1..5. The higher the number, the more positive the sentiment of the text."
      },
      {
        id: "b94cbe2a-f65f-4451-96af-98987e4d7288",
        name: "unsigned int",
        description: "Positive integral number.",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e"
      },
      {
        id: "543d548c-e917-483c-8fa1-e9c4608b1618",
        name: "Float",
        description: "Floating-point number.",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e"
      }
    ]);
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
