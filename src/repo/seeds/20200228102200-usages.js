"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("usages", [
      {
        id: "9917ced1-0105-4c0f-8b03-8a1c4bc4ee4b",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e",
        input_type_id: "96bdf1a5-cf3c-4229-8937-faf7e7931ab2",
        output_type_id: "f68dc1e7-35f2-4952-8678-5549b1ed39d7",
        name: "Chocolate recommendation",
        description:
          "Determine whether a given type of chocolate would be popular on a hypothetical market."
      },
      {
        id: "de34d458-fb8a-4101-bf2c-72b2a400f568",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e",
        input_type_id: "cfa37a71-0d90-4d3c-a1c8-42b9eeef09e1",
        output_type_id: "f68dc1e7-35f2-4952-8678-5549b1ed39d7",
        name: "Chocolate recommendation HK",
        description:
          "Determine whether a given type of chocolate would be popular on the Hong Kong market."
      },
      {
        id: "9a908c1c-c5eb-475d-a3f5-f03755e7fb17",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e",
        input_type_id: "611be30a-e6e3-4802-9232-a44cea39fc9c",
        output_type_id: "baa90d09-182f-4744-9de2-b99400c7501d",
        name: "Sentiment analysis",
        description:
          "Determine whether a given body of text is positive or negative in sentiment."
      }
    ]);
  }
};
