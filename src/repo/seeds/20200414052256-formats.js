module.exports = {
  up: async (queryInterface, _) => {
    const owner_id = "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e";
    await queryInterface.bulkInsert("formats", [
      { name: "PyTorch (*.pt, *.pth, *.tar, *.bin)", owner_id },
      { name: "Legacy Caffe (*.caffemodel)", owner_id },
      { name: "Caffe2 (*.pb)", owner_id },
      { name: "Keras (*.h5, *.keras)", owner_id }
    ]);
    await queryInterface.bulkInsert("formats", [
      {
        id: "e72aead9-933f-4b2a-8e72-326901ceeb20",
        name: "TensorFlow (*.pb)",
        owner_id,
        deployable: true
      }
    ]);
    await queryInterface.sequelize.query(
      `update formats set owner_id = '${owner_id}'`
    );
    // Set some models as TensorFlow
    await queryInterface.sequelize.query(
      `update models set format_id = 'e72aead9-933f-4b2a-8e72-326901ceeb20' where id in (select id from models order by random() limit 5);`
    );
  },

  down: (queryInterface, _) => {
    return queryInterface.bulkDelete("formats", {});
  }
};
