module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("users", [
      {
        id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e",
        email: "karol@libgirl.com",
        name: "Karol Moroz",
        tm_level: 4,
        mm_level: 4,
        dm_level: 4
      }
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("users", null, {});
  }
};
