"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("algorithms", [
      {
        id: "f2929499-1b5c-4c54-919e-70aeb448c3f8",
        name: "Resnet-18",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e",
        description:
          "CNN resnet-18. reference paper: https://arxiv.org/abs/1512.03385"
      },
      {
        id: "1b8f5bbe-aab1-4ec2-96c0-6c1ee62542a1",
        name: "Decision Tree",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e",
        description:
          "Decision tree: Explaining the Predictions of Any Image Classifier via Decision Trees. Reference: https://arxiv.org/abs/1911.01058"
      },
      {
        id: "cd9f9c59-f039-4ba7-b9b1-61c7d145efe3",
        name: "Alexnet",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e",
        description:
          "Alexnet: Finding AlexNet for Time Series Classification, reference paper: https://arxiv.org/abs/1512.03385"
      },
      {
        id: "fd26513a-97a7-43e9-8bf3-2ba8c42fccd5",
        name: "Naïve Bayes",
        owner_id: "5c6a1ff3-d422-4f50-8dea-3089ed9ccf3e",
        description:
          "Predict the category of a body of text using Bayes's theorem."
      }
    ]);
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
