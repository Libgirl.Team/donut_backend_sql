import { repo } from "../repo";
import { knex } from "../repo/knex";
import chai from "chai";
import chaiAsPromised from "chai-as-promised";
chai.use(chaiAsPromised);

(async () => {
  console.log("Syncing test database...");
  try {
    await repo().authenticate();
    await knex();
    run();
  } catch (err) {
    console.error("Database sync failed:", err);
    process.exit(1);
  }
})();
