import User from "../repo/models/User";
import Model from "../repo/models/Model";
import ModelType from "../repo/models/ModelType";
import Algorithm from "../repo/models/Algorithm";
import Dataset from "../repo/models/Dataset";
import Usage from "../repo/models/Usage";
import DataType from "../repo/models/DataType";
import Format from "../repo/models/Format";
import Chance from "chance";
const FactoryGirl = require("factory-girl");

const chance = new Chance();
const factory = FactoryGirl.factory;
const adapter = new FactoryGirl.SequelizeAdapter();

factory.setAdapter(adapter);

factory.define("user", User, {
  name: "Example User",
  email: factory.sequence("user-email", (n: any) => `user-${n}@example.com`),
  tmLevel: 3,
  mmLevel: 3,
  dmLevel: 3
});

factory.define("dataType", DataType, {
  name: factory.sequence("datatype-name", (n: number) => `data type ${n}`),
  description: "Some data type",
  ownerId: factory.assoc("user", "id")
});

factory.define("usage", Usage, {
  name: factory.sequence("sequence-name", (n: any) => `usage ${n}`),
  ownerId: factory.assoc("user", "id"),
  outputTypeId: factory.assoc("dataType", "id"),
  inputTypeId: factory.assoc("dataType", "id"),
  description: "Some usage description"
});

factory.define("algorithm", Algorithm, {
  ownerId: factory.assoc("user", "id"),
  description: chance.sentence(),
  name: factory.sequence("algorithm-name", (n: any) => `Algorithm ${n}`)
});

factory.define("dataset", Dataset, {
  ownerId: factory.assoc("user", "id"),
  name: factory.sequence("dataset-name", (n: number) => `dataset ${n}`),
  datasetURI: factory.sequence(
    "dataset-uri",
    (n: number) => `gs://bucket-name/dataset-${n}.bin`
  ),
  description: chance.sentence()
});

factory.define("modelType", ModelType, {
  ownerId: factory.assoc("user", "id"),
  name: factory.sequence("model-type-name", (n: any) => `Model Type ${n}`),
  algorithmId: factory.assoc("algorithm", "id"),
  usageId: factory.assoc("usage", "id"),
  description: chance.sentence()
});

factory.define("format", Format, {
  ownerId: factory.assoc("user", "id"),
  name: factory.sequence("format-name", (n: any) => `Format ${n}`)
});

factory.define("model", Model, {
  ownerId: factory.assoc("user", "id"),
  major: 1,
  minor: 0,
  modelTypeId: factory.assoc("modelType", "id"),
  datasetId: factory.assoc("dataset", "id"),
  formatId: factory.assoc("format", "id"),
  description: "My description",
  modelURI: factory.sequence(
    "model-uri",
    (n: number) => `gs://bucket/model-${n}.bin`
  )
});

export default factory;
