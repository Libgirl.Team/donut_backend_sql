import { expect } from "chai";

export async function expectValidationError(promise: Promise<any>) {
  await expect(promise).to.be.rejectedWith(Error);
}
