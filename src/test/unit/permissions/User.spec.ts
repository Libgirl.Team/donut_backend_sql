import "../../startTestSuite";
import { expect } from "chai";
import factory from "../../factories";
import Permissions from "../../../shared/permissions";

describe("User permissions", () => {
  it("Regular user can view oneself", async () => {
    const user = await factory.create("user", { tmLevel: 1 });
    const actual = Permissions.can(user, "viewUser", { id: user.id });
    expect(actual).to.eq(true);
  });

  it("TM Admin can view oneself", async () => {
    const user = await factory.create("user", { tmLevel: 3 });
    const actual = Permissions.can(user, "viewUser", { id: user.id });
    expect(actual).to.eq(true);
  });

  it("TM Admin can view other users", async () => {
    const admin = await factory.create("user", { tmLevel: 3 });
    const user = await factory.create("user", { tmLevel: 1 });
    const actual = Permissions.can(admin, "viewUser", { id: user.id });
    expect(actual).to.eq(true);
  });

  it("Regular user cannot view other regular user", async () => {
    const user = await factory.create("user", { tmLevel: 1 });
    const other = await factory.create("user", { tmLevel: 1 });
    const actual = Permissions.can(user, "viewUser", { id: other.id });
    expect(actual).to.eq(false);
  });

  it("Regular user cannot edit other regular user", async () => {
    const user = await factory.create("user", { tmLevel: 1 });
    const other = await factory.create("user", { tmLevel: 1 });
    const actual = Permissions.can(user, "updateUser", { id: other.id });
    expect(actual).to.eq(false);
  });

  it("Regular user cannot edit oneself", async () => {
    const user = await factory.create("user", { tmLevel: 1 });
    const actual = Permissions.can(user, "updateUser", { id: user.id });
    expect(actual).to.eq(false);
  });

  it("TM Admin can edit other users", async () => {
    const user = await factory.create("user", { tmLevel: 3 });
    const other = await factory.create("user", { tmLevel: 1 });
    const actual = Permissions.can(user, "updateUser", { id: other.id });
    expect(actual).to.eq(true);
  });

  it("TM Admin can edit oneself", async () => {
    const user = await factory.create("user", { tmLevel: 3 });
    const actual = Permissions.can(user, "updateUser", { id: user.id });
    expect(actual).to.eq(true);
  });

  it("Regular user cannot deactivate other regular user", async () => {
    const user = await factory.create("user", { tmLevel: 1 });
    const other = await factory.create("user", { tmLevel: 1 });
    const actual = Permissions.can(user, "deactivateUser", { id: other.id });
    expect(actual).to.eq(false);
  });

  it("Regular user cannot deactivate oneself", async () => {
    const user = await factory.create("user", { tmLevel: 1 });
    const actual = Permissions.can(user, "deactivateUser", { id: user.id });
    expect(actual).to.eq(false);
  });

  it("TM Admin can deactivate other users", async () => {
    const user = await factory.create("user", { tmLevel: 3 });
    const other = await factory.create("user", { tmLevel: 1 });
    const actual = Permissions.can(user, "deactivateUser", { id: other.id });
    expect(actual).to.eq(true);
  });

  it("TM Admin cannot deactivate oneself", async () => {
    const user = await factory.create("user", { tmLevel: 3 });
    const actual = Permissions.can(user, "deactivateUser", { id: user.id });
    expect(actual).to.eq(false);
  });

  it("Regular user cannot activate other regular user", async () => {
    const user = await factory.create("user", { tmLevel: 1 });
    const other = await factory.create("user", { tmLevel: 1 });
    const actual = Permissions.can(user, "activateUser", { id: other.id });
    expect(actual).to.eq(false);
  });

  it("Regular user cannot activate oneself", async () => {
    const user = await factory.create("user", { tmLevel: 1 });
    const actual = Permissions.can(user, "activateUser", { id: user.id });
    expect(actual).to.eq(false);
  });

  it("TM Admin can activate other users", async () => {
    const user = await factory.create("user", { tmLevel: 3 });
    const other = await factory.create("user", { tmLevel: 1 });
    const actual = Permissions.can(user, "activateUser", { id: other.id });
    expect(actual).to.eq(true);
  });

  it("TM Admin cannot activate oneself", async () => {
    const user = await factory.create("user", { tmLevel: 3 });
    const actual = Permissions.can(user, "activateUser", { id: user.id });
    expect(actual).to.eq(false);
  });

  it("Regular user can list users", async () => {
    const user = await factory.create("user", { tmLevel: 1 });
    const actual = Permissions.can(user, "listUsers");
    expect(actual).to.eq(true);
  });

  it("User with tmLevel = 0 cannot list users", async () => {
    const user = await factory.create("user", { tmLevel: 0 });
    const actual = Permissions.can(user, "listUsers");
    expect(actual).to.eq(false);
  });

  it("TM Admin can list users", async () => {
    const user = await factory.create("user", { tmLevel: 3 });
    const actual = Permissions.can(user, "listUsers");
    expect(actual).to.eq(true);
  });
});
