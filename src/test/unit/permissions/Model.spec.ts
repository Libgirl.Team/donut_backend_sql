import "../../startTestSuite";
import { expect } from "chai";
import factory from "../../factories";
import Permissions from "../../../shared/permissions";

describe("Model permissions", () => {
  it("null user cannot create Model", () => {
    const actual = Permissions.can(null, "createModel");
    expect(actual).to.equal(false);
  });

  it("User with default permissions cannot create Model", async () => {
    const user = await factory.create("user", { mmLevel: 0 });
    const actual = Permissions.can(user, "createModel");
    expect(actual).to.equal(false);
  });

  it("User with mmLevel >= 1 can create Model", async () => {
    const user = await factory.create("user", { mmLevel: 1 });
    const actual = Permissions.can(user, "createModel");
    expect(actual).to.equal(true);
  });

  it("User with mmLevel == 0 cannot delete Model", async () => {
    const user = await factory.create("user", { mmLevel: 0 });
    const actual = Permissions.can(user, "deleteModel");
    expect(actual).to.equal(false);
  });

  it("Regular user cannot deactivate other user's model", async () => {
    const user = await factory.create("user", { mmLevel: 1 });
    const otherUser = await factory.create("user", { mmLevel: 1 });
    const model = await factory.create("model", { ownerId: otherUser.id });
    const actual = Permissions.can(user, "deactivateModel", model);
    expect(actual).to.equal(false);
  });

  it("Regular user cannot activate other user's model", async () => {
    const user = await factory.create("user", { mmLevel: 1 });
    const otherUser = await factory.create("user", { mmLevel: 1 });
    const model = await factory.create("model", { ownerId: otherUser.id });
    const actual = Permissions.can(user, "activateModel", model);
    expect(actual).to.equal(false);
  });

  it("Owner can deactivate own model", async () => {
    const user = await factory.create("user", { mmLevel: 1 });
    const model = await factory.create("model", { ownerId: user.id });
    const actual = Permissions.can(user, "deactivateModel", model);
    expect(actual).to.equal(true);
  });

  it("Owner can update own model", async () => {
    const user = await factory.create("user", { mmLevel: 1 });
    const model = await factory.create("model", { ownerId: user.id });
    const actual = Permissions.can(user, "updateModel", model);
    expect(actual).to.equal(true);
  });

  it("Owner can activate own model", async () => {
    const user = await factory.create("user", { mmLevel: 1 });
    const model = await factory.create("model", {
      ownerId: user.id,
      deactivatedAt: new Date()
    });
    const actual = Permissions.can(user, "activateModel", model);
    expect(actual).to.equal(true);
  });

  it("Admin can deactivate any model", async () => {
    const user = await factory.create("user", { mmLevel: 1 });
    const admin = await factory.create("user", { mmLevel: 3 });
    const model = await factory.create("model", { ownerId: user.id });
    const actual = Permissions.can(admin, "deactivateModel", model);
    expect(actual).to.equal(true);
  });

  it("Admin can update any model", async () => {
    const user = await factory.create("user", { mmLevel: 1 });
    const admin = await factory.create("user", { mmLevel: 3 });
    const model = await factory.create("model", { ownerId: user.id });
    const actual = Permissions.can(admin, "updateModel", model);
    expect(actual).to.equal(true);
  });

  it("Admin can activate any model", async () => {
    const user = await factory.create("user", { mmLevel: 1 });
    const admin = await factory.create("user", { mmLevel: 3 });
    const model = await factory.create("model", {
      ownerId: user.id,
      deactivatedAt: new Date()
    });
    const actual = Permissions.can(admin, "activateModel", model);
    expect(actual).to.equal(true);
  });

  it("Regular user cannot delete own model", async () => {
    const user = await factory.create("user", { mmLevel: 1 });
    const model = await factory.create("model", { ownerId: user.id });
    const actual = Permissions.can(user, "deleteModel", model);
    expect(actual).to.equal(false);
  });

  it("Admin can delete Models", async () => {
    const admin = await factory.create("user", { mmLevel: 3 });
    const model = await factory.create("model");
    const actual = Permissions.can(admin, "deleteModel", model);
    expect(actual).to.equal(true);
  });

  it("User with default mmLevel cannot edit evaluations or efficiencies", async () => {
    const user = await factory.create("user", { mmLevel: 0 });
    const model = await factory.create("model");
    const canEff = Permissions.can(user, "setModelEfficiencies", model);
    const canEval = Permissions.can(user, "setModelEvaluations", model);
    expect(canEval).to.equal(false);
    expect(canEff).to.equal(false);
  });

  it("User with mmLevel >= 1 cannot edit evaluations or efficiencies on other users' models", async () => {
    const user = await factory.create("user", { mmLevel: 1 });
    const model = await factory.create("model");
    const canEff = Permissions.can(user, "setModelEfficiencies", model);
    const canEval = Permissions.can(user, "setModelEvaluations", model);
    expect(canEval).to.equal(false);
    expect(canEff).to.equal(false);
  });

  it("User with mmLevel >= 1 can edit evaluations or efficiencies on own models", async () => {
    const user = await factory.create("user", { mmLevel: 1 });
    const model = await factory.create("model", { ownerId: user.id });
    const canEff = Permissions.can(user, "setModelEfficiencies", model);
    const canEval = Permissions.can(user, "setModelEvaluations", model);
    expect(canEval).to.equal(true);
    expect(canEff).to.equal(true);
  });

  it("Admin can edit evaluations or efficiencies on any model", async () => {
    const user = await factory.create("user", { mmLevel: 3 });
    const model = await factory.create("model");
    const canEff = Permissions.can(user, "setModelEfficiencies", model);
    const canEval = Permissions.can(user, "setModelEvaluations", model);
    expect(canEval).to.equal(true);
    expect(canEff).to.equal(true);
  });
});
