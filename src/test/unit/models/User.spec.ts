import "../../startTestSuite";
import { expectValidationError } from "../../testHelpers";
import { expect } from "chai";
import factory from "../../factories";
import User from "../../../repo/models/User";

describe("User", () => {
  it("saves with correct parameters", async () => {
    return expect(factory.create("user")).to.be.fulfilled;
  });

  it("does not save with duplicate email", async () => {
    await factory.create("user", { email: "user@example.com" });
    const promise = factory.create("user", { email: "user@example.com" });
    expectValidationError(promise);
  });

  it("can have a duplicate display name", async () => {
    await factory.create("user", { name: "John Doe" });
    const promise = factory.create("user", { name: "John Doe" });
    return expect(promise).to.be.fulfilled;
  });

  it("updating a User with tmLevel === 3 makes him almighty", async () => {
    const user = await factory.create("user", {
      mmLevel: 1,
      tmLevel: 1,
      dmLevel: 1
    });
    expect(user.mmLevel).not.to.eq(3);
    expect(user.dmLevel).not.to.eq(3);
    expect(user.tmLevel).not.to.eq(3);
    await user.update({ tmLevel: 3 });
    expect(user.tmLevel).to.eq(3);
    expect(user.dmLevel).to.eq(3);
    expect(user.mmLevel).to.eq(3);
  });

  it("updating a User with tmLevel === 0 sets all permissions to 0", async () => {
    const user = await factory.create("user", {
      mmLevel: 1,
      tmLevel: 1,
      dmLevel: 1
    });
    expect(user.mmLevel).not.to.eq(0);
    expect(user.dmLevel).not.to.eq(0);
    expect(user.tmLevel).not.to.eq(0);
    await user.update({ tmLevel: 0 });
    expect(user.tmLevel).to.eq(0);
    expect(user.dmLevel).to.eq(0);
    expect(user.mmLevel).to.eq(0);
  });
});

describe("Creating new users on first login", () => {
  it(".isThereAnyAdmin returns false when there are no admins", async () => {
    await User.truncate({ cascade: true });
    const actual = await User.isThereAnyAdmin();
    expect(actual).to.eq(false);
  });

  it(".isThereAnyAdmin returns true when there is at least one admin", async () => {
    await User.truncate({ cascade: true });
    await factory.create("user", { tmLevel: 3, mmLevel: 3, dmLevel: 3 });
    const actual = await User.isThereAnyAdmin();
    expect(actual).to.eq(true);
  });

  it(".findOrCreateByOAuthData creates an ordinary user when there are admins", async () => {
    await User.truncate({ cascade: true });
    const admin = await factory.create("user", {
      tmLevel: 3,
      mmLevel: 3,
      dmLevel: 3,
      email: "example@libgirl.com"
    });
    const OAuthData = {
      email: "user@example.com",
      name: "Example User",
      picture: "https://picsum.photos/200"
    };
    const user = await User.findOrCreateByOAuthData(OAuthData);
    expect(user).not.to.eq(null);
    expect(user?.id).not.to.eq(admin.id);
    expect(user?.tmLevel).to.eq(0);
    expect(user?.mmLevel).to.eq(0);
    expect(user?.dmLevel).to.eq(0);
  });

  it(".findOrCreateByOAuthData creates an admin when there is no admin", async () => {
    await User.truncate({ cascade: true });
    const adminsPresent = await User.isThereAnyAdmin();
    expect(adminsPresent).to.eq(false);
    const OAuthData = {
      email: "user@example.com",
      name: "Example User",
      picture: "https://picsum.photos/200"
    };
    const user = await User.findOrCreateByOAuthData(OAuthData);
    expect(user?.tmLevel).to.eq(3);
    expect(user?.mmLevel).to.eq(3);
    expect(user?.dmLevel).to.eq(3);
  });
});
