import "../../startTestSuite";
import { expect } from "chai";
import factory from "../../factories";

describe("Dataset", () => {
  it("saves with correct parameters", async () => {
    return expect(factory.create("dataset")).to.be.fulfilled;
  });
});
