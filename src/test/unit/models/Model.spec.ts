import "../../startTestSuite";
import { expectValidationError } from "../../testHelpers";

import { expect } from "chai";
import factory from "../../factories";
import "mocha";

describe("Model", () => {
  it("saves with correct parameters", async () => {
    return expect(factory.create("model")).to.be.fulfilled;
  });

  it("does not save with duplicate version/model", async () => {
    const mt = await factory.create("modelType");
    await factory.create("model", {
      modelTypeId: mt.id,
      major: 1,
      minor: 0
    });
    const promise = factory.create("model", {
      modelTypeId: mt.id,
      major: 1,
      minor: 0
    });
    return expectValidationError(promise);
  });

  it("does not save without modelTypeId", async () => {
    const promise = factory.create("model", { modelTypeId: null });
    return expectValidationError(promise);
  });

  it("does not save without datasetId", async () => {
    const promise = factory.create("model", { datasetId: null });
    return expectValidationError(promise);
  });

  it("does not save without ownerId", async () => {
    const promise = factory.create("model", { ownerId: null });
    return expectValidationError(promise);
  });

  it("when creating first model for a model type, source model is not required", async () => {
    const mt = await factory.create("modelType");
    const promise = factory.create("model", {
      modelTypeId: mt.id,
      sourceModelId: null
    });
    return expect(promise).to.be.fulfilled;
  });

  it("when creating first model, source model is not allowed", async () => {
    const mt = await factory.create("modelType");
    const otherMT = await factory.create("modelType");
    const randomSource = await factory.create("model", {
      modelTypeId: otherMT.id,
      sourceModelId: null
    });
    const promise = factory.create("model", {
      modelTypeId: mt.id,
      sourceModelId: randomSource.id
    });
    return expectValidationError(promise);
  });

  it("when creating the second model for the model type, null sourceModelId is invalid", async () => {
    const mt = await factory.create("modelType");
    await factory.create("model", {
      modelTypeId: mt.id,
      sourceModelId: null,
      major: 1,
      minor: 0
    });
    const promise = factory.create("model", {
      modelTypeId: mt.id,
      sourceModelId: null,
      major: 1,
      minor: 1
    });
    return expectValidationError(promise);
  });

  it("when creating the second model for the model type, source model of another type is invalid", async () => {
    const mt = await factory.create("modelType");
    const source = await factory.create("model", {
      modelTypeId: mt.id,
      sourceModelId: null,
      major: 1,
      minor: 0
    });
    const otherMT = await factory.create("modelType");
    await factory.create("model", {
      modelTypeId: otherMT.id,
      sourceModelId: null,
      major: 1,
      minor: 0
    });
    const promise = factory.create("model", {
      sourceModelId: source.id,
      modelTypeId: otherMT.id,
      major: 1,
      minor: 1
    });
    return expectValidationError(promise);
  });

  it("when creating the second model for a model type, valid source model is accepted", async () => {
    const mt = await factory.create("modelType");
    const source = await factory.create("model", {
      modelTypeId: mt.id,
      sourceModelId: null,
      major: 1,
      minor: 0
    });
    const promise = factory.create("model", {
      modelTypeId: mt.id,
      sourceModelId: source.id,
      major: 1,
      minor: 1
    });
    return expect(promise).to.be.fulfilled;
  });

  it(".deployable returns true if format is deployable", async () => {
    const format = await factory.create("format", { deployable: true });
    const model = await factory.create("model", {
      formatId: format.id
    });
    const result = await model.isDeployable();
    return expect(result).to.eq(true);
  });
});
