import "../../startTestSuite";
import { expectValidationError } from "../../testHelpers";
import { expect } from "chai";
import factory from "../../factories";

describe("Model numbering", () => {
  it("A model for new model type can be numbered 1.0", async () => {
    const mt = await factory.create("modelType");
    const promise = factory.create("model", {
      modelTypeId: mt.id,
      major: 1,
      minor: 0
    });
    return expect(promise).to.be.fulfilled;
  });

  it("A model for new model type can also be numbered 0.0", async () => {
    const mt = await factory.create("modelType");
    const promise = factory.create("model", {
      modelTypeId: mt.id,
      major: 0,
      minor: 0
    });
    return expect(promise).to.be.fulfilled;
  });

  it("A model for new model type cannot be numbered 2.0", async () => {
    const promise = factory.create("model", {
      major: 2,
      minor: 0
    });
    return expectValidationError(promise);
  });

  it("When 1.0 exists for the model type, creating 0.0 is not allowed", async () => {
    const mt = await factory.create("modelType");
    await factory.create("model", {
      modelTypeId: mt.id,
      major: 1,
      minor: 0
    });
    const promise = factory.create("model", {
      modelTypeId: mt.id,
      major: 0,
      minor: 0
    });
    return expectValidationError(promise);
  });

  it("When 1.0 exists, 2.0 is valid", async () => {
    const mt = await factory.create("modelType");
    const source = await factory.create("model", {
      modelTypeId: mt.id,
      major: 1,
      minor: 0
    });
    const promise = factory.create("model", {
      modelTypeId: mt.id,
      major: 2,
      minor: 0,
      sourceModelId: source.id
    });
    return expect(promise).to.be.fulfilled;
  });

  it("When 0.0 exists, 1.0 is valid", async () => {
    const mt = await factory.create("modelType");
    const source = await factory.create("model", {
      modelTypeId: mt.id,
      major: 0,
      minor: 0
    });
    const promise = factory.create("model", {
      modelTypeId: mt.id,
      major: 1,
      minor: 0,
      sourceModelId: source.id
    });
    return expect(promise).to.be.fulfilled;
  });

  it("When 0.0 exists but 1.0 does not, 2.0 is invalid", async () => {
    const mt = await factory.create("modelType");
    const source = await factory.create("model", {
      modelTypeId: mt.id,
      major: 0,
      minor: 0
    });
    const promise = factory.create("model", {
      modelTypeId: mt.id,
      major: 2,
      minor: 0,
      sourceModelId: source.id
    });
    return expectValidationError(promise);
  });

  it("When 1.0 exists, 1.1 is valid", async () => {
    const mt = await factory.create("modelType");
    const source = await factory.create("model", {
      modelTypeId: mt.id,
      major: 1,
      minor: 0
    });
    const promise = factory.create("model", {
      modelTypeId: mt.id,
      major: 1,
      minor: 1,
      sourceModelId: source.id
    });
    return expect(promise).to.be.fulfilled;
  });

  it("When 1.0 does not exist, 1.1 is invalid", async () => {
    const mt = await factory.create("modelType");
    const source = await factory.create("model", {
      modelTypeId: mt.id,
      major: 0,
      minor: 0
    });
    const promise = factory.create("model", {
      modelTypeId: mt.id,
      major: 1,
      minor: 1,
      sourceModelId: source.id
    });
    return expectValidationError(promise);
  });

  it("When 1.0 exists but 1.1 does not, 1.2 is invalid", async () => {
    const mt = await factory.create("modelType");
    const source = await factory.create("model", {
      modelTypeId: mt.id,
      major: 1,
      minor: 0
    });
    const promise = factory.create("model", {
      modelTypeId: mt.id,
      major: 1,
      minor: 2,
      sourceModelId: source.id
    });
    return expectValidationError(promise);
  });
});
