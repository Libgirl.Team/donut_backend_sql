import "../../startTestSuite";
import { expectValidationError } from "../../testHelpers";
import { expect } from "chai";
import factory from "../../factories";

describe("ModelType", () => {
  it("saves with correct parameters", async () => {
    return expect(factory.create("modelType")).to.be.fulfilled;
  });

  it("does not save with duplicate name", async () => {
    await factory.create("modelType", { name: "Test Model Type" });
    const promise = factory.create("modelType", { name: "Test Model Type" });
    return expectValidationError(promise);
  });
});
