import config from "config";
import { exec } from "child_process";

export default class AuthClient {
  // Request a Bearer token for the service account
  // that will be passed to the build environment
  static authenticate(): Promise<string> {
    const { serviceRoleName, serviceRoleKeyFilename } = config.get("gcp");
    const activateCommand = `gcloud auth activate-service-account ${serviceRoleName} --key-file=${serviceRoleKeyFilename}`;
    return new Promise((resolve, reject) => {
      exec(activateCommand, (error) => {
        if (error) reject(error);
        const tokenCommand = `gcloud auth print-access-token ${serviceRoleName}`;
        exec(tokenCommand, (error, stdout) => {
          if (error) reject(error);
          return resolve(stdout.trim());
        });
      });
    });
  }
}
