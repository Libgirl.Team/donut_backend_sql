import config from "config";
import { run_v1 } from "googleapis/build/src/apis/run/v1";
import { GoogleAuth } from "google-auth-library";

const { projectId, keyFilename } = config.get("gcp");
const { region } = config.get("gcp");
const defaultOptions = {
  rootUrl: `https://${region}-run.googleapis.com`
};

export default class CloudRunClient {
  static async getAuth() {
    const auth = new GoogleAuth({
      scopes: ["https://www.googleapis.com/auth/cloud-platform"], // doesn't work without this
      keyFilename
    });
    return auth.getClient();
  }

  static async listServicesRaw(): Promise<any> {
    const run = new run_v1.Run({
      auth: await CloudRunClient.getAuth()
    });
    const { data } = (await run.namespaces.services.get(
      {
        name: `namespaces/${projectId}/services`
      },
      defaultOptions
    )) as any;
    return data;
  }

  static async listServices(): Promise<any[]> {
    const data = await CloudRunClient.listServicesRaw();
    return data.items.map((item: any) => {
      return {
        id: item.metadata?.uid,
        name: item.metadata?.name,
        url: item.status?.url,
        insertedAt: item.metadata?.creationTimestamp
      };
    });
  }
}
