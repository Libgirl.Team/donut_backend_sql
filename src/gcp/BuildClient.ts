import { CloudBuildClient } from "@google-cloud/cloudbuild";
import config from "config";
import AuthClient from "./AuthClient";

interface IBuildParams {
  serviceName: string;
  modelURI: string;
  id: string;
}

export default class BuildClient {
  static async deployModel({ id, modelURI, serviceName }: IBuildParams) {
    const { projectId, keyFilename, region } = config.get("gcp");
    const token = await AuthClient.authenticate();
    const cb = new CloudBuildClient({ projectId, keyFilename });
    const tag = "gcr.io/$PROJECT_ID/$_REPO_NAME:$_MODEL_ID";
    cb.createBuild({
      projectId,
      build: {
        source: {
          repoSource: {
            repoName: "donut-serving",
            branchName: "master"
          }
        },
        // All build steps need to be specified in this request rather than
        // in the repo's cloudbuild.yaml file (which is ignored).
        // This is a bit of concern mixing, but it also keeps important logic
        // inside the application.
        steps: [
          // Build the docker image with model
          {
            name: "gcr.io/cloud-builders/docker",
            args: [
              "build",
              "-t",
              tag,
              "--build-arg",
              "MODEL_URI",
              "--build-arg",
              "TOKEN",
              "--build-arg",
              "MODEL_ID",
              "."
            ],
            env: [
              "MODEL_ID=$_MODEL_ID",
              "MODEL_URI=$_MODEL_URI",
              "TOKEN=$_TOKEN"
            ]
          },
          // Push the prebuilt docker image to Container Registry
          {
            name: "gcr.io/cloud-builders/docker",
            args: ["push", tag]
          },
          // Deploy to Cloud Run
          {
            name: "gcr.io/cloud-builders/gcloud",
            args: [
              "run",
              "deploy",
              "$_SERVICE_NAME",
              "--image",
              tag,
              "--region",
              region,
              "--platform",
              "managed",
              "--allow-unauthenticated",
              "--port=8501"
            ]
          }
        ],

        images: [tag],

        substitutions: {
          _MODEL_ID: id.replace(/[^a-f0-9]/g, ""),
          _MODEL_URI: modelURI,
          _SERVICE_NAME: serviceName,
          _REPO_NAME: serviceName,
          _TOKEN: token
        }
      }
    });
  }

  static async listBuilds() {
    const { projectId, keyFilename } = config.get("gcp");
    const cb = new CloudBuildClient({ projectId, keyFilename });
    const builds = await cb.listBuilds({ projectId });
    console.log(builds);
  }
}
