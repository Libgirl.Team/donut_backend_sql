import snakeCase from "lodash/snakeCase";
import capitalize from "lodash/capitalize";
import Resource from "../types/resource";
import { Model } from "../types/model";
import dayjs from "dayjs";

export default class ResourceHelper {
  static displayName(res: Resource) {
    if (res.__typename === "Model") {
      const model = (res as any) as Model;
      return `${model.modelType?.name} v. ${model.version}`;
    }
    return res?.name;
  }

  static insertedAt(res: Resource) {
    return dayjs(res.insertedAt).format("YYYY-MM-DD HH:mm:ss (UTCZ)");
  }

  static readableTypename(res: Resource | string) {
    return capitalize(this.readableResourceName(res));
  }

  static listPath(res: Resource | string) {
    const typename = typeof res === "string" ? res : res.__typename;
    if (typename === "Model") return "/";
    return this.pluralResourcePath(typename);
  }

  static pluralResourcePath(res: Resource | string) {
    if (typeof res === "string") return `/${snakeCase(res)}s`;
    return `/${snakeCase(res.__typename)}s`;
  }

  static singleResourcePath(res: Resource) {
    return `${ResourceHelper.pluralResourcePath(res)}/${res.id}`;
  }

  static editResourcePath(res: Resource) {
    return `${ResourceHelper.singleResourcePath(res)}/edit`;
  }

  static createResourcePath(res: Resource | string) {
    return `${ResourceHelper.pluralResourcePath(res)}/new`;
  }

  static importResourcePath(typename: string) {
    return `${ResourceHelper.pluralResourcePath(typename)}/import`;
  }

  static readableResourceName(res: Resource | string) {
    if (typeof res === "string") return snakeCase(res).replace(/_/g, " ");
    return snakeCase(res.__typename).replace(/_/g, " ");
  }
}
