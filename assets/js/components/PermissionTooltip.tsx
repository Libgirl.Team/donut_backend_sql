import React from "react";
import { User } from "shared/types/auth";
import UserHelper from "../helpers/UserHelper";
import QuestionMark from "../icons/question-circle.svg";

interface Props {
  user: User;
}

const PermissionTooltip = ({ user }: Props) => {
  return (
    <div className="permission_tooltip">
      <div className="permission_tooltip__icon">
        <QuestionMark />
      </div>
      <div className="permission_tooltip__content">
        <h4 className="permission_tooltip__heading">My permissions:</h4>
        <p>Team Management: {UserHelper.numericToLevel(user.tmLevel)}</p>
        <p>Model Management: {UserHelper.numericToLevel(user.mmLevel)}</p>
        <p>Deployment Management: {UserHelper.numericToLevel(user.dmLevel)}</p>
      </div>
    </div>
  );
};

export default PermissionTooltip;
