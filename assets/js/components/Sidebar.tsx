import React, { useMemo } from "react";
import { Link } from "react-router-dom";
import Footer from "./Footer";
import { useAuthContext } from "../context/AuthContext";
import BrainIcon from "../icons/brain.svg";
import ModelTypeIcon from "../icons/project-diagram.svg";
import EvaluationIcon from "../icons/person-booth.svg";
import EfficiencyIcon from "../icons/clock.svg";
import LaptopIcon from "../icons/laptop-code.svg";
import ServerIcon from "../icons/chart-line.svg";
import UsersIcon from "../icons/users.svg";
import PuzzleIcon from "../icons/puzzle-piece.svg";
import WaveIcon from "../icons/wave-square.svg";
import CloudIcon from "../icons/cloud.svg";

interface Props {
  to: string;
  icon: typeof BrainIcon;
  text: string;
}

const SidebarLink = ({ to, icon: Icon, text }: Props) => (
  <Link to={to} className="item sidebar_link">
    <Icon />
    {text}
  </Link>
);

export default () => {
  const { user } = useAuthContext();
  const disabled = useMemo(() => {
    return user?.tmLevel === 0 && user.mmLevel === 0 && user.dmLevel === 0;
  }, []);
  return (
    <aside className={`sidebar ${disabled ? "sidebar--disabled" : ""}`}>
      <SidebarLink to="/" text="Models" icon={BrainIcon} />
      <div className="sidebar__nested_section">
        <SidebarLink
          to="/model_types"
          text="Model types"
          icon={ModelTypeIcon}
        />
        <SidebarLink to="/algorithms" text="Algorithms" icon={LaptopIcon} />
        <SidebarLink to="/datasets" text="Datasets" icon={ServerIcon} />
        <SidebarLink to="/data_types" text="Data types" icon={WaveIcon} />
        <SidebarLink to="/usages" text="Usages" icon={PuzzleIcon} />
        <SidebarLink
          to="/efficiency_types"
          text="Efficiency types"
          icon={EfficiencyIcon}
        />
        <SidebarLink
          to="/evaluation_types"
          text="Evaluation types"
          icon={EvaluationIcon}
        />
        <SidebarLink to="/formats" text="Formats" icon={PuzzleIcon} />
      </div>
      <SidebarLink to="/users" text="Users" icon={UsersIcon} />
      <SidebarLink to="/services" text="Services" icon={CloudIcon} />
      <Footer />
    </aside>
  );
};
