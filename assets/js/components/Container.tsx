import React from "react";

interface Props {
  children:
    | JSX.Element
    | JSX.Element[]
    | React.ReactChildren
    | React.ReactChild;
  className?: string;
}

export default ({ children, className }: Props) => (
  <div className={`container ${className}`}>{children}</div>
);
