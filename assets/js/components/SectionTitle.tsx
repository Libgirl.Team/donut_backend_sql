import React from "react";
import { WrapperProps } from "../types/common";
import SearchForm from "./SearchForm";

type Props = WrapperProps & {
  actionButtons?: JSX.Element | null;
  vertical?: boolean;
  searchUrl?: string;
};

const SectionTitle = ({
  children,
  actionButtons,
  vertical,
  searchUrl
}: Props) => {
  return (
    <div className="section_title">
      <h1>{children}</h1>
      {searchUrl && <SearchForm baseUrl={searchUrl} />}

      {actionButtons ? (
        <div
          className={`section_title__actions ${
            vertical ? "section_title__actions--vertical" : ""
          }`}
        >
          {actionButtons}
        </div>
      ) : null}
    </div>
  );
};

export default SectionTitle;
