import React from "react";

export default () => (
  <footer>
    &copy; 2019-2020 by Libgirl Inc.
    <br /> All rights reserved.
  </footer>
);
