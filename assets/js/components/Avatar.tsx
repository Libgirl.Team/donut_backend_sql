import React from "react";
import { User } from "shared/types/auth";

interface Props {
  user: User;
}

function initials({ name }: User) {
  return name
    .split(" ")
    .map((n: string) => n[0])
    .join("");
}

const Avatar = ({ user }: Props) => {
  if (user.picture) {
    return (
      <img
        src={user.picture}
        className="avatar__img"
        alt={`User: ${user.name}`}
      />
    );
  }
  return <>{initials(user)}</>;
};

export default Avatar;
