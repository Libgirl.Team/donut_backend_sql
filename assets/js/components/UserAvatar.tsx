import React from "react";
import { User } from "shared/types/auth";
import { Link } from "react-router-dom";

interface Props {
  user: User;
}

const UserAvatar = ({ user }: Props) => {
  const initials = user.name
    .split(" ")
    .map((n: string) => n[0])
    .join("");
  return (
    <Link className="avatar" to="/profile">
      {user.picture ? (
        <img src={user.picture} className="avatar__img" alt={initials} />
      ) : (
        initials
      )}
    </Link>
  );
};

export default UserAvatar;
