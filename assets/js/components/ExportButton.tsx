import React from "react";
import ExportIcon from "../icons/file-export.svg";
import { Icon } from "semantic-ui-react";

interface Props {
  typename: string;
}

const ExportButton = ({ typename }: Props) => (
  <a
    href={`${CONFIG.api_endpoint}/export?typename=${typename}`}
    className="ui icon left labeled button tiny"
    target="_blank"
    rel="noopener noreferrer"
    title="Export the whole resource to JSON"
  >
    <div className="icon custom_icon">
      <ExportIcon />
    </div>
    Export
  </a>
);

export default ExportButton;
