import React, { SyntheticEvent } from "react";
import { Form, Dropdown } from "semantic-ui-react";
import { useAuthContext } from "../context/AuthContext";
import Permissions from "../../../src/shared/permissions";
import InlineCreateButton from "./InlineCreateButton";

export interface ISelectOption {
  key: number | string;
  value: number | string;
  text: string;
}

interface Props {
  error?: string;
  label?: string;
  name: string;
  onChange(e: SyntheticEvent, data: any): any;
  options: ISelectOption[];
  placeholder?: string;
  value: any;
  autoFocus?: boolean;
  typename?: string;
}

const DEFAULT_PLACEHOLDER = "Select one";

export default function Select({
  autoFocus,
  error,
  label,
  name,
  onChange,
  options,
  placeholder,
  value,
  typename
}: Props) {
  const { user } = useAuthContext();
  const canCreate = typename && Permissions.canCreate(user, typename);

  return (
    <Form.Field
      error={error}
      className={`select ${canCreate ? "creatable" : ""}`}
    >
      {label ? <label>{label}</label> : null}
      <Dropdown
        autoFocus={autoFocus}
        name={name}
        placeholder={placeholder || DEFAULT_PLACEHOLDER}
        value={value}
        onChange={onChange}
        options={options}
        selection
      />
      {canCreate ? <InlineCreateButton typename={typename as string} /> : null}
      {error ? <span className="error_explanation">{error}</span> : null}
    </Form.Field>
  );
}
