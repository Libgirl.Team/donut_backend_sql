import React, { useState, useEffect } from "react";
import { Button, Icon } from "semantic-ui-react";
import BaseCopyToClipboard from "react-copy-to-clipboard";

interface Props {
  label: string;
  value: string;
}

const CopyToClipboard = ({ label, value }: Props) => {
  const [copied, setCopied] = useState(false);
  useEffect(() => {
    if (!copied) return;
    const timeout = setTimeout(() => {
      setCopied(false);
      clearTimeout(timeout);
    }, 3000);
  }, [copied]);

  return (
    <div
      className={`copy_to_clipboard ${
        copied ? "copy_to_clipboard--copied" : ""
      }`}
    >
      <div className="copy_to_clipboard__label">{label}</div>
      <BaseCopyToClipboard text={value} onCopy={() => setCopied(true)}>
        <Button
          icon
          className="copy_to_clipboard__button"
          labelPosition="right"
        >
          {value}
          <Icon name="copy" />
          <span className="copy_to_clipboard__success">Copied!</span>
        </Button>
      </BaseCopyToClipboard>
    </div>
  );
};

export default CopyToClipboard;
