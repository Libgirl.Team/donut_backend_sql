import React, { useContext } from "react";
import { User } from "shared/types/auth";
import PermissionLabels from "./PermissionLabels";
import PermissionTooltip from "./PermissionTooltip";
import UserAvatar from "./UserAvatar";

interface Props {
  user: User;
}

const UserIdentification = ({ user }: Props) => {
  return (
    <>
      <UserAvatar user={user} />
      <div className="user_section__top">
        <span className="user_section__name">{user.name}</span>
        <div className="permission_labels">
          <PermissionLabels user={user} />
          <PermissionTooltip user={user} />
        </div>
      </div>
    </>
  );
};

export default UserIdentification;
