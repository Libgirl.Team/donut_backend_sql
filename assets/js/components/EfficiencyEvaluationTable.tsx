import React from "react";
import { Model } from "../types/model";
import { Table } from "semantic-ui-react";
import { Link } from "react-router-dom";
import capitalize from "lodash/capitalize";
import EditButton from "./EditButton";
import { useAuthContext } from "../context/AuthContext";
import Permissions from "../../../src/shared/permissions";

export enum TableType {
  Efficiencies = "efficiencies",
  Evaluations = "evaluations"
}

interface Props {
  model: Model;
  type: TableType;
}

interface HeaderProps extends Props {
  editable: boolean;
}

const Header = ({ type, model, editable }: HeaderProps) => (
  <h4>
    {capitalize(type)}
    {editable ? (
      <EditButton
        to={`/models/${model.id}/${type}`}
        className="efficiency-evaluation-table__edit"
      />
    ) : null}
  </h4>
);

const BlankSlate = ({ type, model }: Props) => {
  const { user } = useAuthContext();
  const editable = Permissions.can(user, `setModel${type}`, model);
  return (
    <>
      <Header type={type} model={model} editable={editable} />
      <p className="efficiency-evaluation-table__blank-slate">
        No records found.
        {editable ? (
          <>
            You can add some on the{" "}
            <Link to={`/models/${model.id}/${type}`}>edit page</Link>.
          </>
        ) : (
          <> You are not allowed to edit {type} for this model.</>
        )}
      </p>
    </>
  );
};

export default function EfficiencyEvaluationTable({ model, type }: Props) {
  const data = model[type];
  const { user } = useAuthContext();
  const editable = Permissions.can(user, `setModel${type}`, model);
  if (!data.length) return <BlankSlate model={model} type={type} />;
  return (
    <section id={`${type}-table`} className="efficiency_evaluation_table">
      <Header type={type} model={model} editable={editable} />
      <Table celled className={`${type}_table`} fixed>
        <Table.Header>
          <Table.Row>
            {data.map((e) => (
              <Table.HeaderCell key={`${type}-header-${e.type?.name}`}>
                {e.type?.name}
              </Table.HeaderCell>
            ))}
          </Table.Row>
        </Table.Header>
        <Table.Body>
          <Table.Row>
            {data.map((e) => (
              <Table.Cell key={`${type}-cell-${e.typeId}`}>
                {e.value} {e.type.unit}
              </Table.Cell>
            ))}
          </Table.Row>
        </Table.Body>
      </Table>
    </section>
  );
}
