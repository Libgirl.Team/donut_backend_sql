import React from "react";
import { Icon, Button } from "semantic-ui-react";

interface Props {
  icon?: "save" | "trash";
  onClick?(e: any): void;
  type?: string;
  positive?: boolean;
  negative?: boolean;
  disabled?: boolean;
  label?: string;
}

const SaveButton = ({
  onClick,
  type,
  positive,
  negative,
  icon,
  label,
  disabled
}: Props) => (
  <Button
    icon
    labelPosition="left"
    type={type || "submit"}
    positive={!!positive}
    negative={!!negative}
    disabled={!!disabled}
    onClick={onClick}
  >
    <Icon name={icon || "save"} />
    {label || "Save"}
  </Button>
);

export default SaveButton;
