import React, { useCallback, useMemo } from "react";
import Modal from "./Modal";
import { useMutation } from "@apollo/react-hooks";
import { getMutationKey } from "../graphql/helpers";
import gql from "graphql-tag";

interface Props {
  resource: any;
  handleClose(e?: any): void;
}

const generateMutation = (typename: string, active: boolean) => {
  return gql`
  mutation($id: ID!) {
    ${
      active ? "de" : ""
    }activate${typename}(id: $id) { success errors resource { id deactivatedAt } }
  }`;
};

const DeactivateModal = ({ resource, handleClose }: Props) => {
  const active = !resource?.deactivatedAt;
  const mutation = useMemo(
    () => generateMutation(resource.__typename, active),
    [resource.__typename, active]
  );
  const mutationKey = useMemo(() => getMutationKey(mutation), [mutation]);
  const [mutate] = useMutation(mutation);
  const handleSubmit = useCallback(async () => {
    const res = await mutate({ variables: { id: resource.id } });
    const success = res?.data?.[mutationKey]?.success;
    if (success) {
      handleClose();
    }
  }, [resource.id]);
  return (
    <Modal
      icon={active ? "eye slash" : "eye"}
      submitLabel={active ? "Deactivate" : "Activate"}
      negative={active}
      positive={!active}
      handleClose={handleClose}
      title="Confirmation required"
      handleSubmit={handleSubmit}
    >
      {active ? (
        <p>
          You are about to deactivate a resource. Deactivation is reversible and
          hides the resource for all regular users save for the owner.
        </p>
      ) : (
        <p>
          You are about to activate a resource. It will be visible to all users
          with the permission to list this resource.
        </p>
      )}
    </Modal>
  );
};

export default DeactivateModal;
