import React, { useState, useEffect, useCallback } from "react";
import { useQuery, useLazyQuery } from "@apollo/react-hooks";
import { EfficiencyType, EvaluationType } from "../types/model";
import gql from "graphql-tag";
import { Line } from "@nivo/line";

interface Props {
  modelTypeId: number;
}

interface QueryResult {
  efficiencyTypes: any[];
  evaluationTypes: any[];
}

type typename = "EfficiencyType" | "EvaluationType";

const findType = (
  data: QueryResult,
  type: typename | null,
  id: number | string | null
) => {
  if (!type || !id) {
    return null;
  }
  const collection =
    type === "EfficiencyType" ? data?.efficiencyTypes : data?.evaluationTypes;
  return collection.find((t) => {
    return t.id === id;
  });
};

const efficiencyQuery = gql`
  query($id: ID!, $typeId: ID!, $type: SummaryType) {
    summaryMap(id: $id, typeId: $typeId, type: $type)
  }
`;
const EfficiencyTypePicker = ({ modelTypeId }: Props) => {
  const { data: effTypesData, loading } = useQuery(gql`
    {
      efficiencyTypes {
        id
        name
        unit
      }
      evaluationTypes {
        id
        name
        unit
      }
    }
  `);
  const [type, setType] = useState<typename | null>(null);
  const [typeId, setTypeId] = useState<string | number | null>(null);
  const active = findType(effTypesData, type, typeId);
  const [fetchEfficiencyData, { data }] = useLazyQuery(efficiencyQuery);

  useEffect(() => {
    setTypeId(effTypesData?.efficiencyTypes?.[0]?.id);
    setType("EfficiencyType");
  }, [loading]);

  useEffect(() => {
    if (!typeId) return;
    fetchEfficiencyData({
      variables: {
        id: modelTypeId,
        typeId,
        type: type === "EfficiencyType" ? "EFFICIENCY" : "EVALUATION"
      }
    });
  }, [modelTypeId, typeId, type, fetchEfficiencyData]);

  if (loading) return <p>Loading...</p>;
  return (
    <div className="efficiency_type_picker">
      <h3>Efficiency/evaluation charts</h3>
      <h4>Efficiency types:</h4>
      <div className="efficiency_type_picker__options">
        {effTypesData?.efficiencyTypes?.map((t: EfficiencyType) => (
          <button
            type="button"
            className={`efficiency_type_picker__option ${
              type === t.__typename && t.id === typeId
                ? "efficiency_type_picker__option--active"
                : ""
            }`}
            key={t.id}
            onClick={() => {
              setTypeId(t.id);
              setType("EfficiencyType");
            }}
          >
            {t.name}
          </button>
        ))}
      </div>
      <h4>Evaluation types:</h4>
      <div className="efficiency_type_picker__options">
        {effTypesData?.evaluationTypes?.map((t: EvaluationType) => (
          <button
            type="button"
            className={`efficiency_type_picker__option ${
              type === t.__typename && t.id === typeId
                ? "efficiency_type_picker__option--active"
                : ""
            }`}
            key={t.id}
            onClick={() => {
              setTypeId(t.id);
              setType("EvaluationType");
            }}
          >
            {t.name}
          </button>
        ))}
      </div>
      {active && data?.summaryMap?.length ? (
        <div className="chart">
          <Line
            curve="natural"
            data={data?.summaryMap}
            height={400}
            width={800}
            colors={{ scheme: "set1" }}
            margin={{ top: 60, right: 110, bottom: 60, left: 60 }}
            lineWidth={3}
            pointSize={10}
            useMesh
            enablePointLabel
            pointLabel={((d: any) => d.label) as any}
            axisBottom={{
              legend: "Minor version",
              legendOffset: 50,
              legendPosition: "middle"
            }}
            axisLeft={{
              legend: `${active?.name} (${active?.unit})`,
              legendOffset: -50,
              legendPosition: "middle"
            }}
            legends={[
              {
                anchor: "bottom-right",
                direction: "column",
                justify: false,
                translateX: 100,
                translateY: 0,
                itemsSpacing: 0,
                itemDirection: "left-to-right",
                itemWidth: 80,
                itemHeight: 20,
                itemOpacity: 0.75,
                symbolSize: 12,
                symbolShape: "circle",
                symbolBorderColor: "rgba(0, 0, 0, .5)",
                effects: [
                  {
                    on: "hover",
                    style: {
                      itemBackground: "rgba(0, 0, 0, .03)",
                      itemOpacity: 1
                    }
                  }
                ]
              }
            ]}
          />
        </div>
      ) : (
        <div className="chart chart--empty" />
      )}
    </div>
  );
};

export default EfficiencyTypePicker;
