import React from "react";

export enum LogoSize {
  Small = "small",
  Medium = "medium"
}

interface Props {
  size?: any;
  href?: string;
}

function logo(size: LogoSize = LogoSize.Small) {
  return (
    <img src="donut_logo.svg" className={`site_logo site_logo--${size}`}></img>
  );
}

export default ({ size, href }: Props) => {
  if (href) {
    return <a href={href}>{logo(size)}</a>;
  }
  return logo(size);
};
