import React from "react";
import Permissions from "shared/permissions";
import CreateButton from "components/CreateButton";
import ExportButton from "components/ExportButton";
import ResourceHelper from "helpers/ResourceHelper";
import { User } from "shared/types/auth";
import ImportButton from "components/ImportButton";

interface ActionButtonsProps {
  user: User | null;
  typename: string;
}

const ActionButtons = ({ user, typename }: ActionButtonsProps) => {
  return (
    <>
      {Permissions.canCreate(user, typename) ? (
        <CreateButton to={ResourceHelper.createResourcePath(typename)} />
      ) : null}
      <div className="action_buttons__separator" />
      {Permissions.canImport(user, typename) ? (
        <ImportButton typename={typename} />
      ) : null}
      {Permissions.canExport(user, typename) ? (
        <ExportButton typename={typename} />
      ) : null}
    </>
  );
};

export default ActionButtons;
