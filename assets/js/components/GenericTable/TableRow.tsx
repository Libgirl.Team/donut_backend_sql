import React from "react";
import { Table } from "semantic-ui-react";

interface RowProps {
  data: any;
  renderRow: (data: any) => JSX.Element;
  history: any;
  href?: string | null;
}

const TableRow = ({ data, renderRow: Row, history, href }: RowProps) => {
  const onClick = href ? () => history.push(href) : null;
  return (
    <Table.Row onClick={onClick}>
      <Row {...data} />
    </Table.Row>
  );
};

export default TableRow;
