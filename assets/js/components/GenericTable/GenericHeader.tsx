import React from "react";
import { Table } from "semantic-ui-react";
import SortAscIcon from "icons/sort-up.svg";
import SortDescIcon from "icons/sort-down.svg";

export interface GenericHeaderProps {
  onSetSortColumn(column: string): void;
  sortColumn: string;
  sortOrder: "ASC" | "DESC";
  renderer?(): JSX.Element[];
}

const defaultRenderer = () => {
  return [
    <Table.HeaderCell name="name">Name</Table.HeaderCell>,
    <Table.HeaderCell className="owner-header" name="owner_id">
      Created by
    </Table.HeaderCell>,
    <Table.HeaderCell className="date-header" name="inserted_at">
      Date added
    </Table.HeaderCell>
  ];
};

const GenericTableHeader = ({
  onSetSortColumn,
  sortColumn,
  sortOrder,
  renderer
}: GenericHeaderProps) => {
  const columns = renderer?.() || defaultRenderer();
  const children =
    columns.map((column) => {
      let className = column.props.className || "";
      let onClick;
      let icon = null;
      className += " sortable";
      if (column.props.name) {
        if (column.props.name === sortColumn) {
          const order = sortOrder.toLowerCase();
          className += ` active ${order}`;
          if (order === "asc") icon = <SortAscIcon />;
          else icon = <SortDescIcon />;
        }
        onClick = () => onSetSortColumn(column.props.name);
      }
      return React.cloneElement(
        column,
        { ...column.props, className, onClick },
        column.props.children,
        icon
      );
    }) as any;
  return (
    <Table.Header>
      <Table.Row>{children}</Table.Row>
    </Table.Header>
  );
};

export default GenericTableHeader;
