import React, { useState, useCallback } from "react";
import { useQuery, RenderPromises } from "@apollo/react-hooks";
import { useHistory } from "react-router-dom";
import { useAuthContext } from "../../context/AuthContext";
import { DocumentNode } from "graphql";
import { Table } from "semantic-ui-react";

import Loader from "components/Loader";
import PageWrapper from "components/PageWrapper";
import SectionTitle from "components/SectionTitle";
import Pagination from "components/Pagination";
import usePageFromLocation from "../../hooks/usePageFromLocation";
import ResourceHelper from "../../helpers/ResourceHelper";
import Breadcrumb from "components/Breadcrumb";
import GenericTableHeader from "./GenericHeader";
import GenericRow from "./GenericRow";
import ActionButtons from "./ActionButtons";
import TableRow from "./TableRow";

export interface Props {
  query: DocumentNode;
  heading: string;
  typename: string;
  renderHeader?: () => JSX.Element[];
  renderRow?: (data: any) => JSX.Element;
  dataKey: string;
  fixed?: boolean;
  searchUrl?: string;
}

export default ({
  query,
  heading,
  renderHeader,
  renderRow,
  dataKey,
  typename,
  fixed,
  searchUrl
}: Props) => {
  const [sortColumn, setSortColumn] = useState("inserted_at");
  const [sortOrder, setSortOrder] = useState<"ASC" | "DESC">("DESC");
  const onSetSortColumn = useCallback(
    (column: string) => {
      let order = "ASC";
      if (column === sortColumn && sortOrder === "ASC") order = "DESC";
      setSortColumn(column);
      setSortOrder(order as any);
    },
    [setSortOrder, setSortColumn, sortColumn, sortOrder]
  );
  const { page, term } = usePageFromLocation();
  const { loading, data, refetch } = useQuery(query, {
    variables: { page, term, sortColumn, sortOrder }
  });

  useCallback(() => {
    refetch({ page, term, sortColumn, sortOrder });
  }, [sortColumn, sortOrder]);

  const history = useHistory();
  const Row = renderRow || GenericRow;
  const { user } = useAuthContext();
  if (loading || !data) return <Loader />;
  const queryRes = data[dataKey];
  const paginateable =
    !Array.isArray(queryRes) && typeof queryRes === "object" && queryRes.cursor;
  const rows = paginateable ? queryRes.data : queryRes;
  const singleResourcePath = ResourceHelper.pluralResourcePath(typename);
  return (
    <PageWrapper>
      <Breadcrumb resource={typename} action="index" />
      <SectionTitle
        actionButtons={<ActionButtons user={user} typename={typename} />}
        searchUrl={searchUrl}
      >
        {heading}
      </SectionTitle>
      <Table celled className={`generic_table hoverable`} fixed={fixed}>
        <GenericTableHeader
          onSetSortColumn={onSetSortColumn}
          sortColumn={sortColumn}
          sortOrder={sortOrder}
          renderer={renderHeader}
        />
        <Table.Body>
          {rows.map((row: any) => {
            const href = `${singleResourcePath}/${row.id}`;
            return (
              <TableRow
                data={row}
                renderRow={Row}
                history={history}
                href={href}
                key={`row-${row.id}`}
              />
            );
          })}
        </Table.Body>
      </Table>
      {queryRes?.cursor?.totalPages > 1 ? (
        <Pagination cursor={queryRes?.cursor} basePath={searchUrl as string} />
      ) : null}
    </PageWrapper>
  );
};
