import React from "react";
import day from "dayjs";
import { Table } from "semantic-ui-react";

const GenericRow = (data: any) => (
  <>
    <Table.Cell>{data.name}</Table.Cell>
    <Table.Cell className="owner-value">{data.owner?.name}</Table.Cell>
    <Table.Cell className="date-value">
      {day(data.insertedAt).format("YYYY-MM-DD")}
    </Table.Cell>
  </>
);

export default GenericRow;
