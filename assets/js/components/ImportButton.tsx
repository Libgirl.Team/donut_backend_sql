import React from "react";
import ResourceHelper from "../helpers/ResourceHelper";
import { Link } from "react-router-dom";
import ImportIcon from "../icons/file-import.svg";

interface Props {
  typename: string;
}

const ImportButton = ({ typename }: Props) => (
  <Link
    to={ResourceHelper.importResourcePath(typename)}
    className="ui button icon left labeled tiny"
    title="Import from JSON file"
  >
    <div className="icon custom_icon">
      <ImportIcon />
    </div>
    Import
  </Link>
);

export default ImportButton;
