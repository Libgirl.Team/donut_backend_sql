import React from "react";
import { Card, CardContent, Grid, Form, Button } from "semantic-ui-react";

interface Props {
  onChange(e: any): any;
  name: string;
  value: boolean;
  label: string;
}

export default function Checkbox({ onChange, name, value, label }: Props) {
  return (
    <Form.Field className={value ? "ui checkbox" : "ui checkbox checked"}>
      <input
        type="checkbox"
        checked={value}
        style={{ display: "none" }}
      ></input>
      <label>
        <input
          type="checkbox"
          name={name}
          checked={value}
          onChange={onChange}
        />
        {label}
      </label>
    </Form.Field>
  );
}
