import React, { useCallback, useState, ChangeEvent, useMemo } from "react";
import Modal from "./Modal";
import { useMutation } from "@apollo/react-hooks";
import InputField from "./InputField";
import { Form } from "semantic-ui-react";
import { useHistory } from "react-router";
import ResourceHelper from "../helpers/ResourceHelper";
import { getMutationKey } from "../graphql/helpers";
import gql from "graphql-tag";
import useFlash from "../hooks/useFlash";

interface Props {
  resource: any;
  handleClose(e?: any): void;
  virtual?: boolean;
}

const CONFIRMATION_TEXT = "the void gazes at you";

const compareInput = (actual: string, expected = CONFIRMATION_TEXT) => {
  return actual.toLowerCase() === expected.toLowerCase();
};

const generateDeleteMutation = (typename: any) => {
  const str = `mutation($id: ID!) { delete${typename}(id: $id) { success errors } }`;
  return gql`
    ${str}
  `;
};

const DeleteModal = ({ resource, handleClose, virtual }: Props) => {
  const [input, setInput] = useState("");
  const mutation = useMemo(() => generateDeleteMutation(resource.__typename), [
    resource.__typename
  ]);
  const mutationKey = useMemo(() => getMutationKey(mutation), [mutation]);
  const [mutate] = useMutation(mutation);
  const history = useHistory();
  const { addMessage } = useFlash();

  const handleSubmit = async () => {
    const res = await mutate({ variables: { id: resource.id } });
    const success = res?.data?.[mutationKey]?.success;
    if (success) {
      addMessage({
        level: "success",
        msg: "The resource has been successfully deleted."
      });
      history.push(ResourceHelper.listPath(resource));
    }
  };

  const handleChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      setInput(e.target.value);
    },
    [setInput]
  );

  const disabled = !compareInput(input);

  return (
    <Modal
      icon="trash"
      submitLabel="Delete"
      negative
      disabled={disabled}
      handleClose={handleClose}
      title={"Attention! This is a matter of grave importance!"}
      handleSubmit={handleSubmit}
    >
      <p>
        You are about to delete a resource. This action cannot be undone. All
        dependent resources will be removed, together with their respective
        dependent resources.
      </p>
      <p>This may cause cataclysmic changes to the space-time continuum.</p>
      <p>
        In order to proceed, please type <em>{CONFIRMATION_TEXT}</em> in the
        field below.
      </p>
      {virtual ? (
        <small>
          Note: this resource will not really be removed from the database for
          referential integrity reasons. (It will still be deleted through
          cascade effect when you delete its dependencies).
        </small>
      ) : null}
      <Form onSubmit={handleSubmit} disabled={disabled}>
        <InputField
          autoFocus
          name="confirmation"
          placeholder={CONFIRMATION_TEXT}
          value={input}
          onChange={handleChange}
        />
      </Form>
    </Modal>
  );
};

export default DeleteModal;
