import React, {
  useMemo,
  Reducer,
  useReducer,
  useCallback,
  ChangeEvent,
  FormEvent
} from "react";
import {
  EfficiencyType,
  EfficiencyEvaluationType,
  EfficiencyEvaluation
} from "../types/model";
import { Form, Table, Button, Icon } from "semantic-ui-react";
import InputField from "./InputField";
import Select, { ISelectOption } from "./Select";
import set from "lodash/set";
const TrashIcon = require("../icons/trash-alt.svg").default;

interface Props {
  options: EfficiencyType[];
  data: EfficiencyEvaluation[];
  handleSubmit(e: FormEvent, state: EfficiencyEvaluationParam[]): void;
}

const optionsFromResults = (data: EfficiencyEvaluationType[]) => {
  return data.map((e) => ({
    text: e.name,
    key: e.id,
    value: e.id
  }));
};

const serializeParams = (data: EfficiencyEvaluationParam[]) =>
  data
    .filter(({ value, typeId }) => value && typeId)
    .map(({ value, typeId }) => ({ typeId, value: Number(value) }));

const availableOptions = (
  all: ISelectOption[],
  state: State,
  index: number
) => {
  const used = state
    .map((p, i) => i !== index && String(p.typeId))
    .filter(Boolean);
  return all.filter((el) => used.indexOf(String(el.value)) === -1);
};

const mapFromResults = (
  data: EfficiencyEvaluationType[],
  key: keyof EfficiencyEvaluationType
) =>
  data.reduce((map, val) => {
    map[val.id] = val[key];
    return map;
  }, {} as any);

enum ReducerEventType {
  AddRow,
  RemoveRow,
  SetRowValue,
  SetRowType
}

interface ReducerEvent {
  type: ReducerEventType;
  payload?: any;
}

export interface EfficiencyEvaluationParam {
  typeId: string | number | null;
  value: string | number;
}

type State = EfficiencyEvaluationParam[];

const paramReducer: Reducer<State, ReducerEvent> = (
  state: State,
  event: ReducerEvent
) => {
  const { type, payload } = event;
  switch (type) {
    case ReducerEventType.AddRow:
      return [...state, { typeId: null, value: "" }];
    case ReducerEventType.RemoveRow:
      state.splice(payload, 1);
      return [...state];
    case ReducerEventType.SetRowType:
      return [...set(state, `${payload.index}.typeId`, payload.value)];
    case ReducerEventType.SetRowValue:
      return [...set(state, `${payload.index}.value`, payload.value)];
  }
};

export default function EfficiencyEvaluationForm({
  options,
  data,
  handleSubmit
}: Props) {
  const selectOptions = useMemo(() => optionsFromResults(options), []);
  const units = useMemo(() => mapFromResults(options, "unit"), []);
  const descriptions = useMemo(
    () => mapFromResults(options, "description"),
    []
  );
  const [state, dispatch] = useReducer(paramReducer, data);

  const handleSelect = useCallback(
    (_e: any, { name, value }: any) => {
      dispatch({
        type: ReducerEventType.SetRowType,
        payload: {
          index: name,
          value
        }
      });
    },
    [dispatch]
  );

  const handleChange = useCallback(
    (e: ChangeEvent) => {
      const { name, value } = e.target as HTMLInputElement;
      dispatch({
        type: ReducerEventType.SetRowValue,
        payload: {
          index: name,
          value
        }
      });
    },
    [dispatch]
  );

  const addRow = useCallback(() => {
    dispatch({
      type: ReducerEventType.AddRow
    });
  }, [dispatch]);

  const removeRow = useCallback(
    (index: number) => {
      dispatch({
        type: ReducerEventType.RemoveRow,
        payload: index
      });
    },
    [dispatch]
  );

  return (
    <Form onSubmit={(e) => handleSubmit(e, serializeParams(state))}>
      <Table className="efficiency_evaluation_form">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Efficiency type</Table.HeaderCell>
            <Table.HeaderCell>Value/unit</Table.HeaderCell>
            <Table.HeaderCell className="efficiency_evaluation_form__delete" />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {state.map((option, index) => (
            <React.Fragment key={index}>
              <Table.Row>
                <Table.Cell>
                  <Select
                    options={availableOptions(selectOptions, state, index)}
                    value={option.typeId}
                    name={String(index)}
                    onChange={handleSelect}
                  />
                </Table.Cell>
                <Table.Cell>
                  <InputField
                    name={String(index)}
                    onChange={handleChange}
                    value={option.value}
                    inputLabel={option.typeId && units[option.typeId]}
                    type="text"
                    inputMode="decimal"
                  />
                </Table.Cell>
                <Table.Cell className="efficiency_evaluation_form__delete">
                  <button onClick={() => removeRow(index)} type="button">
                    <TrashIcon />
                  </button>
                </Table.Cell>
              </Table.Row>
              {option.typeId && descriptions[option.typeId] ? (
                <Table.Row>
                  <Table.Cell
                    className="efficiency_evaluation_form__description"
                    colSpan="3"
                  >
                    {descriptions[option.typeId]}
                  </Table.Cell>
                </Table.Row>
              ) : null}
            </React.Fragment>
          ))}
        </Table.Body>
      </Table>
      <Button
        icon
        labelPosition="left"
        type="button"
        onClick={addRow}
        disabled={state.length === selectOptions.length}
      >
        <Icon name="plus" />
        Add row
      </Button>
      <Button icon labelPosition="left" type="submit" positive>
        <Icon name="save" />
        Persist
      </Button>
    </Form>
  );
}
