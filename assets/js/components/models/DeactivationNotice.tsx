import React from "react";
import { Message } from "semantic-ui-react";

const DeactivationNotice = () => {
  return <Message content={`This model is deactivated.`} />;
};

export default DeactivationNotice;
