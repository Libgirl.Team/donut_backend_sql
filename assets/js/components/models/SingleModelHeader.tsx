import React from "react";
import ActionButtons from "../GenericShow/ActionButtons";
import ResourceHelper from "../../helpers/ResourceHelper";
import UserAvatar from "../UserAvatar";
import { Model } from "types/model";

interface Props {
  resource: Model;
}

const SingleModelHeader = ({ resource }: Props) => {
  return (
    <section className="single_model__header">
      <div className="single_model__header__left">
        <h1 className="single_model__header__name">
          {resource.modelType.name}
          <small className="single_model__header__version">
            v. {resource.version}
          </small>
        </h1>
        <p className="single_model__header__meta">
          Created: {ResourceHelper.insertedAt(resource)}
        </p>
      </div>
      <ActionButtons resource={resource} hideEdit />
      <div className="single_model__owner">
        <UserAvatar user={resource.owner} />
        <strong>Owner: {resource.owner?.name}</strong>
      </div>
    </section>
  );
};

export default SingleModelHeader;
