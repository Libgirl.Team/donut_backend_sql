import React from "react";
import { Card } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { Model } from "types/model";

interface Props {
  resource: Model;
}

const SourceModel = ({ resource }: Props) => {
  return (
    <>
      <h3>Source</h3>
      {resource.sourceModel ? (
        <Card fluid>
          <Card.Content>
            <Card.Header>
              Source model:{" "}
              <Link to={`/models/${resource.modelType.id}`}>
                {resource.modelType?.name} v. {resource.sourceModel?.version} (#
                {resource.sourceModel?.id})
              </Link>
            </Card.Header>
            <Card.Description>
              <p>Model URI: {resource.sourceModel?.modelURI}</p>
            </Card.Description>
          </Card.Content>
        </Card>
      ) : (
        <p>No data about source model found.</p>
      )}
    </>
  );
};

export default SourceModel;
