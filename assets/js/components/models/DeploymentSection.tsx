import React from "react";
import { Model } from "types/model";
import { Card } from "semantic-ui-react";
import { Link } from "react-router-dom";

interface Props {
  model: Model;
}

const UnsupportedFormatMessage = () => {
  return (
    <p>
      This model is in an unsupported format and cannot be deployed using Donut.
      At the moment, only TensorFlow models are supported.
    </p>
  );
};

const OKMessage = ({ model }: Props) => {
  return (
    <>
      <p>
        This model is stored in a supported format,{" "}
        <Link to={`/formats/${model?.format?.id}`}>{model?.format?.name}</Link>.
        You can deploy it to Google Cloud Run. Please click the button below to
        proceed.
      </p>
      <Link to={`/models/${model.id}/deploy`} className="ui button">
        Deploy
      </Link>
    </>
  );
};

const DeploymentSection = ({ model }: Props) => {
  return (
    <Card fluid className="model_deployment">
      <Card.Content>
        <Card.Header>Deployment</Card.Header>
      </Card.Content>
      <Card.Content>
        {model.format?.deployable ? (
          <OKMessage model={model} />
        ) : (
          <UnsupportedFormatMessage />
        )}
      </Card.Content>
    </Card>
  );
};

export default DeploymentSection;
