import React from "react";
import DependencyItem from "../GenericShow/DependencyItem";
import Dependencies from "../Dependencies";

interface Props {
  resource: any;
}

const ModelDependencies = ({ resource }: Props) => {
  return (
    <Dependencies resource={resource}>
      <DependencyItem resource={resource.modelType} />
      <DependencyItem resource={resource.modelType.algorithm} />
      <DependencyItem
        resource={resource.modelType.usage}
        meta={
          <>
            Input type: {resource.modelType?.usage?.inputType?.name}, output
            type: {resource.modelType?.usage?.outputType?.name}
          </>
        }
      />
      <DependencyItem resource={resource.format} />
    </Dependencies>
  );
};

export default ModelDependencies;
