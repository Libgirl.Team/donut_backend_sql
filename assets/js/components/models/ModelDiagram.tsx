import React from "react";
import Arrow from "../../icons/long-arrow-alt-right.svg";
import { ModelType } from "../../types/modelType";
import { Link } from "react-router-dom";

interface Props {
  modelType: ModelType;
}

const ModelDiagram = ({ modelType }: Props) => {
  const inputPath = `/data_types/${modelType.usage?.inputType?.id}`;
  const outputPath = `/data_types/${modelType.usage?.outputType?.id}`;
  return (
    <div className="model_diagram">
      <Link className="model_diagram__input" to={inputPath}>
        Input
      </Link>
      <Arrow />
      <div className="model_diagram__model">Model</div>
      <Arrow />
      <Link className="model_diagram__output" to={outputPath}>
        Output
      </Link>
      <Link className="model_diagram__label" to={inputPath}>
        {modelType.usage?.inputType?.name}
      </Link>
      <span></span>
      <p className="model_diagram__label model_diagram__label--primary">
        {modelType.name}
      </p>
      <span></span>
      <Link className="model_diagram__label" to={outputPath}>
        {modelType.usage?.outputType?.name}
      </Link>
    </div>
  );
};

export default ModelDiagram;
