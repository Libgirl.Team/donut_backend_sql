import React from "react";
import { Link } from "react-router-dom";
import { Model } from "types/model";
import { Separator } from "../Breadcrumb";

interface Props {
  model: Model;
}

const ModelBreadcrumb = ({ model }: Props) => {
  return (
    <div className="ui breadcrumb">
      <Link to="/" className="section">
        Models
      </Link>
      <Separator />
      <div className="section active">
        {model.modelType.name} v. {model.version}
      </div>
    </div>
  );
};

export default ModelBreadcrumb;
