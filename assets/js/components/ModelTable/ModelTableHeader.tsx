import React from "react";
import { Table } from "semantic-ui-react";
import { EvaluationType, EfficiencyType } from "../../types/model";
import { IPreferences } from "../../types/preferences";
import { MODEL_COLUMN_HEADINGS } from "../../actions/Preferences";
import SortableHeaderCell from "./SortableHeaderCell";

interface HeaderProps {
  preferences: IPreferences;
  evaluationTypes: EvaluationType[];
  efficiencyTypes: EfficiencyType[];
}
const ModelTableHeader = ({
  preferences,
  evaluationTypes,
  efficiencyTypes
}: HeaderProps) => {
  const { columns, efficiencyTypeIDs, evaluationTypeIDs } =
    preferences.models || {};

  return (
    <Table.Header>
      <Table.Row>
        {columns.map((column: string) => (
          <SortableHeaderCell name={column} key={column}>
            {MODEL_COLUMN_HEADINGS[column] || column}
          </SortableHeaderCell>
        ))}
        {evaluationTypeIDs.map((id) => {
          const evaluationType = evaluationTypes.find(
            (e) => String(e.id) === String(id)
          );
          const key = `evaluation.${id}`;
          return (
            <SortableHeaderCell
              key={key}
              name={key}
              className="evaluation-header"
            >
              {evaluationType?.name as string}
            </SortableHeaderCell>
          );
        })}
        {efficiencyTypeIDs.map((id) => {
          const efficiencyType = efficiencyTypes.find(
            (e) => String(e.id) === String(id)
          );
          const key = `efficiency.${id}`;
          return (
            <SortableHeaderCell
              key={key}
              name={key}
              className="efficiency-header"
            >
              <>
                {efficiencyType?.name}
                {efficiencyType?.unit && ` (${efficiencyType.unit})`}
              </>
            </SortableHeaderCell>
          );
        })}
      </Table.Row>
    </Table.Header>
  );
};

export default ModelTableHeader;
