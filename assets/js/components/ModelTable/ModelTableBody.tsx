import React from "react";
import { Table } from "semantic-ui-react";
import { Model } from "../../types/model";
import { IPreferences } from "../../types/preferences";
import { useHistory } from "react-router";
import getInObject from "lodash/get";
import { Page } from "../../types/cursor";

interface IBodyProps {
  preferences: IPreferences;
  data: Page<Model>;
}

const ModelTableBody = ({ preferences, data }: IBodyProps) => {
  const { data: models } = data;
  const { columns, efficiencyTypeIDs, evaluationTypeIDs } =
    preferences.models || {};
  const history = useHistory();
  return (
    <Table.Body>
      {models.map((row) => (
        <Table.Row
          key={`row-${row.id}`}
          onClick={() => history.push(`/models/${row.id}`)}
          className={row?.deactivatedAt ? "inactive" : ""}
        >
          {columns.map((column) => (
            <Table.Cell key={`${row.id}-${column}`}>
              {getInObject(row, column)}
            </Table.Cell>
          ))}
          {evaluationTypeIDs.map((id: any) => {
            const value = row.evaluationMap[id];
            return (
              <Table.Cell
                key={`${row.id}-ev-${id}`}
                className="evaluation-value"
              >
                {value}
              </Table.Cell>
            );
          })}
          {efficiencyTypeIDs.map((id: any) => {
            const value = row.efficiencyMap[id];
            return (
              <Table.Cell
                key={`${row.id}-ef-${id}`}
                className="efficiency-value"
              >
                {value}
              </Table.Cell>
            );
          })}
        </Table.Row>
      ))}
    </Table.Body>
  );
};

export default ModelTableBody;
