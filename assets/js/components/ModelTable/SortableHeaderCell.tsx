import React, { useCallback } from "react";
import SortAscIcon from "icons/sort-up.svg";
import SortDescIcon from "icons/sort-down.svg";
import { Table } from "semantic-ui-react";
import { useSelector } from "react-redux";
import { RootState } from "../../store/reducers";
import Preferences from "../../actions/Preferences";

interface SortableHeaderCellProps {
  name: string;
  children: React.ReactChild;
  className?: string;
}

const SortIcon = ({ direction }: any) => {
  if (direction === "ASC") return <SortAscIcon />;
  if (direction === "DESC") return <SortDescIcon />;
  return null;
};

const SortableHeaderCell = ({
  name,
  children,
  className
}: SortableHeaderCellProps) => {
  const { sortColumn, sortOrder } = useSelector(
    (state: RootState) => state.preferences.models
  );
  const isActive = sortColumn === name;
  const direction = (isActive && sortOrder) || "";
  const mergedClass = `${className || ""} sortable ${
    isActive ? "sorting " : ""
  }${direction}`;
  const handler = useCallback(() => {
    Preferences.setSortColumn(name);
  }, [name]);
  return (
    <Table.HeaderCell onClick={handler} className={mergedClass}>
      <>
        {children} <SortIcon direction={direction} />
      </>
    </Table.HeaderCell>
  );
};

export default SortableHeaderCell;
