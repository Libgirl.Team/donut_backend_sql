import React from "react";
import { Form } from "semantic-ui-react";

type Props = React.InputHTMLAttributes<HTMLInputElement> & {
  inputLabel?: any;
  error?: string;
  label?: string;
};

export default function InputField({
  error,
  label,
  inputLabel,
  ...rest
}: Props) {
  const input = <input {...rest} />;
  return (
    <Form.Field error={error}>
      {label ? <label>{label}</label> : null}
      {inputLabel ? (
        <div className="ui right labeled input">
          {input}
          <div className="ui basic label">{inputLabel}</div>
        </div>
      ) : (
        input
      )}
      {error ? <span className="error_explanation">{error}</span> : null}
    </Form.Field>
  );
}
