import React from "react";
import { Button } from "semantic-ui-react";
import { Link } from "react-router-dom";

interface Props {
  onClick?(): void;
  to?: string;
}

const CancelButton = ({ onClick, to }: Props) => {
  if (to) {
    return (
      <Link to={to} className="ui button basic">
        Cancel
      </Link>
    );
  }
  return <Button onClick={onClick}>Cancel</Button>;
};

export default CancelButton;
