import React from "react";
import ResourceHelper from "../helpers/ResourceHelper";

interface Props {
  typename: string;
}

export default ({ typename }: Props) => (
  <a
    href={ResourceHelper.createResourcePath(typename)}
    className="ui button positive"
    rel="noopener noreferrer"
    target="_blank"
  >
    New
  </a>
);
