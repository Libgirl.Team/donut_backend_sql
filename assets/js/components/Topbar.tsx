import React, { useContext } from "react";
import UserIdentification from "./UserIdentification";
import { Link } from "react-router-dom";
import AuthClient from "../actions/AuthClient";
import { User } from "shared/types/auth";
import AuthContext from "../context/AuthContext";
const LogoutIcon = require("../icons/sign-out-alt.svg").default;

export default () => {
  const { user, refetch } = useContext(AuthContext);
  return (
    <header className="topbar">
      <Link to="/" className="topbar__logo">
        <img src="/donut_logo.svg"></img>
      </Link>
      <UserIdentification user={(user as unknown) as User} />
      <a className="topbar__logout" onClick={() => AuthClient.signOut(refetch)}>
        Logout
        <LogoutIcon />
      </a>
    </header>
  );
};
