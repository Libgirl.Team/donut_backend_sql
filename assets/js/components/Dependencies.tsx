import React from "react";
import Resource from "../types/resource";
import SitemapIcon from "../icons/sitemap.svg";
import ResourceHelper from "../helpers/ResourceHelper";

interface Props {
  resource: Resource;
  children: React.ReactNode;
}

const Dependencies = ({ resource, children }: Props) => {
  return (
    <div className="dependencies">
      <div className="container">
        <div className="dependencies__title">
          <h2>
            <SitemapIcon />
            Dependencies: {ResourceHelper.displayName(resource)}
          </h2>
        </div>
        <ul className="dependencies__list">{children}</ul>
      </div>
    </div>
  );
};

export default Dependencies;
