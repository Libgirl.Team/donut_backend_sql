import React from "react";
import day from "dayjs";
import Resource from "../../types/resource";

interface Props {
  resource: Resource;
}

const Dates = ({ resource }: Props) => {
  return (
    <>
      <p className="resource__inserted_at">
        <strong>Created at:</strong>{" "}
        {day(resource.insertedAt).format("YYYY-MM-DD HH:mm:ss (UTCZ)")}
      </p>
      <p className="resource__update_at">
        <strong>Updated at:</strong>{" "}
        {day(resource.updatedAt).format("YYYY-MM-DD HH:mm:ss (UTCZ)")}
      </p>
    </>
  );
};

export default Dates;
