import React from "react";
import Resource from "../../types/resource";
import { Model } from "../../types/model";
import { Link } from "react-router-dom";
import ResourceHelper from "../../helpers/ResourceHelper";

export default function DependentResource<T>({
  resource
}: {
  resource: T & Resource;
}) {
  if (!resource) return null;
  switch (resource.__typename) {
    case "Model": {
      const model = (resource as unknown) as Model;
      return (
        <p>
          <Link to={`/models/${resource.id}`}>
            Model: {model?.modelType?.name} v. {model.version} (
            <span className="uuid">{resource.id}</span>)
          </Link>
        </p>
      );
    }
    default:
      return (
        <p>
          <Link to={ResourceHelper.singleResourcePath(resource)}>
            {resource.__typename}: {resource.name} (
            <span className="uuid">{resource.id}</span>)
          </Link>
        </p>
      );
  }
}
