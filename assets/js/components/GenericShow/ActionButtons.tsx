import React, { useState, useCallback } from "react";
import Resource from "../../types/resource";
import DeleteButton from "../DeleteButton";
import DeactivateButton from "../DeactivateButton";
import ResourceHelper from "../../helpers/ResourceHelper";
import Permissions from "shared/permissions";
import EditButton from "../EditButton";
import DeactivationModal from "../DeactivationModal";
import { useAuthContext } from "../../context/AuthContext";
import DeleteModal from "../DeleteModal";

interface Props {
  resource: Resource;
  hideEdit?: boolean;
}

const ActionButtons = ({ resource, hideEdit }: Props) => {
  const { user } = useAuthContext();
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [showDeactivateModal, setShowDeactivateModal] = useState(false);
  const toggleDeleteModal = useCallback(() => {
    setShowDeleteModal((s) => !s);
  }, [setShowDeleteModal]);

  const toggleDeactivateModal = useCallback(() => {
    setShowDeactivateModal((s) => !s);
  }, [setShowDeactivateModal]);
  const updateable = Permissions.canUpdate(user, resource);
  const deleteable = Permissions.canDelete(user, resource);
  const deactivatable =
    resource.__typename === "Model" &&
    Permissions.canDeactivate(user, resource);
  return (
    <>
      <div className="ui buttons tiny">
        {updateable && !hideEdit ? (
          <EditButton to={ResourceHelper.editResourcePath(resource)} />
        ) : null}
        {deleteable ? <DeleteButton onClick={toggleDeleteModal} /> : null}
        {deactivatable ? (
          <DeactivateButton
            onClick={toggleDeactivateModal}
            active={!(resource as any)?.deactivatedAt}
          />
        ) : null}
      </div>
      {deleteable && showDeleteModal ? (
        <DeleteModal
          resource={resource}
          handleClose={toggleDeleteModal}
          virtual={resource.__typename === "Model"}
        />
      ) : null}
      {deactivatable && showDeactivateModal ? (
        <DeactivationModal
          resource={resource}
          handleClose={toggleDeactivateModal}
        />
      ) : null}
    </>
  );
};

export default ActionButtons;
