import React from "react";
import Resource from "../../types/resource";
import DependentResourcesMessage from "./DependentResourcesMessage";
import DependentResource from "./DependentResource";
import Permissions from "../../../../src/shared/permissions";
import { useAuthContext } from "../../context/AuthContext";

interface Props {
  resource: Resource;
  dependentResources?: string[];
}

const DependentResourcesSection = ({ resource, dependentResources }: Props) => {
  const { user } = useAuthContext();
  const deleteable = Permissions.canDelete(user, resource);
  const hasDependent =
    resource &&
    dependentResources?.some((key) => (resource as any)[key]?.length);
  return hasDependent ? (
    <>
      {deleteable ? <DependentResourcesMessage /> : null}
      <h4>Dependent resources</h4>
      {dependentResources?.map((key: string) =>
        (resource as any)[key].map((r: any, index: number) => (
          <DependentResource resource={r} key={`res-${index}`} />
        ))
      )}
    </>
  ) : null;
};

export default DependentResourcesSection;
