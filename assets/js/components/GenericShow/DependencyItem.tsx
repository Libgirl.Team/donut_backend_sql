import React, { useMemo } from "react";
import { Card } from "semantic-ui-react";
import { Link } from "react-router-dom";
import lowerCase from "lodash/lowerCase";
import capitalize from "lodash/capitalize";
import ResourceHelper from "../../helpers/ResourceHelper";
import Resource from "../../types/resource";

interface Props {
  resource: Resource;
  meta?: React.ReactNode;
  resourceType?: string;
}

const DependencyItem = ({ resource, meta, resourceType }: Props) => {
  const { __typename, id, name, description } = resource;
  const url = ResourceHelper.singleResourcePath(resource);
  const heading = useMemo(
    () => resourceType || capitalize(lowerCase(__typename)),
    [__typename, resourceType]
  );

  return (
    <li>
      <strong>{heading}:</strong> <Link to={url}>{name}</Link>
      {meta ? <Card.Meta>{meta}</Card.Meta> : null}
    </li>
  );
};

export default DependencyItem;
