import React from "react";
import MD from "../Markdown";
import Resource from "../../types/resource";

interface Props {
  resource: Resource;
}

const Description = ({ resource }: Props) => {
  if (!resource.description) return null;
  return (
    <section id="description">
      <h4>Description</h4>
      <div className="resource__description">
        <MD source={resource.description} />
      </div>
    </section>
  );
};

export default Description;
