import React from "react";
import SectionTitle from "../SectionTitle";
import Container from "../Container";
import { DocumentNode } from "graphql";
import { useQuery } from "@apollo/react-hooks";
import Loader from "../Loader";
import { useParams } from "react-router";
import UserLink from "./UserLink";
import NotFound from "../../views/NotFound";
import CopyToClipboard from "../CopyToClipboard";
import Resource from "../../types/resource";
import Dates from "./Dates";
import DependentResourcesSection from "./DependenResourcesSection";
import Description from "./Description";
import ActionButtons from "./ActionButtons";
import ResourceHelper from "../../helpers/ResourceHelper";
import Breadcrumb from "../Breadcrumb";

interface Props {
  resourceType: string;
  query: DocumentNode;
  resourceKey: string;
  dependentResources?: string[];
  children?(resource: any): any;
  disableCache?: boolean;
}

function GenericShow({
  resourceType,
  resourceKey,
  query,
  children,
  dependentResources,
  disableCache
}: Props) {
  const { id } = useParams();
  const { data, loading } = useQuery(query, {
    variables: { id },
    fetchPolicy: disableCache ? "no-cache" : "cache-and-network"
  });

  const resource = data?.[resourceKey] as Resource;
  if (loading) return <Loader />;
  if (!loading && !resource) return <NotFound />;
  const displayName = ResourceHelper.displayName(resource);
  const actionButtons = <ActionButtons resource={resource} />;
  return (
    <>
      <Breadcrumb resource={resource} action="show" />
      <Container className="generic_show">
        <SectionTitle actionButtons={actionButtons} vertical>
          {resourceType}: {displayName as string}
        </SectionTitle>
        <CopyToClipboard value={resource.id} label="UUID" />
        <UserLink
          user={resource.owner}
          label="Owner"
          className="resource__owner"
        />
        <UserLink
          user={resource.author}
          label="Author"
          className="resource__author"
        />
        <Dates resource={resource} />
        <Description resource={resource} />
        {children ? children(resource) : null}
        <DependentResourcesSection
          resource={resource}
          dependentResources={dependentResources}
        />
      </Container>
    </>
  );
}

export default GenericShow;
