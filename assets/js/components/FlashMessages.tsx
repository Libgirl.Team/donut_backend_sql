import React, { useEffect, useState } from "react";
import useFlash from "../hooks/useFlash";
import { Message } from "semantic-ui-react";
import { useLocation } from "react-router";
import { FlashLevel } from "../store/reducers/flashReducer";

const levelToColor = (level: FlashLevel) => {
  switch (level) {
    case FlashLevel.Info:
      return "blue";
    case FlashLevel.Error:
      return "red";
    case FlashLevel.Success:
      return "green";
  }
};

export default function FlashMessages() {
  const { messages, clearFlash } = useFlash();
  const location = useLocation();
  const [pageChanged, setPageChanged] = useState(false);
  const [msgCount, setMsgCount] = useState(messages.length);

  useEffect(() => {
    if (messages.length !== msgCount) {
      setMsgCount(messages.length);
      setPageChanged(false);
    } else if (messages.length) {
      if (pageChanged) {
        clearFlash();
      }
      setPageChanged(true);
    }
  }, [location.pathname, clearFlash, messages.length]);

  return (
    <>
      {messages.map((m, index) => (
        <Message
          content={m.msg}
          key={`flash-${index}`}
          color={levelToColor(m.level as FlashLevel)}
        />
      ))}
    </>
  );
}

