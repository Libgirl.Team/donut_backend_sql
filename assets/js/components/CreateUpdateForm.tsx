import React, {
  FormEvent,
  useReducer,
  useCallback,
  Reducer,
  useEffect,
  useMemo,
  ChangeEvent,
  SyntheticEvent
} from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { DocumentNode } from "graphql";
import { useParams, useHistory } from "react-router";
import Loader from "./Loader";
import ResourceHelper from "../helpers/ResourceHelper";
import { Form } from "semantic-ui-react";
import SectionTitle from "./SectionTitle";
import InputField from "./InputField";
import TextArea from "./TextArea";
import gql from "graphql-tag";
import SaveButton from "./SaveButton";
import { getMutationKey } from "../graphql/helpers";
import camelCase from "lodash/camelCase";
import useFlash from "../hooks/useFlash";
import { FlashLevel } from "../store/reducers/flashReducer";
import CancelButton from "./CancelButton";
import ImportButton from "./ImportButton";

export interface IFormFieldRendererProps {
  handleChange(e: ChangeEvent<any>): void;
  handleSelect(e: SyntheticEvent, data: any): void;
  params: { [k: string]: any };
  errors: { [k: string]: any };
  rawData: any;
}

interface Props {
  create?: boolean;
  query?: DocumentNode;
  initialParams?: { [k: string]: any };
  children?(props: IFormFieldRendererProps): React.ReactNode;
  typename: string;
  columns?: string | string[];
}

enum ReducerEventType {
  SetParam,
  DataLoaded
}

interface ReducerEvent {
  type: ReducerEventType;
  payload?: any;
}

const defaultInitialParams = {
  name: "",
  description: ""
};

const generateMutation = (
  create: boolean,
  typename: string,
  columns?: string | string[]
) => {
  let concatenatedColumns = "";
  if (Array.isArray(columns)) {
    concatenatedColumns = columns.join(" ");
  } else if (typeof columns === "string") {
    concatenatedColumns = columns;
  }
  const str = `
  mutation(${create ? "" : "$id: ID!,"} $params: ${typename}Params!)  {
    ${create ? "create" : "update"}${typename}(${
    create ? "" : "id: $id, "
  }params: $params) {
      success errors resource { id ${concatenatedColumns} updatedAt }
    }
  }
  `;
  return gql`
    ${str}
  `;
};

const DefaultFields = ({
  handleChange,
  errors,
  params
}: IFormFieldRendererProps) => (
  <>
    <InputField
      name="name"
      label="Name:"
      autoFocus
      value={params.name}
      error={errors.name}
      onChange={handleChange}
    />
    <TextArea
      name="description"
      label="Description:"
      onChange={handleChange}
      error={errors.description}
      value={params.description}
    />
  </>
);

const paramReducer: Reducer<any, ReducerEvent> = (
  state: any,
  { type, payload }: ReducerEvent
) => {
  switch (type) {
    case ReducerEventType.DataLoaded:
      return Object.keys(state).reduce((acc: any, key) => {
        acc[key] = payload[key];
        return acc;
      }, {});
    case ReducerEventType.SetParam: {
      const { field, value } = payload;
      return {
        ...state,
        [field]: value
      };
    }
  }
};

const NO_OP_QUERY = gql`
  {
    hello
  }
`;

const CreateUpdateForm = ({
  create,
  query,
  initialParams,
  children,
  typename,
  columns
}: Props) => {
  const { id } = useParams();
  const { data, loading } = useQuery(query || NO_OP_QUERY, {
    variables: { id },
    fetchPolicy: "no-cache"
  });
  columns = columns || ["name", "description"];
  const mutation = useMemo(
    () => generateMutation(!!create, typename, columns),
    [typename, create]
  );
  const mutationKey = useMemo(() => getMutationKey(mutation), [mutation]);
  const history = useHistory();
  const [mutate, { data: mutationResponse }] = useMutation(mutation);
  const [params, dispatch] = useReducer(
    paramReducer,
    initialParams || defaultInitialParams
  );
  const resourceKey = camelCase(typename);
  const resource = create ? {} : data?.[resourceKey];
  const resourceType = ResourceHelper.readableResourceName(typename);

  useEffect(() => {
    if (!loading && !create)
      dispatch({
        type: ReducerEventType.DataLoaded,
        payload: resource
      });
  }, [loading]);
  const { addMessage } = useFlash();

  const handleSelect = useCallback(
    (_event: any, { name, value }: any) => {
      dispatch({
        type: ReducerEventType.SetParam,
        payload: { field: name, value }
      });
    },
    [dispatch]
  );

  const handleChange = useCallback(
    (e: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLTextAreaElement>) => {
      const { name, value } = e.target;
      dispatch({
        type: ReducerEventType.SetParam,
        payload: { field: name, value }
      });
    },
    [dispatch]
  );

  if (loading) return <Loader />;
  const result = mutationResponse?.[mutationKey];
  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    const { data } = await mutate({ variables: { params, id } });
    const result = data?.[mutationKey];
    if (result?.success) {
      addMessage({
        level: FlashLevel.Success,
        msg: `The resource has been successfully ${
          create ? "created" : "updated"
        }.`
      });
      history.push(ResourceHelper.singleResourcePath(result.resource));
    } else if (result?.errors?.msg) {
      addMessage({
        level: FlashLevel.Error,
        msg: data.errors.msg
      });
    }
  };
  const renderForm = children || DefaultFields;

  const buttons = create ? <ImportButton typename={typename} /> : null;

  return (
    <>
      <Form onSubmit={handleSubmit}>
        <SectionTitle actionButtons={buttons}>
          {create ? (
            `Create ${resourceType}`
          ) : (
            <>
              Edit {resourceType} #{resource.id}
            </>
          )}
        </SectionTitle>
        {renderForm({
          handleSelect,
          rawData: data,
          handleChange,
          params,
          errors: result?.errors || {}
        })}
        <div className="action_buttons">
          <CancelButton to={ResourceHelper.singleResourcePath(resource)} />
          <SaveButton />
        </div>
      </Form>
    </>
  );
};

export default CreateUpdateForm;
