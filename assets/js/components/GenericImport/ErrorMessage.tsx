import React from "react";

const ErrorMessage = ({ data }: any) => {
  return (
    <>
      <h4>Import failed</h4>
      <p>
        An error occurred during import. This is likely a problem with the data
        you submitted.
        <br />
        The database resources are unchanged. Please review the error message
        below and try again.
      </p>
      <div>
        <p>
          <strong>This error occurred while processing record:</strong>{" "}
          {data?.index} (0-indexed)
        </p>
        <h5>Errors:</h5>
        <ul>
          {Object.keys(data?.errors).map((key) => {
            return (
              <li key={key}>
                <strong>{key}:</strong> {data?.errors?.[key]}
              </li>
            );
          })}
        </ul>
      </div>
    </>
  );
};

export default ErrorMessage;
