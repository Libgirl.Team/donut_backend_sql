import React, { useCallback } from "react";
import { useDropzone } from "react-dropzone";
import Container from "../Container";
import SectionTitle from "../SectionTitle";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import SuccessMessage from "./SuccessMessage";
import Dropzone from "./Dropzone";
import { ExampleSchema } from "./SampleJson";
import ResourceHelper from "../../helpers/ResourceHelper";

interface Props {
  typename: string;
  schema?: ExampleSchema;
}

const generateMutation = (typename: string) => {
  return gql`
  mutation($file: Upload!) {
    result: import${typename}s(file: $file) {
      success
      importCount
      errors
    }
  }
  `;
};

const GenericImport = ({ typename, schema }: Props) => {
  const [mutate, { data, loading }] = useMutation(generateMutation(typename));
  const onDrop = useCallback((files: File[]) => {
    mutate({ variables: { file: files[0] } });
  }, []);
  const { getRootProps, getInputProps } = useDropzone({ onDrop });
  const error = data?.result?.errors;
  const success = data?.result?.success;
  return (
    <Container className="generic_import">
      <SectionTitle>
        Import {ResourceHelper.readableResourceName(typename)}s
      </SectionTitle>
      {success ? (
        <SuccessMessage typename={typename} data={data?.result} />
      ) : (
        <div
          {...getRootProps()}
          className={`generic_import__dropzone ${
            error ? "generic_import__dropzone--error" : ""
          }`}
        >
          <Dropzone
            loading={loading}
            typename={typename}
            error={error}
            schema={schema}
          />
          <input {...getInputProps()} className="generic_import__fileinput" />
        </div>
      )}
    </Container>
  );
};

export default GenericImport;
