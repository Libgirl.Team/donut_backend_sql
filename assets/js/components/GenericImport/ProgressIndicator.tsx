import React from "react";

const ProgressIndicator = () => {
  return <h1>Processing...</h1>;
};

export default ProgressIndicator;
