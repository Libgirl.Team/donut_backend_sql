import React, { useMemo } from "react";
import { uuid } from "uuidv4";

export type ExampleSchema = {
  [key: string]: string;
};

interface SampleJsonProps {
  schema?: ExampleSchema;
}

const sampleValue = (type: string) => {
  switch (type) {
    case "string":
      return "Some string";
    case "integer":
      return 42;
    case "float":
      return 21.37;
    case "boolean":
      return true;
    case "id":
      return uuid();
    case "efficiency_evaluation":
      return [
        {
          typeId: 42,
          value: 21.37
        }
      ];
    default:
      return type;
  }
};

const DEFAULT_SCHEMA = {
  name: "string",
  description: "string"
};

const SampleJson = ({ schema }: SampleJsonProps) => {
  const consolidated = schema || DEFAULT_SCHEMA;
  const json = useMemo(
    () => ({
      data: [
        Object.keys(consolidated).reduce(
          (acc, key) => ({
            ...acc,
            [key]: sampleValue(consolidated[key])
          }),
          {}
        )
      ]
    }),
    []
  );
  return (
    <pre>
      <code>{JSON.stringify(json, null, 2)}</code>
    </pre>
  );
};

export default SampleJson;
