import React from "react";
import ErrorMessage from "./ErrorMessage";
import ProgressIndicator from "./ProgressIndicator";
import { Button } from "semantic-ui-react";
import SampleJson, { ExampleSchema } from "./SampleJson";
import ResourceHelper from "../../helpers/ResourceHelper";

interface Props {
  error: any;
  loading: boolean;
  typename: string;
  schema?: ExampleSchema;
}

const Dropzone = ({ error, loading, typename, schema }: Props) => {
  if (error) return <ErrorMessage data={error} />;
  if (loading) return <ProgressIndicator />;
  const lower = typename.toLowerCase();
  return (
    <>
      <p>
        This view lets you import multiple{" "}
        {ResourceHelper.readableResourceName(typename)}s at once. Please provide
        a file in JSON format with the parameters for each record as an array of
        objects, like so:
      </p>
      <SampleJson schema={schema} />
      <p>
        All resources will be created with you as the owner.
        <br />
        You can drag and drop a file onto this box or click anywhere inside this
        area.
      </p>
      <Button type="button">Select file</Button>
    </>
  );
};

export default Dropzone;
