import React from "react";
import { Link } from "react-router-dom";
import ResourceHelper from "../../helpers/ResourceHelper";

interface Props {
  typename: string;
  data?: any;
}

const SuccessMessage = ({ typename, data }: Props) => {
  const resourcesPath =
    typename === "Model" ? "/" : ResourceHelper.pluralResourcePath(typename);
  return (
    <div className="generic_import__dropzone generic_import__dropzone--success">
      <h3>Import completed</h3>
      <p>
        {data?.importCount} {ResourceHelper.readableResourceName(typename)}s
        have been imported.
      </p>
      <Link to={resourcesPath} className="button ui positive">
        View resources
      </Link>
    </div>
  );
};

export default SuccessMessage;
