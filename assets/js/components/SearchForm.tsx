import React, { useState, useCallback, ChangeEvent, FormEvent } from "react";
import { useHistory } from "react-router-dom";
import qs from "querystring";
import usePageFromLocation from "../hooks/usePageFromLocation";

interface Props {
  baseUrl: string;
}

export default ({ baseUrl }: Props) => {
  const { term } = usePageFromLocation();
  const history = useHistory();
  const [searchTerm, setSearchTerm] = useState(term);
  const onSubmit = useCallback(
    (e: FormEvent) => {
      e.preventDefault();
      if (!searchTerm) {
        history.push(baseUrl);
        return;
      }

      const path = `${baseUrl}?${qs.encode({ term: searchTerm })}`;
      history.push(path);
    },
    [searchTerm, history]
  );
  const onChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      const { value } = e.target;
      setSearchTerm(value);
    },
    [setSearchTerm]
  );
  return (
    <form className="search_form ui form" onSubmit={onSubmit}>
      <div className="ui icon input">
        <input
          autoFocus
          type="text"
          value={searchTerm}
          onChange={onChange}
          className="prompt"
          autoComplete="off"
          placeholder="Press [Enter] to submit..."
        />
        <i aria-hidden="true" className="search icon"></i>
      </div>
    </form>
  );
};
