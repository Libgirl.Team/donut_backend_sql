import React, { SyntheticEvent, useCallback } from "react";
import { useHistory } from "react-router";
import { Pagination } from "semantic-ui-react";
import { Cursor } from "../types/cursor";
import qs from "querystring";
import usePageFromLocation from "../hooks/usePageFromLocation";

interface Props {
  cursor: Cursor;
  basePath: string;
}

export default ({ cursor, basePath }: Props) => {
  const history = useHistory();
  const { term } = usePageFromLocation();
  const handleClickPage = useCallback(
    (_e: SyntheticEvent, data: any) => {
      const params = term
        ? { page: data.activePage, term }
        : { page: data.activePage };
      const str = qs.encode(params);
      history.push(`${basePath}?${str}`);
    },
    [history, basePath, term]
  );
  return (
    <div className="generic_table__pagination">
      <Pagination
        totalPages={cursor?.totalPages}
        activePage={cursor?.page}
        secondary
        pointing
        onPageChange={handleClickPage}
        firstItem={null}
        lastItem={null}
      />
    </div>
  );
};
