import React from "react";
import Resource from "../types/resource";
import { Link } from "react-router-dom";
import ResourceHelper from "../helpers/ResourceHelper";

interface Props {
  resource: Resource | string;
  action?: "create" | "edit" | "index" | "show";
}

export const HomeLink = () => (
  <Link to="/" className="section">
    Home
  </Link>
);

export const Separator = () => (
  <i aria-hidden="true" className="right angle icon divider" />
);

const ListBreadcrumb = ({ resource }: Props) => {
  return (
    <div className="section active">
      {ResourceHelper.readableTypename(resource)}s
    </div>
  );
};

const ShowBreadcrumb = ({ resource }: Props) => {
  return (
    <>
      <Link to={ResourceHelper.listPath(resource)} className="section">
        {ResourceHelper.readableTypename(resource)}s
      </Link>
      <Separator />
      <div className="section active">
        {ResourceHelper.displayName(resource as Resource)}
      </div>
    </>
  );
};

const Breadcrumb = ({ resource, action }: Props) => {
  let breadcrumb = null;
  switch (action) {
    case "index":
      breadcrumb = <ListBreadcrumb resource={resource} />;
      break;
    case "show":
      breadcrumb = <ShowBreadcrumb resource={resource} />;
      break;
  }

  return (
    <div className="ui breadcrumb">
      <HomeLink />
      <Separator />
      {breadcrumb}
    </div>
  );
};

export default Breadcrumb;
