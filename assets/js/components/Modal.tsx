import React from "react";
import { Card, CardContent, Button, Icon } from "semantic-ui-react";
import useEventListener from "@use-it/event-listener";
import SaveButton from "./SaveButton";

export interface IModalChildProps {
  handleClose(): void;
}

interface Props {
  title: string;
  icon?: any;
  disabled?: boolean;
  positive?: boolean;
  negative?: boolean;
  handleClose(): void;
  handleSubmit(e: any): any;
  handleReset?(e: any): any;
  handleCancel?(e: any): any;
  submitLabel?: string;
  children: React.ReactNode;
}

const ESCAPE_KEYS = ["27", "Escape"];

export default ({
  handleClose,
  children,
  handleSubmit,
  title,
  icon,
  submitLabel,
  positive,
  negative,
  handleReset,
  disabled
}: Props) => {
  useEventListener("keydown", ({ key }: any) => {
    if (ESCAPE_KEYS.includes(String(key))) {
      handleClose();
    }
  });

  return (
    <>
      <div className="modal__overlay" />
      <div className="modal">
        <Card fluid>
          <CardContent>
            <h3>{title}</h3>
            <button
              onClick={handleClose}
              className="modal__close"
              type="button"
            >
              &times;
            </button>
            {children}
            <div className="modal__buttons">
              {handleReset ? (
                <Button
                  type="button"
                  icon
                  labelPosition="left"
                  onClick={handleReset}
                >
                  <Icon name="repeat" />
                  Reset
                </Button>
              ) : null}
              <small className="modal__instructions">
                Press [Esc] to exit.
              </small>
              <SaveButton
                disabled={disabled}
                icon={icon || "save"}
                label={submitLabel}
                onClick={handleSubmit}
                positive={positive}
                negative={negative}
              />
            </div>
          </CardContent>
        </Card>
      </div>
    </>
  );
};
