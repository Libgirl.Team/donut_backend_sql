import React from "react";
import Loader from "react-loader-spinner";

export default () => (
  <div className="loader">
    <Loader type={"CradleLoader" as any} width={150} height={150} />
  </div>
);
