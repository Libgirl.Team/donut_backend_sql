import React from "react";
import Router from "./router";
import "../css/index.sass";
import { ApolloProvider } from "@apollo/react-hooks";
import store from "./store";
import { Provider as ReduxProvider } from "react-redux";
import client from "./graphql/client";

export default () => {
  return (
    <ReduxProvider store={store}>
      <ApolloProvider client={client}>
        <Router />
      </ApolloProvider>
    </ReduxProvider>
  );
};
