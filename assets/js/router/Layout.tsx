import React, { useContext } from "react";
import Sidebar from "components/Sidebar";
import Topbar from "components/Topbar";
import AuthContext from "../context/AuthContext";
import FlashMessages from "components/FlashMessages";

interface Props {
  children: any;
}

export default function Layout({ children }: Props) {
  const { user } = useContext(AuthContext);
  if (!user) return <div className="background">{children}</div>;
  return (
    <div className="app">
      <Topbar></Topbar>
      <Sidebar />
      <div className="right-side">
        <main>
          <FlashMessages />
          {children}
        </main>
      </div>
    </div>
  );
}
