import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import Layout from "./Layout";

import AlgorithmsList from "../views/AlgorithmsList";
import DataTypesList from "../views/DataTypesList";
import DatasetsList from "../views/DatasetsList";
import FormatsList from "../views/FormatsList";
import LoginPage from "../views/LoginPage";
import ModelEfficiencies from "../views/ModelEfficiencies";
import ModelEvaluations from "../views/ModelEvaluations";
import ModelTypesList from "../views/ModelTypesList";
import ModelsList from "../views/ModelsList";
import NewAlgorithm from "../views/NewAlgorithm";
import NewDataset from "../views/NewDataset";
import NewFormat from "../views/NewFormat";
import NewModel from "../views/NewModel";
import NewModelType from "../views/NewModelType";
import NewUsage from "../views/NewUsage";
import NotFound from "../views/NotFound";
import ServicesList from "../views/ServicesList";
import SingleModel from "../views/SingleModel";
import SingleModelType from "../views/SingleModelType";
import UsagesList from "../views/UsagesList";
import UsersList from "../views/UsersList";
import DeployModel from "../views/DeployModel";

import useAuth from "../hooks/useAuth";
import AuthContext from "../context/AuthContext";
import NewDataType from "../views/NewDataType";
import EfficiencyTypesList from "../views/EfficiencyTypesList";
import EvaluationTypesList from "../views/EvaluationTypesList";
import SingleAlgorithm from "../views/SingleAlgorithm";
import SingleFormat from "../views/SingleFormat";
import SingleUser from "../views/SingleUser";
import SingleUsage from "../views/SingleUsage";
import SingleDataset from "../views/SingleDataset";
import SingleDataType from "../views/SingleDataType";
import SingleEvaluationType from "../views/SingleEvaluationType";
import SingleEfficiencyType from "../views/SingleEfficiencyType";
import UpdateAlgorithm from "../views/UpdateAlgorithm";
import UpdateFormat from "../views/UpdateFormat";
import UpdateDataset from "../views/UpdateDataset";
import UpdateDataType from "../views/UpdateDataType";
import UpdateUsage from "../views/UpdateUsage";
import UpdateUser from "../views/UpdateUser";
import UpdateModelType from "../views/UpdateModelType";
import UpdateModel from "../views/UpdateModel";
import ImportAlgorithms from "../views/ImportAlgorithms";
import ImportDatasets from "../views/ImportDatasets";
import ImportDataTypes from "../views/ImportDataTypes";
import ImportModelTypes from "../views/ImportModelTypes";
import ImportModels from "../views/ImportModels";
import ImportEfficiencyTypes from "../views/ImportEfficiencyTypes";
import ImportEvaluationTypes from "../views/ImportEvaluationTypes";
import ImportFormats from "../views/ImportFormats";
import ImportUsages from "../views/ImportUsages";
import NewEfficiencyType from "../views/NewEfficiencyType";
import UpdateEfficiencyType from "../views/UpdateEfficiencyType";
import NewEvaluationType from "../views/NewEvaluationType";
import UpdateEvaluationType from "../views/UpdateEvaluationType";
import NotAuthorized from "../views/NotAuthorized";
import UserProfile from "../views/UserProfile";

export default function AppRouter() {
  const { user, loading, error, refetch } = useAuth();

  const PrivateRoute = ({
    component: Component,
    tmLevel,
    mmLevel,
    ...rest
  }: any) => {
    const signedIn = user && !error;
    const authorized =
      signedIn &&
      (!tmLevel || user.tmLevel >= tmLevel) &&
      (!mmLevel || user.mmLevel >= mmLevel);
    return (
      <Route
        {...rest}
        render={(props) => {
          if (!signedIn) return <Redirect to="/login" />;
          if (!authorized) return <NotAuthorized />;
          return <Component {...props} />;
        }}
      />
    );
  };

  return (
    <AuthContext.Provider value={{ user, refetch }}>
      <Router>
        {loading ? (
          <h1>Loading</h1>
        ) : (
          <Layout>
            <Switch>
              <PrivateRoute path="/" component={ModelsList} exact mmLevel={1} />
              <PrivateRoute
                path="/algorithms/new"
                component={NewAlgorithm}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/algorithms/import"
                component={ImportAlgorithms}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/algorithms"
                component={AlgorithmsList}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/algorithms/:id"
                component={SingleAlgorithm}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/algorithms/:id/edit"
                component={UpdateAlgorithm}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/efficiency_types"
                component={EfficiencyTypesList}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/efficiency_types/new"
                component={NewEfficiencyType}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/efficiency_types/import"
                component={ImportEfficiencyTypes}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/efficiency_types/:id"
                component={SingleEfficiencyType}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/efficiency_types/:id/edit"
                component={UpdateEfficiencyType}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/profile"
                component={UserProfile}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/services"
                component={ServicesList}
                dmLevel={1}
                exact
              />
              <PrivateRoute
                path="/formats"
                component={FormatsList}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/formats/new"
                component={NewFormat}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/formats/import"
                component={ImportFormats}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/formats/:id"
                component={SingleFormat}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/formats/:id/edit"
                component={UpdateFormat}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/evaluation_types"
                component={EvaluationTypesList}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/evaluation_types/new"
                component={NewEvaluationType}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/evaluation_types/import"
                component={ImportEvaluationTypes}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/evaluation_types/:id"
                component={SingleEvaluationType}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/evaluation_types/:id/edit"
                component={UpdateEvaluationType}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/model_types"
                component={ModelTypesList}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/model_types/new"
                component={NewModelType}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/model_types/import"
                component={ImportModelTypes}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/model_types/:id"
                component={SingleModelType}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/model_types/:id/edit"
                component={UpdateModelType}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/datasets"
                component={DatasetsList}
                exact
                mmLevel={1}
              />
              <PrivateRoute
                path="/datasets/new"
                component={NewDataset}
                exact
                mmLevel={1}
              />
              <PrivateRoute
                path="/datasets/import"
                component={ImportDatasets}
                exact
                mmLevel={1}
              />
              <PrivateRoute
                path="/datasets/:id"
                component={SingleDataset}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/datasets/:id/edit"
                component={UpdateDataset}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/data_types"
                component={DataTypesList}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/data_types/new"
                component={NewDataType}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/data_types/import"
                component={ImportDataTypes}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/data_types/:id"
                component={SingleDataType}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/data_types/:id/edit"
                component={UpdateDataType}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/usages"
                component={UsagesList}
                exact
                mmLevel={1}
              />
              <PrivateRoute
                path="/usages/new"
                component={NewUsage}
                exact
                mmLevel={1}
              />
              <PrivateRoute
                path="/usages/import"
                component={ImportUsages}
                exact
                mmLevel={1}
              />
              <PrivateRoute
                path="/usages/:id"
                component={SingleUsage}
                exact
                mmLevel={1}
              />
              <PrivateRoute
                path="/usages/:id/edit"
                component={UpdateUsage}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/users/:id/edit"
                component={UpdateUser}
                tmLevel={3}
                exact
              />
              <PrivateRoute
                path="/users/:id"
                component={SingleUser}
                exact
                tmLevel={3}
              />
              <PrivateRoute
                path="/users"
                component={UsersList}
                exact
                tmLevel={3}
              />
              <PrivateRoute
                path="/models/new"
                component={NewModel}
                exact
                mmLevel={1}
              />
              <PrivateRoute
                path="/models/import"
                component={ImportModels}
                exact
                mmLevel={1}
              />
              <PrivateRoute
                path="/models/:id"
                component={SingleModel}
                exact
                mmLevel={1}
              />
              <PrivateRoute
                path="/models/:id/deploy"
                component={DeployModel}
                exact
                mmLevel={1}
                dmLevel={1}
              />
              <PrivateRoute
                path="/models/:id/edit"
                component={UpdateModel}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/models/:id/efficiencies"
                component={ModelEfficiencies}
                mmLevel={1}
                exact
              />
              <PrivateRoute
                path="/models/:id/evaluations"
                component={ModelEvaluations}
                mmLevel={1}
                exact
              />
              <Route path="/login" exact component={LoginPage} />
              <PrivateRoute component={NotFound} />
            </Switch>
          </Layout>
        )}
      </Router>
    </AuthContext.Provider>
  );
}
