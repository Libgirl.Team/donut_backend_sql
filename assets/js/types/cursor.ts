export interface Cursor {
  totalPages: number;
  page: number;
  itemsPerPage: number;
}

export type Page<T> = {
  cursor: Cursor;
  data: T[];
};
