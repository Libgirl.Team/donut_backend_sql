export interface ModelPreferences {
  columns: string[];
  evaluationTypeIDs: string[];
  efficiencyTypeIDs: string[];
  sortColumn: string;
  sortOrder: string;
}

export interface IPreferences {
  models: ModelPreferences;
}
