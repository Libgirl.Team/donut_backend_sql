import { DataType } from "./dataType";
import { User } from "shared/types/auth";

export interface Usage {
  __typename: "Usage";
  id: number;
  name: string;
  description: string;
  insertedAt: string;
  updatedAt: string;
  owner: User;
  inputType: DataType;
  outputType: DataType;
}
