import { Usage } from "./usage";
import { User } from "shared/types/auth";
import { Algorithm } from "./algorithm";
import { Model } from "./model";

export interface ModelType {
  id: number;
  name: string;
  description: string;
  insertedAt: string;
  updatedAt: string;
  usage: Usage;
  models: Model[];
  algorithm: Algorithm;
  owner: User;
  versionMap: { [major: number]: number };
  __typename: "ModelType";
}
