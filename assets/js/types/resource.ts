import { User } from "shared/types/auth";

export default interface Resource {
  __typename: string;
  id: string;
  name?: string;
  description?: string;
  insertedAt: string;
  updatedAt: string;
  owner: User;
  author?: User | null;
}
