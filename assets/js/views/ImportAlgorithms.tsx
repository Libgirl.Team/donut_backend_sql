import React from "react";
import GenericImport from "components/GenericImport/index";

const ImportAlgorithms = () => {
  return <GenericImport typename="Algorithm" />;
};

export default ImportAlgorithms;
