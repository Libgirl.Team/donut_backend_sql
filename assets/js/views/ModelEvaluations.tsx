import React, { FormEvent } from "react";
import EfficiencyEvaluationForm, {
  EfficiencyEvaluationParam
} from "components/EfficiencyEvaluationForm";
import gql from "graphql-tag";
import { useParams, useHistory } from "react-router";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { UPDATE_EVALUATIONS } from "../graphql/mutations/modelMutations";
import Loader from "components/Loader";
import SectionTitle from "components/SectionTitle";

const query = gql`
  query($id: ID!) {
    evaluationTypes {
      id
      name
      unit
      description
    }

    model(id: $id) {
      version
      modelType {
        name
      }
      evaluations {
        value
        typeId
      }
    }
  }
`;

export default () => {
  const { id } = useParams();
  const { loading, data } = useQuery(query, {
    variables: { id },
    fetchPolicy: "no-cache"
  });
  const history = useHistory();

  const [persist] = useMutation(UPDATE_EVALUATIONS);
  const handleSubmit = async (
    e: FormEvent,
    state: EfficiencyEvaluationParam[]
  ) => {
    e.preventDefault();
    await persist({ variables: { modelId: id, params: state } });
    history.push(`/models/${id}`);
  };
  if (loading) return <Loader />;
  const { model, evaluationTypes } = data;
  return (
    <div className="container">
      <SectionTitle>
        Model: {model.modelType?.name} v. {model.version}
      </SectionTitle>
      <h2>Evaluations</h2>
      <EfficiencyEvaluationForm
        options={evaluationTypes}
        data={model.evaluations}
        handleSubmit={handleSubmit}
      />
    </div>
  );
};
