import React from "react";
import CreateUpdateForm, {
  IFormFieldRendererProps
} from "components/CreateUpdateForm";
import gql from "graphql-tag";
import InputField from "components/InputField";
import TextArea from "components/TextArea";
import Select from "components/Select";

export interface IUsageParams {
  description: string;
  name: string;
  inputTypeId: number | string | null;
  outputTypeId: number | string | null;
}

const query = gql`
  query($id: ID!) {
    usage(id: $id) {
      id
      name
      description
      inputTypeId
      outputTypeId
    }
    dataTypes {
      key: id
      value: id
      text: name
    }
  }
`;

export const UsageFields = ({
  rawData,
  handleSelect,
  handleChange,
  params,
  errors
}: IFormFieldRendererProps) => (
  <>
    <InputField
      name="name"
      label="Name:"
      value={params.name}
      error={errors.name}
      onChange={handleChange}
    />
    <TextArea
      name="description"
      label="Description:"
      onChange={handleChange}
      error={errors.description}
      value={params.description}
    />
    <Select
      label="Input data type"
      name="inputTypeId"
      options={rawData?.dataTypes}
      value={params.inputTypeId}
      onChange={handleSelect}
      error={errors.inputTypeId}
      typename="DataType"
    />
    <Select
      label="Output data type"
      name="outputTypeId"
      options={rawData?.dataTypes}
      value={params.outputTypeId}
      onChange={handleSelect}
      error={errors.outputTypeId}
      typename="DataType"
    />
  </>
);

export const INITIAL_USAGE_PARAMS: IUsageParams = {
  name: "",
  description: "",
  inputTypeId: "",
  outputTypeId: ""
};

export default () => {
  return (
    <CreateUpdateForm
      typename="Usage"
      query={query}
      children={UsageFields}
      initialParams={INITIAL_USAGE_PARAMS}
    />
  );
};
