import React from "react";
import gql from "graphql-tag";
import GenericShow from "components/GenericShow";
import DependencyItem from "components/GenericShow/DependencyItem";

const query = gql`
  query($id: ID!) {
    evaluationType(id: $id) {
      id
      name
      description
      insertedAt
      updatedAt
      dataset {
        id
        name
        description
      }
    }
  }
`;

const SingleEvaluationType = () => {
  return (
    <GenericShow
      resourceType="Evaluation type"
      resourceKey="evaluationType"
      query={query}
      disableCache
    >
      {(resource) => (
        <>
          <h4>Dependencies</h4>
          <DependencyItem resource={resource.dataset} />
        </>
      )}
    </GenericShow>
  );
};

export default SingleEvaluationType;
