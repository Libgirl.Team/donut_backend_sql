import React, { useState, FormEvent, useCallback } from "react";
import { LIST_EFFICIENCIES_EVALUATIONS } from "../graphql/queries/modelQueries";
import { useQuery } from "@apollo/react-hooks";
import Preferences, { MODEL_COLUMN_HEADINGS } from "../actions/Preferences";
import { useSelector } from "react-redux";
import { RootState } from "../store/reducers";
import Checkbox from "components/Checkbox";
import { Grid } from "semantic-ui-react";
import Modal from "components/Modal";
import store from "../store";

function checkedToMap(list: any[]) {
  return list.reduce((map, key) => {
    map[key] = true;
    return map;
  }, {});
}

function serialize(columns: object, efficiencies: object, evaluations: object) {
  const {
    preferences: {
      models: { sortColumn, sortOrder }
    }
  } = store.getState();
  const [c, eff, ev] = [columns, efficiencies, evaluations].map((map: any) =>
    Object.keys(map).reduce((acc: any, val: any) => {
      return map[val] ? [...acc, val] : acc;
    }, [])
  );
  return {
    models: {
      columns: c,
      efficiencyTypeIDs: eff,
      evaluationTypeIDs: ev,
      sortColumn,
      sortOrder
    }
  };
}

interface Props {
  handleClose(): void;
}

const ModelTablePreferences = ({ handleClose }: Props) => {
  const preferences = useSelector((s: RootState) => s.preferences.models);
  const { data, loading } = useQuery(LIST_EFFICIENCIES_EVALUATIONS);
  const [columns, setColumns] = useState(checkedToMap(preferences.columns));
  const [efficiencies, setEfficiencies] = useState(
    checkedToMap(preferences.efficiencyTypeIDs)
  );
  const [evaluations, setEvaluations] = useState(
    checkedToMap(preferences.evaluationTypeIDs)
  );
  const efficiencyTypes = data?.efficiencyTypes;
  const evaluationTypes = data?.evaluationTypes;

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    const payload = serialize(columns, efficiencies, evaluations);
    await Preferences.updatePreferences(payload);
    handleClose();
  };

  const handleReset = useCallback(async (e: FormEvent) => {
    e.preventDefault();
    await Preferences.resetPreferences();
    handleClose();
  }, []);

  const setter = (setState: (v: any) => any) => {
    return (e: any) => {
      const name = e.target.name;
      setState((columns: any) => ({ ...columns, [name]: !columns[name] }));
    };
  };

  return (
    <Modal
      handleClose={handleClose}
      handleSubmit={handleSubmit}
      handleReset={handleReset}
      submitLabel="Apply"
      positive
      title="Model Display Preferences"
    >
      <form onSubmit={handleSubmit} className="preferences">
        <Grid columns={3}>
          <Grid.Row>
            <Grid.Column>
              <h4>Columns</h4>
              {Object.keys(MODEL_COLUMN_HEADINGS).map((key: any) => (
                <Checkbox
                  key={key}
                  label={MODEL_COLUMN_HEADINGS[key]}
                  value={columns[key] || false}
                  onChange={setter(setColumns)}
                  name={key}
                />
              ))}
            </Grid.Column>
            <Grid.Column>
              <h4>Efficiencies</h4>
              {loading ? (
                <p>Loading</p>
              ) : (
                efficiencyTypes.map((type: any) => (
                  <Checkbox
                    key={type.id}
                    label={type.name}
                    value={efficiencies[type.id] || false}
                    name={type.id}
                    onChange={setter(setEfficiencies)}
                  />
                ))
              )}
            </Grid.Column>
            <Grid.Column>
              <h4>Evaluations</h4>
              {loading ? (
                <p>Loading</p>
              ) : (
                evaluationTypes.map((type: any) => (
                  <Checkbox
                    key={type.id}
                    label={type.name}
                    value={evaluations[type.id] || false}
                    name={type.id}
                    onChange={setter(setEvaluations)}
                  />
                ))
              )}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </form>
    </Modal>
  );
};

export default ModelTablePreferences;
