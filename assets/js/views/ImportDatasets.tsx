import React from "react";
import { ExampleSchema } from "components/GenericImport/SampleJson";
import GenericImport from "components/GenericImport/index";

const schema: ExampleSchema = {
  name: "string",
  description: "string",
  datasetURI: "string"
};

const ImportDatasets = () => {
  return <GenericImport typename="Dataset" schema={schema} />;
};

export default ImportDatasets;
