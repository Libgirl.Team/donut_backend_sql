import React from "react";
import { LIST_USERS } from "../graphql/queries/userQueries";
import { Table } from "semantic-ui-react";
import day from "dayjs";
import GenericTable from "components/GenericTable";
import { User } from "shared/types/auth";
import UserHelper from "../helpers/UserHelper";

const Header = () => [
  <Table.HeaderCell name="email">Email</Table.HeaderCell>,
  <Table.HeaderCell name="name">Name</Table.HeaderCell>,
  <Table.HeaderCell name="tm_level">Team</Table.HeaderCell>,
  <Table.HeaderCell name="mm_level">Model</Table.HeaderCell>,
  <Table.HeaderCell name="dm_level">Deployment</Table.HeaderCell>,
  <Table.HeaderCell name="inserted_at" className="date-header">
    Date added
  </Table.HeaderCell>
];

const Row = (data: User) => (
  <>
    <Table.Cell>{data.email}</Table.Cell>
    <Table.Cell>{data.name}</Table.Cell>
    <Table.Cell>{UserHelper.numericToLevel(data.tmLevel)}</Table.Cell>
    <Table.Cell>{UserHelper.numericToLevel(data.mmLevel)}</Table.Cell>
    <Table.Cell>{UserHelper.numericToLevel(data.dmLevel)}</Table.Cell>
    <Table.Cell className="date-value">
      {day(data.insertedAt).format("YYYY-MM-DD")}
    </Table.Cell>
  </>
);

export default function UsersList() {
  return (
    <GenericTable
      fixed
      typename="User"
      query={LIST_USERS}
      heading="Users"
      renderHeader={Header}
      searchUrl="/users/"
      renderRow={Row}
      dataKey="users"
    />
  );
}
