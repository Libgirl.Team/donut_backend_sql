import React from "react";
import { ExampleSchema } from "components/GenericImport/SampleJson";
import GenericImport from "components/GenericImport/index";

const schema: ExampleSchema = {
  name: "string",
  description: "string",
  unit: "string",
  datasetId: "id"
};

const ImportEvaluationTypes = () => {
  return <GenericImport typename="EvaluationType" schema={schema} />;
};

export default ImportEvaluationTypes;
