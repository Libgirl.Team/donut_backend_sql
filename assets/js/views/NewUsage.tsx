import React from "react";
import CreateUpdateForm from "components/CreateUpdateForm";
import gql from "graphql-tag";
import { INITIAL_USAGE_PARAMS, UsageFields } from "./UpdateUsage";

const FETCH_USAGE_OPTIONS = gql`
  {
    dataTypes {
      key: id
      value: id
      text: name
    }
  }
`;

export default () => {
  return (
    <CreateUpdateForm
      create
      query={FETCH_USAGE_OPTIONS}
      typename="Usage"
      initialParams={INITIAL_USAGE_PARAMS}
      children={UsageFields}
    />
  );
};
