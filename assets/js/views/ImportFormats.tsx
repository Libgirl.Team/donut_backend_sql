import React from "react";
import GenericImport from "components/GenericImport/index";

const ImportFormats = () => {
  return <GenericImport typename="Format" />;
};

export default ImportFormats;
