import React, { useEffect, useState, useCallback } from "react";
import SectionTitle from "components/SectionTitle";
import { LIST_MODELS } from "../graphql/queries/modelQueries";
import { Table, Button, Icon } from "semantic-ui-react";
import Loader from "components/Loader";
import { useQuery } from "@apollo/react-hooks";
import Header from "components/ModelTable/ModelTableHeader";
import Body from "components/ModelTable/ModelTableBody";
import { useSelector } from "react-redux";
import { RootState } from "../store/reducers";
import Preferences from "../actions/Preferences";
import ModelTablePreferences from "./ModelTablePreferences";
import CreateButton from "components/CreateButton";
import Pagination from "components/Pagination";
import usePageFromLocation from "../hooks/usePageFromLocation";
import SearchForm from "components/SearchForm";
import Breadcrumb from "components/Breadcrumb";

export default () => {
  const { page, term } = usePageFromLocation();
  const preferences = useSelector((s: RootState) => s.preferences);
  const [showPreferences, setShowPreferences] = useState(false);
  useEffect(() => {
    Preferences.fetchPreferences();
  }, []);
  const { evaluationTypeIDs, efficiencyTypeIDs } = preferences.models;
  const { data, loading } = useQuery(LIST_MODELS, {
    variables: {
      evaluationTypeIDs,
      efficiencyTypeIDs,
      page,
      term,
      sortColumn: preferences.models.sortColumn,
      sortOrder: preferences.models.sortOrder
    },
    fetchPolicy: "no-cache"
  });
  if (loading && !data) return <Loader />;
  const response = data?.paginateModels;
  const actionButtons = <CreateButton to="/models/new" />;
  return (
    <>
      <Breadcrumb resource="Model" action="index" />
      <SectionTitle actionButtons={actionButtons}>Models</SectionTitle>
      <div className="space-between">
        <Button
          icon
          labelPosition="left"
          onClick={() => setShowPreferences(true)}
          size="small"
        >
          <Icon name="configure" />
          Customize table
        </Button>
        <SearchForm baseUrl="/" />
      </div>
      <Table
        celled
        fixed
        className={`hoverable generic_table ${
          loading ? "generic_table--loading" : ""
        }`}
      >
        <Header
          preferences={preferences}
          evaluationTypes={data.evaluationTypes}
          efficiencyTypes={data.efficiencyTypes}
        />
        <Body preferences={preferences} data={response} />
      </Table>
      <Pagination cursor={response?.cursor} basePath="/" />
      {showPreferences && (
        <ModelTablePreferences handleClose={() => setShowPreferences(false)} />
      )}
    </>
  );
};
