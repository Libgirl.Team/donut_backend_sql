import React from "react";
import gql from "graphql-tag";
import GenericShow from "components/GenericShow";
import { MODEL_TYPE_DETAILS } from "../graphql/queries/modelTypeQueries";
import DependencyItem from "components/GenericShow/DependencyItem";
import EfficiencyTypePicker from "components/EfficiencyTypePicker";
import { Model } from "../types/model";
import { Link } from "react-router-dom";

const query = gql`
  ${MODEL_TYPE_DETAILS}
  query($id: ID!) {
    modelType(id: $id) {
      ...ModelTypeDetails
      description
      insertedAt
      updatedAt
      algorithm {
        description
      }
      models {
        id
        version
      }
    }
  }
`;

const SingleModelType = () => {
  return (
    <GenericShow
      resourceType="Model type"
      resourceKey="modelType"
      query={query}
    >
      {(resource) => (
        <>
          <EfficiencyTypePicker modelTypeId={resource.id} />
          <h4>Dependencies</h4>
          <DependencyItem resource={resource.algorithm} />
          <h4>Dependent resources: models</h4>
          {resource.models.map((model: Model) => (
            <>
              <Link to={`/models/${model.id}`} key={model.id}>
                {model.version} (#{model.id})
              </Link>
              ,{" "}
            </>
          ))}
        </>
      )}
    </GenericShow>
  );
};

export default SingleModelType;
