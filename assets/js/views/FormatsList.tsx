import React from "react";
import gql from "graphql-tag";
import GenericTable from "components/GenericTable";

const query = gql`
  query($page: Int, $sortColumn: String, $sortOrder: SortOrder) {
    formats: paginateFormats(
      page: $page
      sortColumn: $sortColumn
      sortOrder: $sortOrder
    ) {
      data {
        id
        name
        description
        insertedAt
        owner {
          id
          name
        }
      }
      cursor {
        page
        totalPages
      }
    }
  }
`;

const FormatsList = () => (
  <GenericTable
    query={query}
    typename="Format"
    heading="Formats"
    dataKey="formats"
  />
);

export default FormatsList;
