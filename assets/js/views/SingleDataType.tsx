import React from "react";
import gql from "graphql-tag";
import GenericShow from "components/GenericShow";

const query = gql`
  query($id: ID!) {
    dataType(id: $id) {
      id
      name
      owner {
        id
        name
      }
      description
      insertedAt
      updatedAt
      usages {
        id
        name
      }
    }
  }
`;

const SingleDataType = () => {
  return (
    <GenericShow
      resourceType="Data type"
      resourceKey="dataType"
      query={query}
      dependentResources={["usages"]}
    />
  );
};

export default SingleDataType;
