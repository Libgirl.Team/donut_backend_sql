import React from "react";
import CreateUpdateForm from "components/CreateUpdateForm";
import { DatasetFields, INITIAL_DATASET_PARAMS } from "./UpdateDataset";

export default () => {
  return (
    <CreateUpdateForm
      create
      typename="Dataset"
      initialParams={INITIAL_DATASET_PARAMS}
      children={DatasetFields}
    />
  );
};
