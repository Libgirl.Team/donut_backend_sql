import React from "react";
import { LIST_DATASETS } from "../graphql/queries/datasetQueries";
import GenericTable from "components/GenericTable";

export default () => (
  <GenericTable
    query={LIST_DATASETS}
    typename="Dataset"
    heading="Datasets"
    dataKey="datasets"
  />
);
