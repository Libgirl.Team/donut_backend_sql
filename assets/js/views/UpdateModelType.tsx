import React from "react";
import CreateUpdateForm from "components/CreateUpdateForm";
import gql from "graphql-tag";
import { INITIAL_MODEL_TYPE_PARAMS, ModelTypeFields } from "./NewModelType";

const query = gql`
  query($id: ID!) {
    modelType(id: $id) {
      id
      name
      algorithmId
      usageId
      description
    }
    algorithms {
      key: id
      value: id
      text: name
    }
    usages {
      key: id
      value: id
      text: name
    }
  }
`;

export default () => {
  return (
    <CreateUpdateForm
      typename="ModelType"
      query={query}
      initialParams={INITIAL_MODEL_TYPE_PARAMS}
      children={ModelTypeFields}
    />
  );
};
