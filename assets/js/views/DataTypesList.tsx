import React from "react";
import GenericTable from "components/GenericTable";
import gql from "graphql-tag";

const query = gql`
  query($page: Int, $sortColumn: String, $sortOrder: SortOrder) {
    dataTypes: paginateDataTypes(
      page: $page
      sortColumn: $sortColumn
      sortOrder: $sortOrder
    ) {
      data {
        id
        name
        owner {
          name
        }
        insertedAt
      }
      cursor {
        page
        totalPages
      }
    }
  }
`;

export default () => (
  <GenericTable
    query={query}
    typename="DataType"
    heading="Data Types"
    dataKey="dataTypes"
  />
);
