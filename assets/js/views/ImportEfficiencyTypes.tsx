import React from "react";
import { ExampleSchema } from "components/GenericImport/SampleJson";
import GenericImport from "components/GenericImport/index";

const schema: ExampleSchema = {
  name: "string",
  description: "string",
  unit: "string"
};

const ImportEfficiencyTypes = () => {
  return <GenericImport typename="EfficiencyType" schema={schema} />;
};

export default ImportEfficiencyTypes;
