import React from "react";
import CreateUpdateForm, {
  IFormFieldRendererProps
} from "components/CreateUpdateForm";
import gql from "graphql-tag";
import { ManagementLevel } from "shared/types/auth";
import Select from "components/Select";
import { Grid } from "semantic-ui-react";

export interface UserParams {
  mmLevel: ManagementLevel;
  tmLevel: ManagementLevel;
  dmLevel: ManagementLevel;
}

const query = gql`
  query($id: ID!) {
    user(id: $id) {
      id
      name
      email
      mmLevel
      tmLevel
      dmLevel
    }
  }
`;

export const initialParams: UserParams = {
  mmLevel: 0,
  tmLevel: 0,
  dmLevel: 0
};

const levels = [
  { key: "0", value: 0, text: "Default" },
  { key: "1", value: 1, text: "User" },
  { key: "2", value: 2, text: "Maintainer" },
  { key: "3", value: 3, text: "Admin" }
];

const UpdateUser = () => {
  return (
    <CreateUpdateForm
      typename="User"
      query={query}
      initialParams={initialParams}
    >
      {({ handleSelect, errors, params }: IFormFieldRendererProps) => (
        <Grid columns={3}>
          <Grid.Column>
            <Select
              label="Team Management"
              name="tmLevel"
              value={params.tmLevel}
              onChange={handleSelect}
              options={levels}
              error={errors.tmLevel}
            />
          </Grid.Column>
          <Grid.Column>
            <Select
              label="Model Management"
              name="mmLevel"
              value={params.mmLevel}
              onChange={handleSelect}
              options={levels}
              error={errors.mmLevel}
            />
          </Grid.Column>
          <Grid.Column>
            <Select
              label="Deployment Management"
              name="dmLevel"
              value={params.dmLevel}
              onChange={handleSelect}
              options={levels}
              error={errors.dmLevel}
            />
          </Grid.Column>
        </Grid>
      )}
    </CreateUpdateForm>
  );
};

export default UpdateUser;
