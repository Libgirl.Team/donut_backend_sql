import React from "react";
import { GET_MODEL } from "../graphql/queries/modelQueries";
import EfficiencyEvaluationTable, {
  TableType
} from "components/EfficiencyEvaluationTable";
import { Card } from "semantic-ui-react";
import { useParams } from "react-router-dom";
import CopyToClipboard from "components/CopyToClipboard";
import ModelDiagram from "components/models/ModelDiagram";
import { useQuery } from "@apollo/react-hooks";
import Loader from "components/Loader";
import Markdown from "components/Markdown";
import EditButton from "components/EditButton";
import ModelDependencies from "components/models/Dependencies";
import { Link } from "react-router-dom";
import SourceModel from "components/models/SourceModel";
import { Model } from "types/model";
import NotFound from "./NotFound";
import DeploymentSection from "components/models/DeploymentSection";
import ModelBreadcrumb from "components/models/ModelBreadcrumb";
import SingleModelHeader from "components/models/SingleModelHeader";
import DeactivationNotice from "components/models/DeactivationNotice";

interface GetModelQuery {
  model: Model;
}

const SingleModel = () => {
  const { id } = useParams();
  const { data, loading } = useQuery<GetModelQuery>(GET_MODEL, {
    variables: { id }
  });
  if (loading) return <Loader />;
  const resource = data?.model as Model;
  const active = !resource?.deactivatedAt;
  if (!resource) return <NotFound />;
  return (
    <>
      <ModelBreadcrumb model={resource} />
      <div className={`single_model ${active ? "" : "single_model--inactive"}`}>
        <div className="container">
          <SingleModelHeader resource={resource} />
          {active ? null : <DeactivationNotice />}
          <section className="single_model__content">
            <ModelDiagram modelType={resource.modelType} />
            <CopyToClipboard value={resource.id} label="Model ID" />
            <Card fluid>
              <div className="single_model__description__header">
                Description
                <EditButton to={`/models/${resource.id}/edit`} />
              </div>
              <Card.Content>
                <Card.Description>
                  <Markdown source={resource.description} />
                </Card.Description>
              </Card.Content>
            </Card>
            <CopyToClipboard value={resource.modelURI} label="Model URI" />
            <EfficiencyEvaluationTable
              model={resource}
              type={TableType.Efficiencies}
            />
            <EfficiencyEvaluationTable
              model={resource}
              type={TableType.Evaluations}
            />
            <p className="single_model__format">
              <strong>Format:</strong>{" "}
              <Link to={`/formats/${resource.format?.id}`}>
                {resource.format?.name}
              </Link>
            </p>
            <SourceModel resource={resource} />
            <DeploymentSection model={resource} />
          </section>
        </div>
        <ModelDependencies resource={resource} />
      </div>
    </>
  );
};

export default SingleModel;
