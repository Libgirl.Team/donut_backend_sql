import React from "react";
import gql from "graphql-tag";
import GenericShow from "components/GenericShow";

const query = gql`
  query($id: ID!) {
    format(id: $id) {
      id
      name
      owner {
        id
        name
      }
      description
      deployable
      insertedAt
      updatedAt
      models {
        id
        version
        modelType {
          id
          name
        }
      }
    }
  }
`;

const SingleFormat = () => {
  return (
    <GenericShow
      resourceType="Format"
      resourceKey="format"
      query={query}
      dependentResources={["models"]}
    />
  );
};

export default SingleFormat;
