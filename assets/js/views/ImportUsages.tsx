import React from "react";
import { ExampleSchema } from "components/GenericImport/SampleJson";
import GenericImport from "components/GenericImport/index";

const schema: ExampleSchema = {
  name: "string",
  description: "string",
  inputTypeId: "id",
  outputTypeId: "id"
};

const ImportUsages = () => {
  return <GenericImport typename="Usage" schema={schema} />;
};

export default ImportUsages;
