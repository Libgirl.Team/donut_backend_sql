import React from "react";
import GenericImport from "components/GenericImport/index";
import { ExampleSchema } from "components/GenericImport/SampleJson";

const schema: ExampleSchema = {
  name: "string",
  description: "string",
  algorithmId: "id",
  usageId: "id"
};

const ImportModelTypes = () => {
  return <GenericImport typename="ModelType" schema={schema} />;
};

export default ImportModelTypes;
