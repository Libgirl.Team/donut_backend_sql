import React, {
  Reducer,
  useReducer,
  useEffect,
  useCallback,
  ChangeEvent,
  FormEvent
} from "react";
import gql from "graphql-tag";
import { Grid, Form } from "semantic-ui-react";
import InputField from "components/InputField";
import Select from "components/Select";
import TextArea from "components/TextArea";
import { IModelParams } from "../actions/Models";
import { useQuery, useMutation, useLazyQuery } from "@apollo/react-hooks";
import Loader from "components/Loader";
import { ModelType } from "../types/modelType";
import { createMutation } from "../graphql/helpers";
import SaveButton from "components/SaveButton";
import { Redirect } from "react-router-dom";
import SectionTitle from "components/SectionTitle";
import ImportButton from "components/ImportButton";

const FETCH_MODEL_OPTIONS = gql`
  {
    modelTypes {
      id
      key: id
      value: id
      text: name
      versionMap
    }
    datasets {
      key: id
      value: id
      text: name
    }
    formats {
      key: id
      value: id
      text: name
    }
  }
`;

const fetchModelTypeModels = gql`
  query($id: ID!) {
    modelType(id: $id) {
      name
      models {
        id
        key: id
        value: id
        text: displayName
      }
    }
  }
`;

export const initialParams: IModelParams = {
  description: "",
  major: 1,
  minor: 0,
  modelTypeId: undefined,
  sourceModelId: undefined,
  authorId: undefined,
  datasetId: undefined,
  formatId: undefined,
  modelURI: ""
};

const initialState = {
  modelTypes: [],
  versions: {},
  params: initialParams
};

const maxMajor = (modelType: ModelType | undefined) => {
  const v = modelType?.versionMap;
  if (!v) return 0;
  return Math.max(...Object.keys(v).map(Number));
};

const canMajorBeZero = (modelType: ModelType | undefined) => {
  if (!modelType) return true;
  const v = modelType?.versionMap;
  if (!v) return true;
  return typeof v[0] !== "undefined";
};

interface ModelReducerState {
  params: IModelParams;
  modelTypes: ModelType[];
  versions: { [major: number]: number };
}

enum EventType {
  SetParam,
  SetModelType,
  SetMajor,
  LoadModelTypes
}

interface ModelReducerEvent {
  type: EventType;
  payload: any;
}

const modelParamReducer: Reducer<ModelReducerState, ModelReducerEvent> = (
  state: ModelReducerState,
  { type, payload }: ModelReducerEvent
) => {
  switch (type) {
    case EventType.LoadModelTypes:
      return {
        ...state,
        modelTypes: payload
      };
    case EventType.SetParam: {
      const { field, value } = payload;
      return {
        ...state,
        params: {
          ...state.params,
          [field]: value
        }
      };
    }
    case EventType.SetModelType: {
      const mt = state.modelTypes.find((m) => m.id === payload.value);
      const major = maxMajor(mt);
      const minor =
        mt?.versionMap?.[major] === undefined ? 0 : mt?.versionMap?.[major] + 1;
      return {
        ...state,
        params: {
          ...state.params,
          modelTypeId: mt?.id,
          major,
          minor
        }
      };
    }
    case EventType.SetMajor: {
      const newMajor = payload.value;
      const mt = state.modelTypes.find(
        (m) => m.id === state.params.modelTypeId
      );
      const versions = mt?.versionMap || {};
      const minor =
        versions[newMajor] === undefined ? 0 : versions[newMajor] + 1;
      return {
        ...state,
        params: {
          ...state.params,
          major: newMajor,
          minor
        }
      };
    }
    default:
      return state;
  }
};

const mutation = createMutation("Model");

export default () => {
  const { data, loading } = useQuery(FETCH_MODEL_OPTIONS, {
    fetchPolicy: "no-cache"
  });

  const [state, dispatch] = useReducer(modelParamReducer, initialState);
  const [mutate, { called, data: mutationResponse }] = useMutation(mutation);
  const params = state.params;
  const result = mutationResponse?.createModel;
  const errors = result?.errors || {};
  const modelType = state.modelTypes.find(
    (mt) => state.params.modelTypeId === mt.id
  );
  const maxPossibleMajor = maxMajor(modelType) + 1;
  const zeroable = canMajorBeZero(modelType);
  const [loadModels, { data: modelsData }] = useLazyQuery(fetchModelTypeModels);
  const modelTypeId = params?.modelTypeId;

  useEffect(() => {
    if (!loading) {
      const { modelTypes } = data;
      dispatch({
        type: EventType.LoadModelTypes,
        payload: modelTypes
      });
    }
  }, [loading]);

  useEffect(() => {
    loadModels({ variables: { id: modelTypeId } });
  }, [modelTypeId, loadModels]);

  const selectedModelType = modelsData?.modelType;
  const modelOptionsForSelect = selectedModelType?.models.map((m: any) => ({
    ...m,
    text: `${selectedModelType.name} v. ${m.text}`
  }));

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    return mutate({ variables: { params } });
  };

  const handleSelect = useCallback(
    (_event: ChangeEvent<HTMLSelectElement>, { name, value }: any) => {
      dispatch({
        type:
          name === "modelTypeId" ? EventType.SetModelType : EventType.SetParam,
        payload: { field: name, value }
      });
    },
    [dispatch]
  );

  const handleChange = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = e.target;
    dispatch({
      type: name === "major" ? EventType.SetMajor : EventType.SetParam,
      payload: { field: name, value }
    });
  };

  if (loading) return <Loader />;
  if (called && result?.success) {
    return <Redirect to={`/models/${result?.resource?.id}`} />;
  }

  return (
    <>
      <SectionTitle actionButtons={<ImportButton typename="Model" />}>
        Create model
      </SectionTitle>
      <Form onSubmit={handleSubmit}>
        <Grid columns={3}>
          <Grid.Row>
            <Grid.Column>
              <Select
                autoFocus
                label="Model type:"
                name="modelTypeId"
                options={data.modelTypes}
                value={params.modelTypeId}
                onChange={handleSelect}
                error={errors.modelTypeId}
                typename="ModelType"
              />
            </Grid.Column>
            <Grid.Column>
              <InputField
                label="Major:"
                name="major"
                type="number"
                value={params.major}
                onChange={handleChange}
                min={zeroable ? 0 : 1}
                max={maxPossibleMajor}
                step={1}
              />
            </Grid.Column>
            <Grid.Column>
              <InputField
                label="Minor:"
                readOnly
                name="minor"
                value={params.minor}
                error={errors.minor}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <br />
        <Select
          label="Dataset:"
          name="datasetId"
          options={data.datasets}
          value={params.datasetId}
          onChange={handleSelect}
          error={errors.datasetId}
          typename="Dataset"
        />
        <Select
          label="Format:"
          name="formatId"
          options={data.formats}
          value={params.formatId}
          onChange={handleSelect}
          error={errors.formatId}
          typename="Dataset"
        />
        <Select
          label="Source model:"
          name="sourceModelId"
          options={modelOptionsForSelect}
          value={params.sourceModelId}
          onChange={handleSelect}
          error={errors.sourceModelId}
        />
        <InputField
          label="Model URI:"
          placeholder="gs://bucket-name/resource.bin"
          name="modelURI"
          value={params.modelURI}
          onChange={handleChange}
          error={errors.modelURI}
        />
        <TextArea
          label="Description:"
          name="description"
          value={params.description}
          onChange={handleChange}
          error={errors.description}
        />
        <SaveButton />
      </Form>
    </>
  );
};
