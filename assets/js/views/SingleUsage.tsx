import React from "react";
import gql from "graphql-tag";
import GenericShow from "components/GenericShow";
import DependencyItem from "components/GenericShow/DependencyItem";

const query = gql`
  query($id: ID!) {
    usage(id: $id) {
      id
      name
      owner {
        id
        name
      }
      inputType {
        id
        name
        description
      }
      outputType {
        id
        name
        description
      }
      description
      insertedAt
      updatedAt
      modelTypes {
        id
        name
      }
    }
  }
`;

const SingleUsage = () => {
  return (
    <GenericShow
      resourceType="Usage"
      resourceKey="usage"
      query={query}
      dependentResources={["modelTypes"]}
      disableCache
    >
      {(resource) => (
        <>
          <h4>Dependencies</h4>
          <DependencyItem
            resource={resource.inputType}
            resourceType="Input type"
          />
          <DependencyItem
            resource={resource.outputType}
            resourceType="Output type"
          />
        </>
      )}
    </GenericShow>
  );
};

export default SingleUsage;
