import React from "react";
import CreateUpdateForm from "components/CreateUpdateForm";

const NewFormat = () => {
  return <CreateUpdateForm create typename="Format" />;
};

export default NewFormat;
