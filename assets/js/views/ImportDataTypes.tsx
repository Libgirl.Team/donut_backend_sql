import React from "react";
import { ExampleSchema } from "components/GenericImport/SampleJson";
import GenericImport from "components/GenericImport/index";

const schema: ExampleSchema = {
  name: "string",
  description: "string"
};

const ImportDataTypes = () => {
  return <GenericImport typename="DataType" schema={schema} />;
};

export default ImportDataTypes;
