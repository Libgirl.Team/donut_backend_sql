import React, { useState, FormEvent } from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { useParams } from "react-router-dom";
import { GET_MODEL } from "../graphql/queries/modelQueries";
import Loader from "components/Loader";
import SectionTitle from "components/SectionTitle";
import { Search, Button, Form } from "semantic-ui-react";
import gql from "graphql-tag";

const query = gql`
  {
    services {
      id
      name
      url
    }
  }
`;

const mutation = gql`
  mutation deployModel($id: ID!, $service: String!) {
    deployModel(id: $id, serviceName: $service) {
      success
    }
  }
`;

const DeployModel = () => {
  const { id } = useParams();
  const { data, loading } = useQuery(GET_MODEL, { variables: { id } });
  const [term, setTerm] = useState("");
  const [mutate] = useMutation(mutation);
  let results: any[] = [];

  const onChange = (_: any, data: any) => {
    setTerm(data?.value);
  };

  const onResultSelect = (_: any, { result }: any) => {
    setTerm(result.title);
  };

  const onSubmit = (_: FormEvent) => {
    mutate({ variables: { id, service: term } });
  };

  const { data: serviceData, loading: servicesLoading } = useQuery(query);
  if (loading) {
    return <Loader />;
  }
  const services = serviceData?.services;
  if (term.length > 0 && services?.length) {
    results = services
      .filter(({ name }: any) => {
        return name.indexOf(term) !== -1 && name !== term;
      })
      .map(({ name, url }: any) => ({
        title: name,
        description: url
      }));
  }

  const model = data.model;
  return (
    <div className="deploy_model">
      <div className="container">
        <SectionTitle vertical>
          Deploy model: {model.modelType.name} v. {model.version}
        </SectionTitle>
        <p>
          Using this feature, you can deploy a single model stored in TensorFlow
          Saved Model format (*.pb) to your organization&apos;s GCP account.
          This feature is a convenience wrapper for&#20;
          <a
            href="https://www.tensorflow.org/tfx/guide/serving"
            target="_blank"
            rel="noopener noreferrer"
          >
            TensorFlow Serving
          </a>{" "}
          and can be used to quickly get a model online and test it on other
          platforms or devices.
        </p>
        <p>
          The deployment job will download the file located at the URI below and
          deploy it as a Google Cloud Run service. This requires that the file
        </p>
        <Form onSubmit={onSubmit}>
          <Form.Field>
            <label>
              Please pick a name for the service. (autocompleteable)
            </label>
            <Search
              loading={servicesLoading}
              icon={null}
              value={term}
              onSearchChange={onChange}
              onResultSelect={onResultSelect}
              results={results}
              className="deploy_model__input"
              autoFocus
              required
              showNoResults={false}
            ></Search>
          </Form.Field>
          <Button positive type="submit">
            Deploy
          </Button>
        </Form>
      </div>
    </div>
  );
};

export default DeployModel;
