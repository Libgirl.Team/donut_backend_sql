import React from "react";
import GenericTable from "components/GenericTable";
import gql from "graphql-tag";
import { Table } from "semantic-ui-react";
import day from "dayjs";

const query = gql`
  {
    services {
      id
      name
      url
      insertedAt
    }
  }
`;

const Header = () => [
  <Table.HeaderCell>Name</Table.HeaderCell>,
  <Table.HeaderCell>URL</Table.HeaderCell>,
  <Table.HeaderCell>Created on</Table.HeaderCell>
];

const Row = (data: any) => (
  <>
    <Table.Cell>{data.name}</Table.Cell>
    <Table.Cell>{data.url}</Table.Cell>
    <Table.Cell className="date-value">
      {day(data.insertedAt).format("YYYY-MM-DD")}
    </Table.Cell>
  </>
);

export default () => (
  <GenericTable
    query={query}
    typename="Service"
    heading="Services"
    dataKey="services"
    renderHeader={Header}
    renderRow={Row}
  />
);
