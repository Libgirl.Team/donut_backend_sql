import React from "react";
import CreateUpdateForm from "components/CreateUpdateForm";
import gql from "graphql-tag";
import {
  INITIAL_EFFICIENCY_TYPE_PARAMS,
  EfficiencyTypeFields
} from "./NewEfficiencyType";

const query = gql`
  query($id: ID!) {
    efficiencyType(id: $id) {
      id
      name
      description
      unit
    }
  }
`;

export default () => (
  <CreateUpdateForm
    typename="EfficiencyType"
    initialParams={INITIAL_EFFICIENCY_TYPE_PARAMS}
    query={query}
    children={EfficiencyTypeFields}
    columns={["name", "description", "unit"]}
  />
);
