import React, { useCallback, useState } from "react";
import SectionTitle from "components/SectionTitle";
import { useParams } from "react-router";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import Loader from "components/Loader";
import NotFound from "./NotFound";
import Container from "components/Container";
import UserHelper from "../helpers/UserHelper";
import day from "dayjs";
import { useAuthContext } from "../context/AuthContext";
import Permissions from "../../../src/shared/permissions";
import EditButton from "components/EditButton";
import DeleteButton from "components/DeleteButton";
import DeleteModal from "components/DeleteModal";
import DeactivateButton from "components/DeactivateButton";
import DeactivationModal from "components/DeactivationModal";
import CopyToClipboard from "components/CopyToClipboard";
import Breadcrumb from "components/Breadcrumb";

const query = gql`
  query($id: ID!) {
    user(id: $id) {
      id
      active
      name
      email
      mmLevel
      tmLevel
      dmLevel
      deactivatedAt
      insertedAt
      updatedAt
    }
  }
`;

export default function SingleUser() {
  const { user: currentUser } = useAuthContext();
  const { id } = useParams();
  const { data, loading } = useQuery(query, {
    variables: { id },
    fetchPolicy: "no-cache"
  });
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [showDeactivateModal, setShowDeactivateModal] = useState(false);

  const toggleDeleteModal = useCallback(() => {
    setShowDeleteModal((s) => !s);
  }, [setShowDeleteModal]);
  const toggleDeactivateModal = useCallback(() => {
    setShowDeactivateModal((s) => !s);
  }, [setShowDeactivateModal]);

  const user = data?.user;

  if (loading) return <Loader />;
  if (!loading && !user) return <NotFound />;
  const deleteable = Permissions.can(currentUser, "deleteUser", user);
  const deactivateable = Permissions.can(currentUser, "deactivateUser", user);
  const updateable = Permissions.can(currentUser, "updateUser", user);

  const actionButtons = (
    <>
      {updateable ? <EditButton to={`/users/${user.id}/edit`} /> : null}
      {deleteable ? <DeleteButton onClick={toggleDeleteModal} /> : null}
      {deactivateable ? (
        <DeactivateButton
          onClick={toggleDeactivateModal}
          active={user.active}
        />
      ) : null}
    </>
  );

  return (
    <>
      <Breadcrumb resource={user} action="show" />
      <Container>
        <SectionTitle actionButtons={actionButtons} vertical>
          User: {user.name} ({user.email})
        </SectionTitle>
        <h4>Basic informations</h4>
        <CopyToClipboard label="Email" value={user.email} />
        <p className="resource__inserted_at">
          <strong>Created at:</strong>{" "}
          {day(user.insertedAt).format("YYYY-MM-DD HH:mm:ss (UTCZ)")}
        </p>
        <p className="resource__update_at">
          <strong>Updated at:</strong>{" "}
          {day(user.updatedAt).format("YYYY-MM-DD HH:mm:ss (UTCZ)")}
        </p>
        <h4>Permissions</h4>
        <p>
          <strong>Team Management:</strong>{" "}
          {UserHelper.numericToLevel(user.tmLevel)}
        </p>
        <p>
          <strong>Model Management:</strong>{" "}
          {UserHelper.numericToLevel(user.mmLevel)}
        </p>
        <p>
          <strong>Deployment Management:</strong>{" "}
          {UserHelper.numericToLevel(user.dmLevel)}
        </p>
      </Container>
      {deleteable && showDeleteModal ? (
        <DeleteModal resource={user} handleClose={toggleDeleteModal} />
      ) : null}
      {deactivateable && showDeactivateModal ? (
        <DeactivationModal
          resource={user}
          handleClose={toggleDeactivateModal}
        />
      ) : null}
    </>
  );
}
