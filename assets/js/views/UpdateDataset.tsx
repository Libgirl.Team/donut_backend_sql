import React from "react";
import CreateUpdateForm, {
  IFormFieldRendererProps
} from "components/CreateUpdateForm";
import gql from "graphql-tag";
import InputField from "components/InputField";
import TextArea from "components/TextArea";

export interface IDatasetParams {
  description: string;
  name: string;
  datasetURI: string;
}

export const DatasetFields = ({
  handleChange,
  errors,
  params
}: IFormFieldRendererProps) => (
  <>
    <InputField
      name="name"
      label="Name:"
      value={params.name}
      error={errors.name}
      onChange={handleChange}
    />
    <InputField
      name="datasetURI"
      label="Dataset URI:"
      value={params.datasetURI}
      error={errors.datasetURI}
      onChange={handleChange}
    />
    <TextArea
      name="description"
      label="Description:"
      onChange={handleChange}
      error={errors.description}
      value={params.description}
    />
  </>
);

const query = gql`
  query($id: ID!) {
    dataset(id: $id) {
      id
      name
      description
      datasetURI
    }
  }
`;

export const INITIAL_DATASET_PARAMS: IDatasetParams = {
  name: "",
  description: "",
  datasetURI: ""
};

export default () => {
  return (
    <CreateUpdateForm
      typename="Dataset"
      query={query}
      initialParams={INITIAL_DATASET_PARAMS}
      children={DatasetFields}
      columns={["name", "description", "datasetURI"]}
    />
  );
};
