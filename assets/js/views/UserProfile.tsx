import React from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import Loader from "components/Loader";
import UserHelper from "../helpers/UserHelper";
import Avatar from "components/Avatar";
import day from "dayjs";
import { Separator } from "components/Breadcrumb";
import { Link } from "react-router-dom";

const ElevationMessage = ({ name, email }: any) => {
  return (
    <p className="profile__elevation_message">
      If you need permission elevation, please contact your administrator:&#20;
      {name} <a href={`mailto:${email}`}>&lt;{email}&gt;</a>.
    </p>
  );
};

const UserProfile = () => {
  const { data, loading } = useQuery(gql`
    {
      admin: whoIsMyAdmin {
        name
        email
      }
      user {
        id
        name
        email
        tmLevel
        mmLevel
        dmLevel
        picture
        insertedAt
      }
    }
  `);

  const user = data?.user;
  if (loading || !user) return <Loader />;
  const elevatable = user.tmLevel < 3;

  return (
    <>
      <div className="ui breadcrumb">
        <Link to="/" className="section">
          Home
        </Link>
        <Separator />
        <div className="section active">Profile</div>
      </div>
      <div className="profile">
        <section className="profile__user_data">
          <div className="profile__avatar">
            <Avatar user={user} />
          </div>
          <div className="profile__display_name">{user?.name}</div>
          <div className="profile__email">{user?.email}</div>
          <p>Member since {day(user.insertedAt).format("MMMM D, YYYY")}</p>
        </section>
        <table className="profile__permissions">
          <thead>
            <tr>
              <th colSpan={2}>My permissions</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Team Management:</td>
              <td>{UserHelper.numericToLevel(user.tmLevel)}</td>
            </tr>
            <tr>
              <td>Model Management:</td>
              <td>{UserHelper.numericToLevel(user.mmLevel)}</td>
            </tr>
            <tr>
              <td>Deployment Management:</td>
              <td>{UserHelper.numericToLevel(user.dmLevel)}</td>
            </tr>
          </tbody>
        </table>
        {true ? <ElevationMessage {...data?.admin} /> : null}
      </div>
    </>
  );
};

export default UserProfile;
