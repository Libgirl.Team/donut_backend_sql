import React from "react";
import { LIST_USAGES } from "../graphql/queries/usageQueries";
import GenericTable from "components/GenericTable";

export default () => (
  <GenericTable
    query={LIST_USAGES}
    typename="Usage"
    heading="Usages"
    dataKey="usages"
  />
);
