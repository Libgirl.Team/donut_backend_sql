import React from "react";
import CreateUpdateForm from "components/CreateUpdateForm";

const NewAlgorithm = () => {
  return <CreateUpdateForm create typename="Algorithm" />;
};

export default NewAlgorithm;
