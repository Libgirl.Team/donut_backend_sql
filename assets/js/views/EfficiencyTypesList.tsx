import React from "react";
import GenericTable from "components/GenericTable";
import { Table } from "semantic-ui-react";
import gql from "graphql-tag";
import day from "dayjs";

const query = gql`
  query($page: Int, $sortColumn: String, $sortOrder: SortOrder) {
    efficiencyTypes: paginateEfficiencyTypes(
      page: $page
      sortColumn: $sortColumn
      sortOrder: $sortOrder
    ) {
      data {
        id
        insertedAt
        name
        unit
      }
      cursor {
        page
        totalPages
      }
    }
  }
`;

const Header = () => [
  <Table.HeaderCell name="name">Name</Table.HeaderCell>,
  <Table.HeaderCell name="unit">Unit</Table.HeaderCell>,
  <Table.HeaderCell className="date-header" name="inserted_at">
    Date added
  </Table.HeaderCell>
];

const Row = (data: any) => (
  <>
    <Table.Cell>{data.name}</Table.Cell>
    <Table.Cell>{data.unit}</Table.Cell>
    <Table.Cell className="date-value">
      {day(data.insertedAt).format("YYYY-MM-DD")}
    </Table.Cell>
  </>
);

export default () => (
  <GenericTable
    typename="EfficiencyType"
    query={query}
    heading="Efficiency types"
    dataKey="efficiencyTypes"
    renderRow={Row}
    renderHeader={Header}
  />
);
