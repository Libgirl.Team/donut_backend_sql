import React, { useCallback } from "react";
import { Button } from "semantic-ui-react";
import { useHistory } from "react-router";
const Icon = require("../icons/arrow-left.svg").default;

export default function NotFound() {
  const history = useHistory();
  const goBack = useCallback(() => {
    history.goBack();
  }, [history]);
  return (
    <div className="error_page">
      <h1 className="error_page__code">404</h1>
      <h2 className="error_page__subtitle">Oooops&hellip;</h2>
      <p className="error_page__explanation">
        The requested resource could not be found.
      </p>
      <Button color="blue" size="big" onClick={goBack}>
        <Icon /> Go back
      </Button>
    </div>
  );
}
