import React from "react";
import { LIST_MODEL_TYPES } from "../graphql/queries/modelTypeQueries";
import { Table } from "semantic-ui-react";
import day from "dayjs";
import GenericTable from "components/GenericTable";

const Header = () => [
  <Table.HeaderCell name="name">Name</Table.HeaderCell>,
  <Table.HeaderCell name="algorithm_id">Algorithm</Table.HeaderCell>,
  <Table.HeaderCell className="owner-header" name="owner_id">
    Created by
  </Table.HeaderCell>,
  <Table.HeaderCell className="date-header" name="inserted_at">
    Date added
  </Table.HeaderCell>
];

const Row = (data: any) => (
  <>
    <Table.Cell>{data.name}</Table.Cell>
    <Table.Cell>{data.algorithm?.name}</Table.Cell>
    <Table.Cell className="owner-value">{data.owner?.name}</Table.Cell>
    <Table.Cell className="date-value">
      {day(data.insertedAt).format("YYYY-MM-DD")}
    </Table.Cell>
  </>
);

export default () => {
  return (
    <GenericTable
      fixed
      typename="ModelType"
      query={LIST_MODEL_TYPES}
      heading="Model Types"
      renderHeader={Header}
      renderRow={Row}
      dataKey="modelTypes"
    />
  );
};
