import React, { useContext } from "react";
import { Card, Button } from "semantic-ui-react";
import Logo from "components/Logo";
import { Redirect } from "react-router-dom";
import AuthClient from "../actions/AuthClient";
import AuthContext from "../context/AuthContext";

export default function LoginPage() {
  const { user, refetch } = useContext(AuthContext);
  if (user) return <Redirect to="/" />;

  return (
    <Card className="centered_card login">
      <Card.Content>
        <Logo href="https://donut.libgirl.com/" />
        <p>
          Please sign in to the application using your Google Cloud Platform
          account.
        </p>
        <div>
          <Button
            onClick={() => AuthClient.performGoogleLogin(refetch)}
            variant="outlined"
            fluid
            size="large"
            color="blue"
            style={{ marginBottom: ".75rem" }}
          >
            Login with GCP
          </Button>
          <Button color="red" basic fluid>
            I don't have a GCP account
          </Button>
        </div>
      </Card.Content>
    </Card>
  );
}
