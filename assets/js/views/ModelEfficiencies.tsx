import React, { FormEvent } from "react";
import EfficiencyEvaluationForm, {
  EfficiencyEvaluationParam
} from "components/EfficiencyEvaluationForm";
import gql from "graphql-tag";
import { useParams, useHistory } from "react-router";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { UPDATE_EFFICIENCIES } from "../graphql/mutations/modelMutations";
import Loader from "components/Loader";
import SectionTitle from "components/SectionTitle";

const query = gql`
  query($id: ID!) {
    efficiencyTypes {
      id
      name
      unit
      description
    }

    model(id: $id) {
      version
      modelType {
        name
      }
      efficiencies {
        value
        typeId
      }
    }
  }
`;

export default () => {
  const { id } = useParams();
  const { loading, data } = useQuery(query, {
    variables: { id },
    fetchPolicy: "no-cache"
  });
  const history = useHistory();

  const [persist] = useMutation(UPDATE_EFFICIENCIES);
  const handleSubmit = async (
    e: FormEvent,
    state: EfficiencyEvaluationParam[]
  ) => {
    e.preventDefault();
    console.log(state);
    await persist({ variables: { modelId: id, params: state } });
    history.push(`/models/${id}`);
  };
  if (loading) return <Loader />;
  const { model, efficiencyTypes } = data;
  return (
    <div className="container">
      <SectionTitle>
        Model: {model.modelType?.name} v. {model.version}
      </SectionTitle>
      <h2>Efficiencies</h2>
      <EfficiencyEvaluationForm
        options={efficiencyTypes}
        data={model.efficiencies}
        handleSubmit={handleSubmit}
      />
    </div>
  );
};
