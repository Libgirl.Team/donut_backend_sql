import React from "react";
import CreateUpdateForm, {
  IFormFieldRendererProps
} from "components/CreateUpdateForm";
import InputField from "components/InputField";
import TextArea from "components/TextArea";

interface IEfficiencyTypeParams {
  name: string;
  description: string;
  unit: string;
}

export const INITIAL_EFFICIENCY_TYPE_PARAMS: IEfficiencyTypeParams = {
  name: "",
  description: "",
  unit: ""
};

export const EfficiencyTypeFields = ({
  params,
  errors,
  handleChange
}: IFormFieldRendererProps) => (
  <>
    <InputField
      name="name"
      label="Name:"
      value={params.name}
      error={errors.name}
      onChange={handleChange}
    />
    <InputField
      name="unit"
      label="Unit:"
      value={params.unit}
      error={errors.unit}
      onChange={handleChange}
    />
    <TextArea
      name="description"
      label="Description:"
      onChange={handleChange}
      error={errors.description}
      value={params.description}
    />
  </>
);

export default () => (
  <CreateUpdateForm
    create
    typename="EfficiencyType"
    initialParams={INITIAL_EFFICIENCY_TYPE_PARAMS}
    children={EfficiencyTypeFields}
  />
);
