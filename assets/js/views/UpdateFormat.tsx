import React from "react";
import CreateUpdateForm from "components/CreateUpdateForm";
import gql from "graphql-tag";

const query = gql`
  query($id: ID!) {
    format(id: $id) {
      id
      name
      description
    }
  }
`;

const UpdateFormat = () => {
  return <CreateUpdateForm query={query} typename="Format" />;
};

export default UpdateFormat;
