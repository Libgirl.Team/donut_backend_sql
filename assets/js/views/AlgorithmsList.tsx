import React from "react";
import { LIST_ALGORITHMS } from "../graphql/queries/algorithmQueries";
import GenericTable from "components/GenericTable";

const AlgorithmsList = () => (
  <GenericTable
    query={LIST_ALGORITHMS}
    typename="Algorithm"
    heading="Algorithms"
    dataKey="algorithms"
  />
);

export default AlgorithmsList;
