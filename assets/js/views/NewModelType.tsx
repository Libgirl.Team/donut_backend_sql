import React from "react";
import { FETCH_MODEL_TYPE_OPTIONS } from "../graphql/queries/modelTypeQueries";
import InputField from "components/InputField";
import Select from "components/Select";
import TextArea from "components/TextArea";
import { IModelTypeParams } from "../actions/ModelTypes";
import CreateUpdateForm, {
  IFormFieldRendererProps
} from "components/CreateUpdateForm";
import gql from "graphql-tag";

export const ModelTypeFields = ({
  handleChange,
  handleSelect,
  errors,
  params,
  rawData: data
}: IFormFieldRendererProps) => (
  <>
    <InputField
      autoFocus
      label="Name:"
      name="name"
      value={params.name}
      onChange={handleChange}
      error={errors.name}
    />
    <TextArea
      label="Description:"
      name="description"
      value={params.description}
      onChange={handleChange}
      error={errors.description}
    />
    <Select
      label="Algorithm"
      name="algorithmId"
      options={data.algorithms}
      value={params.algorithmId}
      onChange={handleSelect}
      error={errors.algorithmId}
      typename="Algorithm"
    />
    <Select
      label="Usage"
      name="usageId"
      options={data.usages}
      value={params.usageId}
      onChange={handleSelect}
      error={errors.usageId}
      typename="Usage"
    />
  </>
);

export const INITIAL_MODEL_TYPE_PARAMS: IModelTypeParams = {
  name: "",
  description: "",
  algorithmId: undefined,
  usageId: undefined
};

export default () => {
  return (
    <CreateUpdateForm
      create
      query={FETCH_MODEL_TYPE_OPTIONS}
      typename="ModelType"
      initialParams={INITIAL_MODEL_TYPE_PARAMS}
      children={ModelTypeFields}
    />
  );
};
