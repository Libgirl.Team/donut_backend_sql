import React from "react";
import CreateUpdateForm from "components/CreateUpdateForm";
import gql from "graphql-tag";
import {
  INITIAL_EVALUATION_TYPE_PARAMS,
  EvaluationTypeFields
} from "./NewEvaluationType";

const query = gql`
  query($id: ID!) {
    evaluationType(id: $id) {
      id
      name
      description
      unit
      datasetId
    }
    datasets {
      key: id
      value: id
      text: name
    }
  }
`;

export default () => (
  <CreateUpdateForm
    typename="EvaluationType"
    initialParams={INITIAL_EVALUATION_TYPE_PARAMS}
    query={query}
    children={EvaluationTypeFields}
  />
);
