import React from "react";
import CreateUpdateForm, {
  IFormFieldRendererProps
} from "components/CreateUpdateForm";
import gql from "graphql-tag";
import InputField from "components/InputField";
import TextArea from "components/TextArea";
import Select from "components/Select";

interface IEvaluationTypeParams {
  name: string;
  description: string;
  unit: string;
  datasetId: number | string | null;
}

export const INITIAL_EVALUATION_TYPE_PARAMS: IEvaluationTypeParams = {
  name: "",
  description: "",
  unit: "",
  datasetId: null
};

const query = gql`
  {
    datasets {
      key: id
      value: id
      text: name
    }
  }
`;

export const EvaluationTypeFields = ({
  params,
  errors,
  handleChange,
  handleSelect,
  rawData: data
}: IFormFieldRendererProps) => (
  <>
    <InputField
      name="name"
      label="Name:"
      value={params.name}
      error={errors.name}
      onChange={handleChange}
    />
    <InputField
      name="unit"
      label="Unit:"
      value={params.unit}
      error={errors.unit}
      onChange={handleChange}
    />
    <TextArea
      name="description"
      label="Description:"
      onChange={handleChange}
      error={errors.description}
      value={params.description}
    />
    <Select
      name="datasetId"
      label="Dataset:"
      onChange={handleSelect}
      options={data.datasets}
      error={errors.datasetId}
      value={params.datasetId}
    />
  </>
);

export default () => (
  <CreateUpdateForm
    create
    query={query}
    typename="EvaluationType"
    initialParams={INITIAL_EVALUATION_TYPE_PARAMS}
    children={EvaluationTypeFields}
  />
);
