import React from "react";
import CreateUpdateForm from "components/CreateUpdateForm";
import gql from "graphql-tag";
import { initialParams } from "./NewModel";
import { IFormFieldRendererProps } from "components/CreateUpdateForm";
import InputField from "components/InputField";
import Select from "components/Select";
import TextArea from "components/TextArea";

const ModelFields = ({
  params,
  handleSelect,
  handleChange,
  errors,
  rawData: data
}: IFormFieldRendererProps) => (
  <>
    <p>
      <strong>Model info: Model type:</strong> {data.model?.modelType?.name} (#
      {data.model?.modelType?.id}), <strong>version:</strong>{" "}
      {data.model?.version}
    </p>
    <Select
      label="Dataset:"
      name="datasetId"
      options={data.datasets}
      value={params.datasetId}
      onChange={handleSelect}
      error={errors.datasetId}
    />
    <Select
      label="Format:"
      name="formatId"
      options={data.formats}
      value={params.formatId}
      onChange={handleSelect}
      error={errors.formatId}
    />
    <InputField
      label="Model URI:"
      placeholder="gs://bucket-name/resource.bin"
      name="modelURI"
      value={params.modelURI}
      onChange={handleChange}
      error={errors.modelURI}
    />
    <TextArea
      label="Description:"
      name="description"
      value={params.description}
      onChange={handleChange}
      error={errors.description}
    />
  </>
);

const query = gql`
  query($id: ID!) {
    datasets {
      key: id
      value: id
      text: name
    }
    formats {
      key: id
      value: id
      text: name
    }
    model(id: $id) {
      id
      modelType {
        id
        name
      }
      version
      description
      owner {
        id
        name
      }
      author {
        id
        name
      }
      datasetId
      formatId
      modelURI
    }
  }
`;

export default () => {
  return (
    <CreateUpdateForm
      query={query}
      initialParams={initialParams}
      children={ModelFields}
      typename="Model"
    />
  );
};
