import React from "react";
import gql from "graphql-tag";
import GenericShow from "components/GenericShow";

const query = gql`
  query($id: ID!) {
    efficiencyType(id: $id) {
      id
      name
      unit
      description
      insertedAt
      updatedAt
    }
  }
`;

const SingleEfficiencyType = () => {
  return (
    <GenericShow
      resourceType="Efficiency type"
      resourceKey="efficiencyType"
      query={query}
    >
      {(resource) => (
        <>
          <p>
            <strong>Unit:</strong> {resource.unit}
          </p>
        </>
      )}
    </GenericShow>
  );
};

export default SingleEfficiencyType;
