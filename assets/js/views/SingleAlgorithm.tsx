import React from "react";
import gql from "graphql-tag";
import GenericShow from "components/GenericShow";

const query = gql`
  query($id: ID!) {
    algorithm(id: $id) {
      id
      name
      owner {
        id
        name
      }
      description
      insertedAt
      updatedAt
      modelTypes {
        id
        name
      }
    }
  }
`;

const SingleAlgorithm = () => {
  return (
    <GenericShow
      resourceType="Algorithm"
      resourceKey="algorithm"
      query={query}
      dependentResources={["modelTypes"]}
    />
  );
};

export default SingleAlgorithm;
