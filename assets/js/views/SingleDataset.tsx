import React from "react";
import gql from "graphql-tag";
import GenericShow from "components/GenericShow";

const query = gql`
  query($id: ID!) {
    dataset(id: $id) {
      id
      name
      owner {
        id
        name
      }
      datasetURI
      description
      insertedAt
      updatedAt
      models {
        id
        version
        modelType {
          id
          name
        }
      }
    }
  }
`;

const SingleDataset = () => {
  return (
    <GenericShow
      resourceType="Dataset"
      resourceKey="dataset"
      query={query}
      dependentResources={["models"]}
    >
      {(resource) => (
        <p>
          <strong>Dataset URI:</strong>{" "}
          <a
            href={resource.datasetURI}
            target="_blank"
            rel="noopener noreferrer"
          >
            {resource.datasetURI}
          </a>
        </p>
      )}
    </GenericShow>
  );
};

export default SingleDataset;
