import React from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const query = gql`
  {
    admin: whoIsMyAdmin {
      name
      email
    }
  }
`;

export default function NotFound() {
  const { data, loading } = useQuery(query);
  return (
    <div className="error_page">
      <h1 className="error_page__code">403</h1>
      <h2 className="error_page__subtitle">Forbidden</h2>
      <p className="error_page__explanation">
        Your don&apos;t have sufficient permissions to view this page.
        <br />
        Please contact your organization&apos;s administrator to request
        permission elevation.
        <br />
        Your administrator is
        {loading ? (
          <span> (loading)</span>
        ) : (
          <span>
            {" "}
            <a href={`mailto:${data.admin.email}`}>
              {data.admin.name} &lt;{data.admin.email}&gt;
            </a>
            .
          </span>
        )}
      </p>
    </div>
  );
}
