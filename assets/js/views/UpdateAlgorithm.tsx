import React from "react";
import CreateUpdateForm from "components/CreateUpdateForm";
import gql from "graphql-tag";

const query = gql`
  query($id: ID!) {
    algorithm(id: $id) {
      id
      name
      description
    }
  }
`;

const UpdateAlgorithm = () => {
  return <CreateUpdateForm query={query} typename="Algorithm" />;
};

export default UpdateAlgorithm;
