import React from "react";
import CreateUpdateForm from "components/CreateUpdateForm";
import gql from "graphql-tag";

const query = gql`
  query($id: ID!) {
    dataType(id: $id) {
      id
      name
      description
    }
  }
`;

export default () => {
  return <CreateUpdateForm query={query} typename="DataType" />;
};
