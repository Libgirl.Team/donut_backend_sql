import React from "react";
import CreateUpdateForm from "components/CreateUpdateForm";

export default () => {
  return <CreateUpdateForm create typename="DataType" />;
};
