import React from "react";
import GenericImport from "components/GenericImport/index";
import { ExampleSchema } from "components/GenericImport/SampleJson";

const schema: ExampleSchema = {
  modelTypeId: "id",
  major: "integer",
  minor: "integer",
  datasetId: "id",
  sourceModelId: "id",
  modelURI: "string",
  description: "string",
  efficiencies: "efficiency_evaluation",
  evaluations: "efficiency_evaluation"
};

const ImportModels = () => {
  return <GenericImport typename="Model" schema={schema} />;
};

export default ImportModels;
