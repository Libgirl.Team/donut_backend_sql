import reducers from "./reducers";
import thunkMiddleware from "redux-thunk";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";

function configureStore() {
  const middlewareEnhancer = applyMiddleware(thunkMiddleware);
  const composedEnhancers = composeWithDevTools(middlewareEnhancer);
  const store = createStore(reducers, undefined, composedEnhancers);
  return store;
}

export default configureStore();
