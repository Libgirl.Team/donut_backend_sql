import { IPreferences } from "../types/preferences";

export const DEFAULT_PREFERENCES: IPreferences = {
  models: {
    columns: [
      "modelType.name",
      "version",
      "modelType.algorithm.name",
      "owner.name"
    ],
    efficiencyTypeIDs: [
      "6716984a-0864-4bf0-b872-6fd468c16b20",
      "02c17f9d-f535-421e-9ebf-7b2e68187ae7",
      "3eabee02-9b26-4e1f-89e3-b9d5b55d9ff8"
    ],
    evaluationTypeIDs: ["fed24ecb-beef-462c-96a3-b91e1fa6858d"],
    sortColumn: "version",
    sortOrder: "DESC"
  }
};
