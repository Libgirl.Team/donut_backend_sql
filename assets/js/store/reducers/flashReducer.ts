export enum FlashLevel {
  Info = "info",
  Error = "info",
  Success = "success"
}

export interface FlashMessage {
  level: FlashLevel | FlashLevel[keyof FlashLevel];
  msg: string;
}

export enum FlashActionType {
  AddMessage = "ADD_FLASH_MESSAGE",
  Clear = "CLEAR_FLASH"
}

type FlashAction =
  | { type: FlashActionType.Clear }
  | { type: typeof FlashActionType.AddMessage; payload: FlashMessage };

export type FlashState = FlashMessage[];

const initialState: FlashState = [];

export default (state: FlashState = initialState, action: FlashAction) => {
  switch (action.type) {
    case FlashActionType.Clear:
      return [];
    case FlashActionType.AddMessage:
      return [...state, action.payload];
    default:
      return state;
  }
};
