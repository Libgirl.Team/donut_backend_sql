import { combineReducers } from "redux";
import preferences from "./preferencesReducer";
import flash from "./flashReducer";

const rootReducer = combineReducers({
  preferences,
  flash
});

export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
