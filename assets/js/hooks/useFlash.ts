import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../store/reducers";
import { useCallback } from "react";
import {
  FlashMessage,
  FlashActionType,
  FlashState
} from "../store/reducers/flashReducer";

export default () => {
  const dispatch = useDispatch();
  const addMessage = useCallback(
    (msg: FlashMessage) => {
      dispatch({ type: FlashActionType.AddMessage, payload: msg });
    },
    [dispatch]
  );
  const clearFlash = useCallback(() => {
    dispatch({ type: FlashActionType.Clear });
  }, [dispatch]);

  const messages = useSelector<RootState>(state => state.flash) as FlashState;
  return {
    messages,
    addMessage,
    clearFlash
  };
};
