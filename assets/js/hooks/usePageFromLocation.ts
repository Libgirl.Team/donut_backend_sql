import { useLocation } from "react-router";
import qs from "querystring";

export default function usePageFromLocation() {
  const location = useLocation();
  const query = location?.search?.replace(/^\?/, "");
  if (!query) return { page: 1 };
  const { page, term } = qs.parse(query);
  let parsed;
  try {
    parsed = parseInt(page as any);
  } catch (_e) {
    parsed = 1;
  }
  return { page: parsed || 1, term: term || "" };
}
