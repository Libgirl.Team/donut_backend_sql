import gql from "graphql-tag";

export const LIST_USAGES = gql`
  query($page: Int, $sortColumn: String, $sortOrder: SortOrder) {
    usages: paginateUsages(
      page: $page
      sortColumn: $sortColumn
      sortOrder: $sortOrder
    ) {
      cursor {
        totalPages
        page
      }
      data {
        id
        name
        insertedAt
        owner {
          id
          name
        }
        inputType {
          id
          name
        }
        outputType {
          id
          name
        }
      }
    }
  }
`;
