import gql from "graphql-tag";

export const USER_DATA = gql`
  fragment UserData on User {
    id
    email
    name
    mmLevel
    tmLevel
    dmLevel
    picture
  }
`;

export const FETCH_USER_DATA = gql`
  ${USER_DATA}
  {
    user {
      ...UserData
    }
  }
`;

export const LIST_USERS = gql`
  ${USER_DATA}
  query($page: Int, $term: String, $sortColumn: String, $sortOrder: SortOrder) {
    users: paginateUsers(
      page: $page
      term: $term
      sortColumn: $sortColumn
      sortOrder: $sortOrder
    ) {
      cursor {
        page
        itemsPerPage
        totalPages
      }
      data {
        ...UserData
        insertedAt
      }
    }
  }
`;
