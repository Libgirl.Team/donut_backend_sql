import gql from "graphql-tag";

export const MODEL_TYPE_DETAILS = gql`
  fragment ModelTypeDetails on ModelType {
    id
    name
    insertedAt
    owner {
      id
      name
    }
    algorithm {
      id
      name
    }
  }
`;

export const LIST_MODEL_TYPES = gql`
  ${MODEL_TYPE_DETAILS}
  query ListModelTypes($page: Int, $sortOrder: SortOrder, $sortColumn: String) {
    modelTypes: paginateModelTypes(
      page: $page
      sortOrder: $sortOrder
      sortColumn: $sortColumn
    ) {
      data {
        ...ModelTypeDetails
      }
      cursor {
        page
        totalPages
      }
    }
  }
`;

export const GET_MODEL_TYPE = gql`
  ${MODEL_TYPE_DETAILS}
  query getModelType($id: ID!) {
    modelType(id: $id) {
      ...ModelTypeDetails
    }
  }
`;

export const FETCH_MODEL_TYPE_OPTIONS = gql`
  {
    algorithms {
      key: id
      value: id
      text: name
    }
    usages {
      key: id
      value: id
      text: name
    }
  }
`;
