import gql from "graphql-tag";

export const LIST_ALGORITHMS = gql`
  query($page: Int, $sortOrder: SortOrder, $sortColumn: String) {
    algorithms: paginateAlgorithms(
      page: $page
      sortOrder: $sortOrder
      sortColumn: $sortColumn
    ) {
      cursor {
        totalPages
      }
      data {
        id
        name
        owner {
          name
        }
        insertedAt
      }
    }
  }
`;
