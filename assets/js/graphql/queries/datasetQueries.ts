import gql from "graphql-tag";

export const LIST_DATASETS = gql`
  query($page: Int, $sortColumn: String, $sortOrder: SortOrder) {
    datasets: paginateDatasets(
      page: $page
      sortColumn: $sortColumn
      sortOrder: $sortOrder
    ) {
      data {
        id
        name
        owner {
          name
        }
        insertedAt
      }
      cursor {
        page
        totalPages
      }
    }
  }
`;
