import gql from "graphql-tag";

export const FETCH_PREFERENCES = gql`
  {
    user {
      preferences
    }
  }
`;
