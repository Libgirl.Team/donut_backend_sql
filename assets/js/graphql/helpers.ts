import { DocumentNode } from "graphql";
import getInObject from "lodash/get";
import gql from "graphql-tag";

export function getMutationKey(mutation: DocumentNode) {
  return getInObject(
    mutation,
    "definitions[0].selectionSet.selections[0].name.value"
  );
}

export function createMutation(typename: string) {
  return gql`
  mutation($params: ${typename}Params!)  {
   create${typename}(params: $params) {
      success errors resource { id }
    }
  }
  `;
}
