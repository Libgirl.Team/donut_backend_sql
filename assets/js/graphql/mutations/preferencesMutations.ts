import gql from "graphql-tag";

export const UPDATE_PREFERENCES = gql`
  mutation updatePreferences($preferences: PreferencesInput) {
    updatePreferences(preferences: $preferences) {
      success
    }
  }
`;
