import gql from "graphql-tag";
import { EFFICIENCY_EVALUATION_DETAILS } from "../queries/modelQueries";

export const UPDATE_EFFICIENCIES = gql`
  ${EFFICIENCY_EVALUATION_DETAILS}
  mutation updateEfficiencies(
    $modelId: ID!
    $params: [EfficiencyEvaluationParam!]!
  ) {
    setModelEfficiencies(id: $modelId, efficiencies: $params) {
      success
      errors
      resource {
        id
        efficiencies {
          ...EfficiencyEvaluationDetails
        }
      }
    }
  }
`;

export const UPDATE_EVALUATIONS = gql`
  ${EFFICIENCY_EVALUATION_DETAILS}
  mutation updateEvaluations(
    $modelId: ID!
    $params: [EfficiencyEvaluationParam!]!
  ) {
    setModelEvaluations(id: $modelId, evaluations: $params) {
      success
      errors
      resource {
        id
        evaluations {
          ...EfficiencyEvaluationDetails
        }
      }
    }
  }
`;
