import axios from "axios";
import qs from "querystring";

const OAUTH_BASE = "https://accounts.google.com/o/oauth2/v2/auth?";

export default class AuthClient {
  static async signOut(callback: () => void) {
    axios
      .post(`${CONFIG.api_endpoint}/logout`, null, { withCredentials: true })
      .then(callback);
  }

  static async performGoogleLogin(callback: () => void) {
    AuthClient.initiateGoogleLogin().then(callback);
  }

  static async initiateGoogleLogin() {
    const authUrl =
      OAUTH_BASE +
      qs.encode({
        redirect_uri: `${CONFIG.api_endpoint}/oauth`,
        client_id: CONFIG.oauth_client_id,
        response_type: "code",
        scope: "email profile"
      });
    return new Promise<any>((resolve, reject) => {
      const loginWindow = window.open(authUrl);
      if (loginWindow) {
        const timer = setInterval(() => {
          if (loginWindow.closed) {
            clearInterval(timer);
            resolve(true);
          }
        }, 500);
      } else {
        return reject(new Error("Could not open login window"));
      }
    });
  }
}
