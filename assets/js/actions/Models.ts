export interface IModelParams {
  major: number;
  minor: number;
  modelTypeId?: number | string;
  sourceModelId?: number | string;
  sourceModelNotes?: string;
  authorId?: string;
  datasetId?: string;
  formatId?: string;
  description: string;
  modelURI: string;
}
