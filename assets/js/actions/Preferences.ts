import { IPreferences } from "../types/preferences";
import { PreferencesAction } from "../store/reducers/preferencesReducer";
import store from "../store";
import client from "../graphql/client";
import { UPDATE_PREFERENCES } from "../graphql/mutations/preferencesMutations";
import { FETCH_PREFERENCES } from "../graphql/queries/preferencesQueries";
import { DEFAULT_PREFERENCES } from "../store/defaults";

export const MODEL_COLUMN_HEADINGS: { [key: string]: any } = {
  "modelType.name": "Model type",
  version: "Version",
  "modelType.algorithm.name": "Algorithm",
  "owner.name": "Owner",
  "author.name": "Author"
};

export default class Preferences {
  static async updatePreferences(preferences: IPreferences) {
    await Preferences.persistPreferences(preferences);
    store.dispatch({
      type: PreferencesAction.Update,
      payload: preferences
    });
  }

  static async persistPreferences(preferences: IPreferences) {
    return client.mutate({
      mutation: UPDATE_PREFERENCES,
      variables: { preferences }
    });
  }

  static async setSortColumn(name: string) {
    const { preferences } = store.getState();
    let sortOrder;
    if (
      name === preferences.models?.sortColumn &&
      preferences.models?.sortOrder === "DESC"
    )
      sortOrder = "ASC";
    else sortOrder = "DESC";
    const newPreferences = {
      ...preferences,
      models: {
        ...preferences.models,
        sortOrder,
        sortColumn: name
      }
    };
    Preferences.persistPreferences(newPreferences);
    store.dispatch({
      type: PreferencesAction.SetSortColumn,
      payload: {
        sortOrder,
        sortColumn: name
      }
    });
  }

  static async fetchPreferences() {
    const { data } = await client.query({
      query: FETCH_PREFERENCES,
      fetchPolicy: "no-cache"
    });
    const preferences = data?.user?.preferences;
    store.dispatch({
      type: PreferencesAction.Fetch,
      payload: preferences
    });
  }

  static async resetPreferences() {
    await Preferences.persistPreferences(DEFAULT_PREFERENCES);
    store.dispatch({
      type: PreferencesAction.Reset
    });
  }
}
