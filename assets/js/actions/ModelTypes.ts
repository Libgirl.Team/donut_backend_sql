export interface IModelTypeParams {
  name: string;
  description: string;
  algorithmId?: number | string;
  usageId?: number | string;
}

export type FormErrors<T> = { [K in keyof T]?: string };
